package ru.bot.starter.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.kotlin.coroutine.EnableCoroutine
import ru.bot.starter.handler.ErrorHandler
import ru.bot.starter.handler.ExceptionHandler
import ru.bot.starter.handler.ResultHandler
import ru.bot.starter.tdlib.Client
import ru.bot.starter.tdlib.LogStreamFile
import ru.bot.starter.tdlib.SetLogStream
import java.io.IOError
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path

@Configuration
@EnableCoroutine
@ComponentScan("ru.bot.starter")
@EnableConfigurationProperties(TelegramProperties::class)
class TdlibClientConfiguration {

    @Bean
    @ConditionalOnMissingBean(Client::class)
    fun client(updatesHandler: ResultHandler,
               errorHandler: ErrorHandler?,
               exceptionHandler: ExceptionHandler,
               props: TelegramProperties): Client {
        System.loadLibrary("tdjni")
        val dir = props.tdlib.databaseDirectory
        if (!Files.exists(Path.of(dir))) {
            Files.createDirectory(Path.of(dir))
        }
        val file = "$dir/tdlib.log"
        val result = Client.execute(SetLogStream(LogStreamFile(file, 1 shl 27)))
        if (result is Error) {
            throw IOError(IOException("Write access to $file is required"))
        }
        return Client(updatesHandler, null, errorHandler, exceptionHandler)
    }

}
