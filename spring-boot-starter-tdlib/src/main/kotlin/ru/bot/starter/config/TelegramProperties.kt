package ru.bot.starter.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("telegram")
class TelegramProperties(
        val bot: Bot,
        val tdlib: Tdlib,
        val proxy: Proxy = Proxy()
) {

    class Bot(
            val token: String
    )

    class Tdlib(
            val useTestDc: Boolean = false,
            val databaseDirectory: String = System.getProperty("user.home", "") + "/tdlib",
            val filesDirectory: String = "",
            val useFileDatabase: Boolean = false,
            val useChatInfoDatabase: Boolean = false,
            val useMessageDatabase: Boolean = false,
            val useSecretChats: Boolean = false,
            val apiId: Int,
            val apiHash: String,
            val systemLanguageCode: String = "en",
            val deviceModel: String = System.getProperty("os.name"),
            val systemVersion: String = System.getProperty("os.version"),
            val applicationVersion: String,
            val enableStorageOptimizer: Boolean = true,
            val ignoreFileNames: Boolean = false
    )

    class Proxy(
            val host: String? = null,
            val port: Int? = null,
            val secret: String? = null
    )

}
