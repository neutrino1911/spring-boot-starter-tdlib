package ru.bot.starter.config

import mu.KotlinLogging
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kotlin.coroutine.event.CoroutineApplicationEventPublisher
import ru.bot.starter.handler.DefaultUpdatesHandler
import ru.bot.starter.handler.ExceptionHandler
import ru.bot.starter.handler.ResultHandler
import ru.bot.starter.tdlib.Client

@Configuration
class HandlerConfiguration {

    @Bean
    @ConditionalOnMissingBean(ResultHandler::class)
    fun resultHandler(publisher: CoroutineApplicationEventPublisher) = DefaultUpdatesHandler(publisher)

    @Bean
    @ConditionalOnMissingBean(ExceptionHandler::class)
    fun exceptionHandler(): ExceptionHandler {
        val logger = KotlinLogging.logger(Client::class.java.name)
        return object : ExceptionHandler {
            override suspend fun onException(exception: Throwable) {
                logger.error(exception.localizedMessage, exception)
            }
        }
    }

}
