package ru.bot.starter.event

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.event.EventListener
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import ru.bot.starter.tdlib.Message
import ru.bot.starter.util.EventFilter

@Component
class NewMessageEventProducer(
        private val filters: List<NewMessageEventFilter>,
        private val handlers: List<NewMessageEventHandler>
) {

    @EventListener(UpdateNewMessageEvent::class)
    suspend fun handle(event: UpdateNewMessageEvent): NewMessageEvent? {
        return NewMessageEvent(event.update.message).let { newMessageEvent ->
            if (filters.all { it.test(newMessageEvent) }) newMessageEvent else null
        }.also { newMessageEvent ->
            newMessageEvent?.let { nme -> handlers.forEach { it.handle(nme) } }
        }
    }

}

data class NewMessageEvent(val message: Message)

interface NewMessageEventFilter : EventFilter<NewMessageEvent>

@Order(0)
@Component
@ConditionalOnProperty(name = ["telegram.starter.filter-outgoing-events"], havingValue = "true", matchIfMissing = true)
class OutgoingNewMessageEventFilter : NewMessageEventFilter {
    override suspend fun test(t: NewMessageEvent) = !t.message.isOutgoing
}

interface NewMessageEventHandler {
    suspend fun handle(event: NewMessageEvent)
}
