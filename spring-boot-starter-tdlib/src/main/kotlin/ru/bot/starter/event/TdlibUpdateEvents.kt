package ru.bot.starter.event

import ru.bot.starter.tdlib.*

open class TdlibUpdateEvent<T : TelegramUpdate>(val name: String, val update: T)

class UpdateAuthorizationStateEvent(name: String, update: UpdateAuthorizationState) :
        TdlibUpdateEvent<UpdateAuthorizationState>(name, update)

class UpdateNewMessageEvent(name: String, update: UpdateNewMessage) :
        TdlibUpdateEvent<UpdateNewMessage>(name, update)

class UpdateMessageSendAcknowledgedEvent(name: String, update: UpdateMessageSendAcknowledged) :
        TdlibUpdateEvent<UpdateMessageSendAcknowledged>(name, update)

class UpdateMessageSendSucceededEvent(name: String, update: UpdateMessageSendSucceeded) :
        TdlibUpdateEvent<UpdateMessageSendSucceeded>(name, update)

class UpdateMessageSendFailedEvent(name: String, update: UpdateMessageSendFailed) :
        TdlibUpdateEvent<UpdateMessageSendFailed>(name, update)

class UpdateMessageContentEvent(name: String, update: UpdateMessageContent) :
        TdlibUpdateEvent<UpdateMessageContent>(name, update)

class UpdateMessageEditedEvent(name: String, update: UpdateMessageEdited) :
        TdlibUpdateEvent<UpdateMessageEdited>(name, update)

class UpdateMessageViewsEvent(name: String, update: UpdateMessageViews) :
        TdlibUpdateEvent<UpdateMessageViews>(name, update)

class UpdateMessageContentOpenedEvent(name: String, update: UpdateMessageContentOpened) :
        TdlibUpdateEvent<UpdateMessageContentOpened>(name, update)

class UpdateMessageMentionReadEvent(name: String, update: UpdateMessageMentionRead) :
        TdlibUpdateEvent<UpdateMessageMentionRead>(name, update)

class UpdateNewChatEvent(name: String, update: UpdateNewChat) :
        TdlibUpdateEvent<UpdateNewChat>(name, update)

class UpdateChatTitleEvent(name: String, update: UpdateChatTitle) :
        TdlibUpdateEvent<UpdateChatTitle>(name, update)

class UpdateChatPhotoEvent(name: String, update: UpdateChatPhoto) :
        TdlibUpdateEvent<UpdateChatPhoto>(name, update)

class UpdateChatPermissionsEvent(name: String, update: UpdateChatPermissions) :
        TdlibUpdateEvent<UpdateChatPermissions>(name, update)

class UpdateChatLastMessageEvent(name: String, update: UpdateChatLastMessage) :
        TdlibUpdateEvent<UpdateChatLastMessage>(name, update)

class UpdateChatOrderEvent(name: String, update: UpdateChatOrder) :
        TdlibUpdateEvent<UpdateChatOrder>(name, update)

class UpdateChatIsPinnedEvent(name: String, update: UpdateChatIsPinned) :
        TdlibUpdateEvent<UpdateChatIsPinned>(name, update)

class UpdateChatIsMarkedAsUnreadEvent(name: String, update: UpdateChatIsMarkedAsUnread) :
        TdlibUpdateEvent<UpdateChatIsMarkedAsUnread>(name, update)

class UpdateChatIsSponsoredEvent(name: String, update: UpdateChatIsSponsored) :
        TdlibUpdateEvent<UpdateChatIsSponsored>(name, update)

class UpdateChatDefaultDisableNotificationEvent(name: String, update: UpdateChatDefaultDisableNotification) :
        TdlibUpdateEvent<UpdateChatDefaultDisableNotification>(name, update)

class UpdateChatReadInboxEvent(name: String, update: UpdateChatReadInbox) :
        TdlibUpdateEvent<UpdateChatReadInbox>(name, update)

class UpdateChatReadOutboxEvent(name: String, update: UpdateChatReadOutbox) :
        TdlibUpdateEvent<UpdateChatReadOutbox>(name, update)

class UpdateChatUnreadMentionCountEvent(name: String, update: UpdateChatUnreadMentionCount) :
        TdlibUpdateEvent<UpdateChatUnreadMentionCount>(name, update)

class UpdateChatNotificationSettingsEvent(name: String, update: UpdateChatNotificationSettings) :
        TdlibUpdateEvent<UpdateChatNotificationSettings>(name, update)

class UpdateScopeNotificationSettingsEvent(name: String, update: UpdateScopeNotificationSettings) :
        TdlibUpdateEvent<UpdateScopeNotificationSettings>(name, update)

class UpdateChatPinnedMessageEvent(name: String, update: UpdateChatPinnedMessage) :
        TdlibUpdateEvent<UpdateChatPinnedMessage>(name, update)

class UpdateChatReplyMarkupEvent(name: String, update: UpdateChatReplyMarkup) :
        TdlibUpdateEvent<UpdateChatReplyMarkup>(name, update)

class UpdateChatDraftMessageEvent(name: String, update: UpdateChatDraftMessage) :
        TdlibUpdateEvent<UpdateChatDraftMessage>(name, update)

class UpdateChatOnlineMemberCountEvent(name: String, update: UpdateChatOnlineMemberCount) :
        TdlibUpdateEvent<UpdateChatOnlineMemberCount>(name, update)

class UpdateNotificationEvent(name: String, update: UpdateNotification) :
        TdlibUpdateEvent<UpdateNotification>(name, update)

class UpdateNotificationGroupEvent(name: String, update: UpdateNotificationGroup) :
        TdlibUpdateEvent<UpdateNotificationGroup>(name, update)

class UpdateActiveNotificationsEvent(name: String, update: UpdateActiveNotifications) :
        TdlibUpdateEvent<UpdateActiveNotifications>(name, update)

class UpdateHavePendingNotificationsEvent(name: String, update: UpdateHavePendingNotifications) :
        TdlibUpdateEvent<UpdateHavePendingNotifications>(name, update)

class UpdateDeleteMessagesEvent(name: String, update: UpdateDeleteMessages) :
        TdlibUpdateEvent<UpdateDeleteMessages>(name, update)

class UpdateUserChatActionEvent(name: String, update: UpdateUserChatAction) :
        TdlibUpdateEvent<UpdateUserChatAction>(name, update)

class UpdateUserStatusEvent(name: String, update: UpdateUserStatus) :
        TdlibUpdateEvent<UpdateUserStatus>(name, update)

class UpdateUserEvent(name: String, update: UpdateUser) :
        TdlibUpdateEvent<UpdateUser>(name, update)

class UpdateBasicGroupEvent(name: String, update: UpdateBasicGroup) :
        TdlibUpdateEvent<UpdateBasicGroup>(name, update)

class UpdateSupergroupEvent(name: String, update: UpdateSupergroup) :
        TdlibUpdateEvent<UpdateSupergroup>(name, update)

class UpdateSecretChatEvent(name: String, update: UpdateSecretChat) :
        TdlibUpdateEvent<UpdateSecretChat>(name, update)

class UpdateUserFullInfoEvent(name: String, update: UpdateUserFullInfo) :
        TdlibUpdateEvent<UpdateUserFullInfo>(name, update)

class UpdateBasicGroupFullInfoEvent(name: String, update: UpdateBasicGroupFullInfo) :
        TdlibUpdateEvent<UpdateBasicGroupFullInfo>(name, update)

class UpdateSupergroupFullInfoEvent(name: String, update: UpdateSupergroupFullInfo) :
        TdlibUpdateEvent<UpdateSupergroupFullInfo>(name, update)

class UpdateServiceNotificationEvent(name: String, update: UpdateServiceNotification) :
        TdlibUpdateEvent<UpdateServiceNotification>(name, update)

class UpdateFileEvent(name: String, update: UpdateFile) :
        TdlibUpdateEvent<UpdateFile>(name, update)

class UpdateFileGenerationStartEvent(name: String, update: UpdateFileGenerationStart) :
        TdlibUpdateEvent<UpdateFileGenerationStart>(name, update)

class UpdateFileGenerationStopEvent(name: String, update: UpdateFileGenerationStop) :
        TdlibUpdateEvent<UpdateFileGenerationStop>(name, update)

class UpdateCallEvent(name: String, update: UpdateCall) :
        TdlibUpdateEvent<UpdateCall>(name, update)

class UpdateUserPrivacySettingRulesEvent(name: String, update: UpdateUserPrivacySettingRules) :
        TdlibUpdateEvent<UpdateUserPrivacySettingRules>(name, update)

class UpdateUnreadMessageCountEvent(name: String, update: UpdateUnreadMessageCount) :
        TdlibUpdateEvent<UpdateUnreadMessageCount>(name, update)

class UpdateUnreadChatCountEvent(name: String, update: UpdateUnreadChatCount) :
        TdlibUpdateEvent<UpdateUnreadChatCount>(name, update)

class UpdateOptionEvent(name: String, update: UpdateOption) :
        TdlibUpdateEvent<UpdateOption>(name, update)

class UpdateInstalledStickerSetsEvent(name: String, update: UpdateInstalledStickerSets) :
        TdlibUpdateEvent<UpdateInstalledStickerSets>(name, update)

class UpdateTrendingStickerSetsEvent(name: String, update: UpdateTrendingStickerSets) :
        TdlibUpdateEvent<UpdateTrendingStickerSets>(name, update)

class UpdateRecentStickersEvent(name: String, update: UpdateRecentStickers) :
        TdlibUpdateEvent<UpdateRecentStickers>(name, update)

class UpdateFavoriteStickersEvent(name: String, update: UpdateFavoriteStickers) :
        TdlibUpdateEvent<UpdateFavoriteStickers>(name, update)

class UpdateSavedAnimationsEvent(name: String, update: UpdateSavedAnimations) :
        TdlibUpdateEvent<UpdateSavedAnimations>(name, update)

class UpdateSelectedBackgroundEvent(name: String, update: UpdateSelectedBackground) :
        TdlibUpdateEvent<UpdateSelectedBackground>(name, update)

class UpdateLanguagePackStringsEvent(name: String, update: UpdateLanguagePackStrings) :
        TdlibUpdateEvent<UpdateLanguagePackStrings>(name, update)

class UpdateConnectionStateEvent(name: String, update: UpdateConnectionState) :
        TdlibUpdateEvent<UpdateConnectionState>(name, update)

class UpdateTermsOfServiceEvent(name: String, update: UpdateTermsOfService) :
        TdlibUpdateEvent<UpdateTermsOfService>(name, update)

class UpdateNewInlineQueryEvent(name: String, update: UpdateNewInlineQuery) :
        TdlibUpdateEvent<UpdateNewInlineQuery>(name, update)

class UpdateNewChosenInlineResultEvent(name: String, update: UpdateNewChosenInlineResult) :
        TdlibUpdateEvent<UpdateNewChosenInlineResult>(name, update)

class UpdateNewCallbackQueryEvent(name: String, update: UpdateNewCallbackQuery) :
        TdlibUpdateEvent<UpdateNewCallbackQuery>(name, update)

class UpdateNewInlineCallbackQueryEvent(name: String, update: UpdateNewInlineCallbackQuery) :
        TdlibUpdateEvent<UpdateNewInlineCallbackQuery>(name, update)

class UpdateNewShippingQueryEvent(name: String, update: UpdateNewShippingQuery) :
        TdlibUpdateEvent<UpdateNewShippingQuery>(name, update)

class UpdateNewPreCheckoutQueryEvent(name: String, update: UpdateNewPreCheckoutQuery) :
        TdlibUpdateEvent<UpdateNewPreCheckoutQuery>(name, update)

class UpdateNewCustomEventEvent(name: String, update: UpdateNewCustomEvent) :
        TdlibUpdateEvent<UpdateNewCustomEvent>(name, update)

class UpdateNewCustomQueryEvent(name: String, update: UpdateNewCustomQuery) :
        TdlibUpdateEvent<UpdateNewCustomQuery>(name, update)

class UpdatePollEvent(name: String, update: UpdatePoll) :
        TdlibUpdateEvent<UpdatePoll>(name, update)
