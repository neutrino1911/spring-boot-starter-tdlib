package ru.bot.starter.event

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.event.EventListener
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import ru.bot.starter.tdlib.*
import ru.bot.starter.util.EventFilter

@Component
class CommandEventProducer(
        private val client: Client,
        private val filters: List<CommandEventFilter>,
        private val handlers: List<CommandEventHandler>
) {

    private lateinit var username: String

    @EventListener(NewMessageEvent::class)
    suspend fun handle(event: NewMessageEvent): CommandEvent? {
        return extractCommand(event.message)?.let { command ->
            if (filters.all { it.test(command) }) command else null
        }.also { command ->
            command?.let { cmd -> handlers.forEach { it.handle(cmd) } }
        }
    }

    private suspend fun extractCommand(message: Message): CommandEvent? {
        if (!this::username.isInitialized) {
            val result = client.send(GetMe())
            if (result is User) {
                username = result.username
            } else return null
        }
        return when (val content = message.content) {
            is MessageText -> extractCommand(content.text)
            is MessageAnimation -> extractCommand(content.caption)
            is MessageAudio -> extractCommand(content.caption)
            is MessageDocument -> extractCommand(content.caption)
            is MessagePhoto -> extractCommand(content.caption)
            is MessageVideo -> extractCommand(content.caption)
            is MessageVoiceNote -> extractCommand(content.caption)
            else -> emptySequence()
        }.map { CommandEvent(it.first, it.second, message) }.firstOrNull()
    }

    private fun extractCommand(formattedText: FormattedText) =
            formattedText.entities.asSequence()
                    .filter { it.type is TextEntityTypeBotCommand }
                    .map { it to formattedText.text.substring(it.offset, it.offset + it.length) }
                    .filter { !it.second.contains('@') || it.second.substringAfter('@') == username }
                    .map { it.first to it.second.substringBefore('@') }
                    .map { it.second to extractArgs(formattedText.text, it.first) }

    private fun extractArgs(text: String, entity: TextEntity) =
            text.substring(entity.offset + entity.length).trim().split(' ').filter { it.isNotBlank() }

}

data class CommandEvent(val command: String, val args: List<String>, val message: Message)

interface CommandEventFilter : EventFilter<CommandEvent>

@Order(0)
@Component
@ConditionalOnProperty(name = ["telegram.starter.filter-outgoing-events"], havingValue = "true", matchIfMissing = true)
class OutgoingCommandEventFilter : CommandEventFilter {
    override suspend fun test(t: CommandEvent) = !t.message.isOutgoing
}

interface CommandEventHandler {
    suspend fun handle(event: CommandEvent)
}

interface CommandHandler {
    val command: String
    suspend fun handle(args: List<String>, message: Message)
}

@Component
class CommandEventListener(handlers: List<CommandHandler>) {
    private val handlersMap = handlers.map { it.command to it }.toMap()

    @EventListener(CommandEvent::class)
    suspend fun onApplicationEvent(event: CommandEvent) {
        handlersMap[event.command]?.handle(event.args, event.message)
    }
}
