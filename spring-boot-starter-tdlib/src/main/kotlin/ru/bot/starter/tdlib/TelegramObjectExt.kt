package ru.bot.starter.tdlib

fun TelegramObject.toPlainString(): String {
    return toString().replace("\n\\s*".toRegex(), " ")
}
