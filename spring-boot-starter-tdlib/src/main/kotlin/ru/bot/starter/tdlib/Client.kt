package ru.bot.starter.tdlib

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import ru.bot.starter.handler.ErrorHandler
import ru.bot.starter.handler.ExceptionHandler
import ru.bot.starter.handler.ResultHandler
import ru.bot.starter.handler.TelegramException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.locks.ReadWriteLock
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.thread

typealias NativeClient = org.drinkless.tdlib.Client

/**
 * Main class for interaction with the TDLib.
 */
class Client(
        updatesHandler: ResultHandler?,
        updateExceptionHandler: ExceptionHandler?,
        private var errorHandler: ErrorHandler?,
        private var defaultExceptionHandler: ExceptionHandler? = null
) {

    private val logger = KotlinLogging.logger { }

    private val nativeClientId: Long = NativeClient.createNativeClient()

    private val sendChannel = Channel<Pair<Long, TelegramFunction>>(Channel.UNLIMITED)

    private val handlers = ConcurrentHashMap<Long, Handler>().apply {
        put(0L, Handler(updatesHandler, updateExceptionHandler))
    }

    private val deferredResults = ConcurrentHashMap<Long, CompletableDeferred<TelegramObject>>()

    private val maxEvents = 1000

    private val eventIds = LongArray(maxEvents)

    private val events = arrayOfNulls<TelegramObject>(maxEvents)

    private val stopFlag = AtomicBoolean()

    private val clientDestroyed = AtomicBoolean()

    private val currentQueryId = AtomicLong()

    private val readWriteLock: ReadWriteLock = ReentrantReadWriteLock()

    private val readLock = readWriteLock.readLock()

    private val writeLock = readWriteLock.writeLock()

    /**
     * Sends a request to the TDLib with an empty ExceptionHandler.
     *
     * @param query Object representing a query to the TDLib.
     */
    suspend fun send(query: TelegramFunction) = send(query, null, null)

    /**
     * Sends a request to the TDLib with an empty ExceptionHandler.
     *
     * @param query         Object representing a query to the TDLib.
     * @param resultHandler Result handler with onResult method which will be called with result
     *                      of the query or with TdApi.Error as parameter. If it is null, then
     *                      defaultExceptionHandler will be called.
     */
    suspend fun send(query: TelegramFunction, resultHandler: ResultHandler) =
            send(query, resultHandler, null)

    /**
     * Sends a request to the TDLib.
     *
     * @param query            Object representing a query to the TDLib.
     * @param resultHandler    Result handler with onResult method which will be called with result
     *                         of the query or with TdApi.Error as parameter. If it is null, nothing
     *                         will be called.
     * @param exceptionHandler Exception handler with onException method which will be called on
     *                         exception thrown from resultHandler. If it is null, then
     *                         defaultExceptionHandler will be called.
     */
    suspend fun send(query: TelegramFunction,
                     resultHandler: ResultHandler?,
                     exceptionHandler: ExceptionHandler?
    ): TelegramObject {
        val handler = Handler(resultHandler, exceptionHandler)
        val result = if (clientDestroyed.get()) {
            val telegramError = TelegramError(500, "Client is closed")
            handleResult(telegramError, handler)
            telegramError
        } else {
            val deferred = CompletableDeferred<TelegramObject>()
            val queryId = currentQueryId.incrementAndGet()
            handlers[queryId] = handler
            deferredResults[queryId] = deferred
            sendChannel.send(queryId to query)
            logger.debug { "sent to the channel: queryId=$queryId, query=${query.toPlainString()}" }
            deferred.await()
        }
        if (result is TelegramError) {
            throw TelegramException(result)
        }
        return result
    }

    /**
     * Starts Client.
     */
    fun start() {
        thread(start = true, name = "TDLib receive") {
            while (!stopFlag.get()) {
                logger.debug("receiving queries")
                receiveQueries()
                logger.debug("received queries")
            }
        }
        thread(start = true, name = "TDLib send") {
            runBlocking {
                for ((queryId, query) in sendChannel) {
                    readLock.lock()
                    try {
                        if (!clientDestroyed.get()) {
                            logger.debug { "received from the channel: queryId=$queryId, query=${query.javaClass}" }
                            NativeClient.nativeClientSend(nativeClientId, queryId, query)
                            logger.debug { "sent: queryId=$queryId, query=${query.javaClass}" }
                        }
                    } finally {
                        readLock.unlock()
                    }
                }
            }
        }
    }

    /**
     * Closes Client.
     */
    fun close() {
        writeLock.lock()
        try {
            if (clientDestroyed.get()) {
                return
            }
            if (!stopFlag.get()) {
                val queryId = currentQueryId.incrementAndGet()
                NativeClient.nativeClientSend(nativeClientId, queryId, Close())
            }
            clientDestroyed.set(true)
            while (!stopFlag.get()) {
                Thread.yield()
            }
            while (handlers.size != 1) {
                receiveQueries()
            }
            NativeClient.destroyNativeClient(nativeClientId)
        } finally {
            writeLock.unlock()
        }
    }

    protected fun finalize() {
        close()
    }

    private fun receiveQueries() {
        val count = NativeClient.nativeClientReceive(nativeClientId, eventIds, events, 300.0)
        for (i in 0 until count) {
            processResult(eventIds[i], events[i]!!)
            events[i] = null
        }
    }

    private fun processResult(id: Long, result: TelegramObject) {
        logger.debug { "received answer: queryId=$id, query=${result.toPlainString()}" }
        if (result is UpdateAuthorizationState && result.authorizationState is AuthorizationStateClosed) {
            stopFlag.set(true)
        }
        if (id == 0L) { // update handler stays forever
            handlers[id]
        } else {
            deferredResults.remove(id)?.complete(result)
            handlers.remove(id)
        }?.let {
            GlobalScope.launch {
                handleResult(result, it)
            }
        }
    }

    private suspend fun handleResult(result: TelegramObject, handler: Handler) {
        try {
            if (result is TelegramError) {
                errorHandler?.handle(result)
            }
            handler.resultHandler?.onResult(result)
        } catch (cause: Throwable) {
            try {
                (handler.exceptionHandler ?: defaultExceptionHandler)?.onException(cause)
            } catch (ex: Throwable) {
                logger.error("Unexpected error occurred", ex)
            }
        }
    }

    private class Handler(val resultHandler: ResultHandler?, val exceptionHandler: ExceptionHandler?)

    companion object {
        /**
         * Synchronously executes a TDLib request. Only a few marked accordingly requests can be executed synchronously.
         *
         * @param query Object representing a query to the TDLib.
         * @return request result.
         */
        fun execute(query: TelegramFunction): TelegramObject = NativeClient.nativeClientExecute(query)
    }

}
