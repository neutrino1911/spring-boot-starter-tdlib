@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package ru.bot.starter.util

import ru.bot.starter.tdlib.Client
import ru.bot.starter.tdlib.FormattedText
import ru.bot.starter.tdlib.InputMessageText
import ru.bot.starter.tdlib.Message
import ru.bot.starter.tdlib.ParseTextEntities
import ru.bot.starter.tdlib.SendMessage
import ru.bot.starter.tdlib.TextParseModeMarkdown

suspend fun sendMessage(message: Message, init: suspend SendMessageDsl.() -> Unit): SendMessageDsl {
    val obj = SendMessageDsl()
    obj.chatId = message.chatId
    obj.replyToMessageId = message.id
    obj.init()
    return obj
}

suspend fun sendMessage(init: suspend SendMessageDsl.() -> Unit): SendMessageDsl {
    val obj = SendMessageDsl()
    obj.init()
    return obj
}

//DSL classes

@DslMarker
annotation class TelegramObjectMarker

@TelegramObjectMarker
class SendMessageDsl : SendMessage() {

    suspend fun inputMessageText(init: suspend InputMessageTextDsl.() -> Unit) {
        val obj = InputMessageTextDsl()
        obj.init()
        inputMessageContent = obj
    }

    suspend fun formattedText(init: suspend FormattedText.() -> Unit) {
        inputMessageText {
            text = FormattedText()
            text.init()
        }
    }

    suspend fun withText(init: suspend () -> String) {
        formattedText {
            text = init()
        }
    }

    suspend fun withFormattedText(init: suspend () -> String) {
        inputMessageText {
            text = Client.execute(ParseTextEntities(init(), TextParseModeMarkdown())) as FormattedText
        }
    }

}

@TelegramObjectMarker
class InputMessageTextDsl : InputMessageText()
