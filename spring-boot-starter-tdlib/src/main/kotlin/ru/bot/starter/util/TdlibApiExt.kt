@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package ru.bot.starter.util

import kotlinx.coroutines.runBlocking
import ru.bot.starter.tdlib.ChatMembers
import ru.bot.starter.tdlib.ChatMembersFilterAdministrators
import ru.bot.starter.tdlib.Client
import ru.bot.starter.tdlib.GetChatAdministrators
import ru.bot.starter.tdlib.SearchChatMembers
import ru.bot.starter.tdlib.User
import ru.bot.starter.tdlib.Users

suspend fun Client.isAdmin(userId: Int, chatId: Long): Boolean {
    val members = send(SearchChatMembers(chatId, "", 1000, ChatMembersFilterAdministrators())) as ChatMembers
    val users = send(GetChatAdministrators(chatId)) as Users

    return members.members.any { it.userId == userId } || users.userIds.contains(userId)
}

fun Client.isAdminSync(userId: Int, chatId: Long): Boolean {
    return runBlocking {
        isAdmin(userId, chatId)
    }
}

fun User.getMention() =
        (firstName + " " + (lastName ?: "")).trim().let {
            "[$it](tg://user?id=$id)"
        }
