package ru.bot.starter.util

interface EventFilter<T> {
    suspend fun test(t: T): Boolean
}
