package ru.bot.starter.handler

import ru.bot.starter.tdlib.TelegramError

interface ErrorHandler {
    suspend fun handle(error: TelegramError)
}
