package ru.bot.starter.handler

import ru.bot.starter.tdlib.TelegramError

class DefaultErrorHandler : ErrorHandler {
    override suspend fun handle(error: TelegramError) {
        throw TelegramException(error)
    }
}
