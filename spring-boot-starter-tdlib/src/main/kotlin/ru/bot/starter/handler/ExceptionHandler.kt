package ru.bot.starter.handler

/**
 * Interface for handler of exceptions thrown while invoking ResultHandler.
 * By default, all such exceptions are ignored.
 * All exceptions thrown from ExceptionHandler are ignored.
 */
interface ExceptionHandler {
    /**
     * Callback called on exceptions thrown while invoking ResultHandler.
     *
     * @exception Exception thrown by ResultHandler.
     */
    suspend fun onException(exception: Throwable)
}
