package ru.bot.starter.handler

import ru.bot.starter.tdlib.TelegramObject

/**
 * Interface for handler for results of queries to TDLib and incoming updates from TDLib.
 */
interface ResultHandler {
    /**
     * Callback called on result of query to TDLib or incoming update from TDLib.
     *
     * @result Result of query or update of type TdApi.Update about new events.
     */
    suspend fun onResult(result: TelegramObject)
}
