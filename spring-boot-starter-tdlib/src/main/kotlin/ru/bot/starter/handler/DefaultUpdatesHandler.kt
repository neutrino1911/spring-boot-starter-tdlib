package ru.bot.starter.handler

import mu.KotlinLogging
import org.springframework.kotlin.coroutine.event.CoroutineApplicationEventPublisher
import ru.bot.starter.event.UpdateEventFactory
import ru.bot.starter.tdlib.TelegramError
import ru.bot.starter.tdlib.TelegramObject
import ru.bot.starter.tdlib.TelegramUpdate
import ru.bot.starter.tdlib.toPlainString

class DefaultUpdatesHandler(
        private val publisher: CoroutineApplicationEventPublisher
) : ResultHandler {
    private val logger = KotlinLogging.logger { }
    override suspend fun onResult(result: TelegramObject) {
        logger.debug { result.toPlainString() }
        when (result) {
            is TelegramError -> logger.error {
                "${result.code}:${result.message.replace("\n\\s*".toRegex(), " ")}"
            }
            is TelegramUpdate -> publisher.publishEvent(UpdateEventFactory.createEvent(result))
        }
    }
}
