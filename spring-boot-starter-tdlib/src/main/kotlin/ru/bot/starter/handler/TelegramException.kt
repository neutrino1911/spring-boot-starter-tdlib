package ru.bot.starter.handler

import ru.bot.starter.tdlib.TelegramError

class TelegramException(message: String, private val error: TelegramError) : RuntimeException(message) {
    constructor(error: TelegramError) : this("[${error.code}] ${error.message}", error)

    override fun getLocalizedMessage() = "[${error.code}] ${error.message}"
}
