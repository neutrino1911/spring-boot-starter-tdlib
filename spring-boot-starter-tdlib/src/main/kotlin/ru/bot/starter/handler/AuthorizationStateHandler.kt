package ru.bot.starter.handler

import kotlinx.coroutines.delay
import mu.KotlinLogging
import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import ru.bot.starter.config.TelegramProperties
import ru.bot.starter.event.UpdateAuthorizationStateEvent
import ru.bot.starter.tdlib.*
import java.lang.Long.parseLong
import java.util.concurrent.atomic.AtomicInteger
import kotlin.system.exitProcess

@Component
class AuthorizationStateHandler(
        val clientProvider: ObjectFactory<Client>,
        val props: TelegramProperties
) {

    private val logger = KotlinLogging.logger { }

    private val client by lazy { clientProvider.`object` }

    private val errorCounter = AtomicInteger()

    private lateinit var state: AuthorizationState

    @EventListener(UpdateAuthorizationStateEvent::class)
    suspend fun onApplicationEvent(event: UpdateAuthorizationStateEvent) {
        state = event.update.authorizationState
        handleState(state)
    }

    private suspend fun handleState(state: AuthorizationState) {
        logger.debug { "authorization state: ${state.javaClass.simpleName}" }
        when (state) {
            is AuthorizationStateWaitTdlibParameters -> {
                val parameters = TdlibParameters(
                        props.tdlib.useTestDc,
                        props.tdlib.databaseDirectory,
                        props.tdlib.filesDirectory,
                        props.tdlib.useFileDatabase,
                        props.tdlib.useChatInfoDatabase,
                        props.tdlib.useMessageDatabase,
                        props.tdlib.useSecretChats,
                        props.tdlib.apiId,
                        props.tdlib.apiHash,
                        props.tdlib.systemLanguageCode,
                        props.tdlib.deviceModel,
                        props.tdlib.systemVersion,
                        props.tdlib.applicationVersion,
                        props.tdlib.enableStorageOptimizer,
                        props.tdlib.ignoreFileNames
                )
                handleResult(client.send(SetTdlibParameters(parameters)))
            }
            is AuthorizationStateWaitPhoneNumber -> {
                handleResult(client.send(CheckAuthenticationBotToken(props.bot.token)))
            }
            is AuthorizationStateWaitEncryptionKey -> {
                handleResult(client.send(CheckDatabaseEncryptionKey()))
            }
            is AuthorizationStateReady -> logger.info { "Logging in" }
            is AuthorizationStateLoggingOut -> logger.info { "Logging out" }
            is AuthorizationStateClosing -> logger.info { "Closing" }
            is AuthorizationStateClosed -> logger.info { "Closed" }
            else -> logger.error { "Unsupported authorization state: $state" }
        }
    }

    suspend fun handleResult(result: TelegramObject) {
        when (result) {
            is TelegramError -> {
                logger.error { "Receive an error: ${result.code}:${result.message}" }
                if (errorCounter.incrementAndGet() == 3) {
                    logger.warn { "Too many failed attempts" }
                    client.close()
                    exitProcess(-1)
                }
                if (result.code == 429) {
                    val seconds = parseLong(result.message.substring(31))
                    logger.warn { "Waiting $seconds seconds" }
                    delay(seconds * 1000 + 1000)
                }
                delay(errorCounter.get() * 5000L)
                handleState(state)
            }
            is TelegramOk -> {
                errorCounter.set(0)
                // result is already received through UpdateAuthorizationState, nothing to do
            }
            else -> logger.error { "Receive wrong response from TDLib: ${result.toPlainString()}" }
        }
    }

}
