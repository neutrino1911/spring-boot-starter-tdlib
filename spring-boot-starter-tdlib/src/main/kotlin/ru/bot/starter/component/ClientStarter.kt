package ru.bot.starter.component

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import ru.bot.starter.tdlib.Client

@Component
class ClientStarter(val client: Client) : ApplicationRunner {
    override fun run(args: ApplicationArguments) {
        client.start()
    }
}
