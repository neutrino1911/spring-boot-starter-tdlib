package ru.bot.starter.component

import mu.KotlinLogging
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import ru.bot.starter.config.TelegramProperties
import ru.bot.starter.event.UpdateConnectionStateEvent
import ru.bot.starter.tdlib.*

@Component
class ProxyConfigurer(
        val client: Client,
        val props: TelegramProperties
) {

    private val logger = KotlinLogging.logger { }

    @EventListener(UpdateConnectionStateEvent::class)
    suspend fun run(event: UpdateConnectionStateEvent) {
        if (event.update.state is ConnectionStateConnecting) {
            onResult(client.send(GetProxies()))
        }
    }

    suspend fun onResult(result: TelegramObject) {
        logger.debug { result.toPlainString() }
        val proxies = result as Proxies
        props.proxy.let { proxy ->
            if (proxy.host == null || proxy.port == null || proxy.secret == null) {
                disableProxy(proxies.proxies)
            } else {
                enableProxy(proxies.proxies)
            }
        }
    }

    private suspend fun enableProxy(proxies: Array<Proxy>) {
        for (proxy in proxies) {
            if (proxy.server == props.proxy.host
                    && proxy.port == props.proxy.port
                    && proxy.type is ProxyTypeMtproto
                    && (proxy.type as ProxyTypeMtproto).secret == props.proxy.secret
            ) {
                if (!proxy.isEnabled) {
                    client.send(EnableProxy(proxy.id))
                }
                return
            }
        }

        client.send(AddProxy().apply {
            server = props.proxy.host
            port = props.proxy.port!!
            type = ProxyTypeMtproto(props.proxy.secret)
            enable = true
        })
    }

    private suspend fun disableProxy(proxies: Array<Proxy>) {
        for (proxy in proxies) {
            client.send(RemoveProxy(proxy.id))
        }
    }

}
