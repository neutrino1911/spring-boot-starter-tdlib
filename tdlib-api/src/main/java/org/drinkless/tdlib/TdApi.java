// @formatter:off
package org.drinkless.tdlib;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * This class contains as static nested classes all other TDLib interface
 * type-classes and function-classes.
 * <p>
 * It has no inner classes, functions or public members.
 */
public class TdApi {

    /**
     * This class is a base class for all TDLib interface classes.
     */
    public abstract static class Object {
        /**
         * @return string representation of the object.
         */
        public native String toString();
        /**
         * @return identifier uniquely determining type of the object.
         */
        public abstract int getConstructor();
    }

    /**
     * This class is a base class for all TDLib interface function-classes.
     */
    public abstract static class Function extends Object {
        /**
         * @return string representation of the object.
         */
        public native String toString();
    }

    /**
     * Contains information about the period of inactivity after which the current user's account will automatically be deleted.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AccountTtl extends Object {
        /**
         * Number of days of inactivity before the account will be flagged for deletion; should range from 30-366 days.
         */
        public int days;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1324495492;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes an address.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Address extends Object {
        /**
         * A two-letter ISO 3166-1 alpha-2 country code.
         */
        public String countryCode;
        /**
         * State, if applicable.
         */
        public String state;
        /**
         * City.
         */
        public String city;
        /**
         * First line of the address.
         */
        public String streetLine1;
        /**
         * Second line of the address.
         */
        public String streetLine2;
        /**
         * Address postal code.
         */
        public String postalCode;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2043654342;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes an animation file. The animation must be encoded in GIF or MPEG4 format.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Animation extends Object {
        /**
         * Duration of the animation, in seconds; as defined by the sender.
         */
        public int duration;
        /**
         * Width of the animation.
         */
        public int width;
        /**
         * Height of the animation.
         */
        public int height;
        /**
         * Original name of the file; as defined by the sender.
         */
        public String fileName;
        /**
         * MIME type of the file, usually &quot;image/gif&quot; or &quot;video/mp4&quot;.
         */
        public String mimeType;
        /**
         * Animation minithumbnail; may be null.
         */
        public Minithumbnail minithumbnail;
        /**
         * Animation thumbnail; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * File containing the animation.
         */
        public File animation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1629245379;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a list of animations.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Animations extends Object {
        /**
         * List of animations.
         */
        public Animation[] animations;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 344216945;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes an audio file. Audio is usually in MP3 or M4A format.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Audio extends Object {
        /**
         * Duration of the audio, in seconds; as defined by the sender.
         */
        public int duration;
        /**
         * Title of the audio; as defined by the sender.
         */
        public String title;
        /**
         * Performer of the audio; as defined by the sender.
         */
        public String performer;
        /**
         * Original name of the file; as defined by the sender.
         */
        public String fileName;
        /**
         * The MIME type of the file; as defined by the sender.
         */
        public String mimeType;
        /**
         * The minithumbnail of the album cover; may be null.
         */
        public Minithumbnail albumCoverMinithumbnail;
        /**
         * The thumbnail of the album cover; as defined by the sender. The full size thumbnail should be extracted from the downloaded file; may be null.
         */
        public PhotoSize albumCoverThumbnail;
        /**
         * File containing the audio.
         */
        public File audio;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1475294302;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Information about the authentication code that was sent.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthenticationCodeInfo extends Object {
        /**
         * A phone number that is being authenticated.
         */
        public String phoneNumber;
        /**
         * Describes the way the code was sent to the user.
         */
        public AuthenticationCodeType type;
        /**
         * Describes the way the next code will be sent to the user; may be null.
         */
        public AuthenticationCodeType nextType;
        /**
         * Timeout before the code should be re-sent, in seconds.
         */
        public int timeout;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -860345416;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Provides information about the method by which an authentication code is delivered to the user.
     */
    public abstract static class AuthenticationCodeType extends Object {
    }

    /**
     * An authentication code is delivered via a private Telegram message, which can be viewed in another client.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthenticationCodeTypeTelegramMessage extends AuthenticationCodeType {
        /**
         * Length of the code.
         */
        public int length;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2079628074;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An authentication code is delivered via an SMS message to the specified phone number.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthenticationCodeTypeSms extends AuthenticationCodeType {
        /**
         * Length of the code.
         */
        public int length;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 962650760;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An authentication code is delivered via a phone call to the specified phone number.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthenticationCodeTypeCall extends AuthenticationCodeType {
        /**
         * Length of the code.
         */
        public int length;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1636265063;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An authentication code is delivered by an immediately cancelled call to the specified phone number. The number from which the call was made is the code.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthenticationCodeTypeFlashCall extends AuthenticationCodeType {
        /**
         * Pattern of the phone number from which the call will be made.
         */
        public String pattern;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1395882402;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents the current authorization state of the client.
     */
    public abstract static class AuthorizationState extends Object {
    }

    /**
     * TDLib needs TdlibParameters for initialization.
     */
    public static class AuthorizationStateWaitTdlibParameters extends AuthorizationState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 904720988;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * TDLib needs an encryption key to decrypt the local database.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthorizationStateWaitEncryptionKey extends AuthorizationState {
        /**
         * True, if the database is currently encrypted.
         */
        public boolean isEncrypted;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 612103496;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * TDLib needs the user's phone number to authorize.
     */
    public static class AuthorizationStateWaitPhoneNumber extends AuthorizationState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 306402531;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * TDLib needs the user's authentication code to authorize.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthorizationStateWaitCode extends AuthorizationState {
        /**
         * Information about the authorization code that was sent.
         */
        public AuthenticationCodeInfo codeInfo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 52643073;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is unregistered and need to accept terms of service and enter their first name and last name to finish registration.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthorizationStateWaitRegistration extends AuthorizationState {
        /**
         * Telegram terms of service.
         */
        public TermsOfService termsOfService;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 550350511;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user has been authorized, but needs to enter a password to start using the application.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AuthorizationStateWaitPassword extends AuthorizationState {
        /**
         * Hint for the password; may be empty.
         */
        public String passwordHint;
        /**
         * True, if a recovery email address has been set up.
         */
        public boolean hasRecoveryEmailAddress;
        /**
         * Pattern of the email address to which the recovery email was sent; empty until a recovery email has been sent.
         */
        public String recoveryEmailAddressPattern;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 187548796;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user has been successfully authorized. TDLib is now ready to answer queries.
     */
    public static class AuthorizationStateReady extends AuthorizationState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1834871737;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is currently logging out.
     */
    public static class AuthorizationStateLoggingOut extends AuthorizationState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 154449270;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * TDLib is closing, all subsequent queries will be answered with the error 500. Note that closing TDLib can take a while. All resources will be freed only after authorizationStateClosed has been received.
     */
    public static class AuthorizationStateClosing extends AuthorizationState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 445855311;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * TDLib client is in its final state. All databases are closed and all resources are released. No other updates will be received after this. All queries will be responded to with error code 500. To continue working, one should create a new instance of the TDLib client.
     */
    public static class AuthorizationStateClosed extends AuthorizationState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1526047584;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains auto-download settings.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AutoDownloadSettings extends Object {
        /**
         * True, if the auto-download is enabled.
         */
        public boolean isAutoDownloadEnabled;
        /**
         * Maximum size of a photo file to be auto-downloaded.
         */
        public int maxPhotoFileSize;
        /**
         * Maximum size of a video file to be auto-downloaded.
         */
        public int maxVideoFileSize;
        /**
         * Maximum size of other file types to be auto-downloaded.
         */
        public int maxOtherFileSize;
        /**
         * True, if the beginning of videos needs to be preloaded for instant playback.
         */
        public boolean preloadLargeVideos;
        /**
         * True, if the next audio track needs to be preloaded while the user is listening to an audio file.
         */
        public boolean preloadNextAudio;
        /**
         * True, if &quot;use less data for calls&quot; option needs to be enabled.
         */
        public boolean useLessDataForCalls;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1086183818;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains auto-download settings presets for the user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AutoDownloadSettingsPresets extends Object {
        /**
         * Preset with lowest settings; supposed to be used by default when roaming.
         */
        public AutoDownloadSettings low;
        /**
         * Preset with medium settings; supposed to be used by default when using mobile data.
         */
        public AutoDownloadSettings medium;
        /**
         * Preset with highest settings; supposed to be used by default when connected on Wi-Fi.
         */
        public AutoDownloadSettings high;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -782099166;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a chat background.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Background extends Object {
        /**
         * Unique background identifier.
         */
        public long id;
        /**
         * True, if this is one of default backgrounds.
         */
        public boolean isDefault;
        /**
         * True, if the background is dark and is recommended to be used with dark theme.
         */
        public boolean isDark;
        /**
         * Unique background name.
         */
        public String name;
        /**
         * Document with the background; may be null. Null only for solid backgrounds.
         */
        public Document document;
        /**
         * Type of the background.
         */
        public BackgroundType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -429971172;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes a type of a background.
     */
    public abstract static class BackgroundType extends Object {
    }

    /**
     * A wallpaper in JPEG format.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BackgroundTypeWallpaper extends BackgroundType {
        /**
         * True, if the wallpaper must be downscaled to fit in 450x450 square and then box-blurred with radius 12.
         */
        public boolean isBlurred;
        /**
         * True, if the background needs to be slightly moved when device is rotated.
         */
        public boolean isMoving;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1972128891;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A PNG pattern to be combined with the color chosen by the user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BackgroundTypePattern extends BackgroundType {
        /**
         * True, if the background needs to be slightly moved when device is rotated.
         */
        public boolean isMoving;
        /**
         * Main color of the background in RGB24 format.
         */
        public int color;
        /**
         * Intensity of the pattern when it is shown above the main background color, 0-100.
         */
        public int intensity;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1091944673;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A solid background.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BackgroundTypeSolid extends BackgroundType {
        /**
         * A color of the background in RGB24 format.
         */
        public int color;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -31192323;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of backgrounds.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Backgrounds extends Object {
        /**
         * A list of backgrounds.
         */
        public Background[] backgrounds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 724728704;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a basic group of 0-200 users (must be upgraded to a supergroup to accommodate more than 200 users).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BasicGroup extends Object {
        /**
         * Group identifier.
         */
        public int id;
        /**
         * Number of members in the group.
         */
        public int memberCount;
        /**
         * Status of the current user in the group.
         */
        public ChatMemberStatus status;
        /**
         * True, if the group is active.
         */
        public boolean isActive;
        /**
         * Identifier of the supergroup to which this group was upgraded; 0 if none.
         */
        public int upgradedToSupergroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -317839045;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains full information about a basic group.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BasicGroupFullInfo extends Object {
        /**
         * Group description.
         */
        public String description;
        /**
         * User identifier of the creator of the group; 0 if unknown.
         */
        public int creatorUserId;
        /**
         * Group members.
         */
        public ChatMember[] members;
        /**
         * Invite link for this group; available only for the group creator and only after it has been generated at least once.
         */
        public String inviteLink;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 161500149;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents commands supported by a bot.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BotCommand extends Object {
        /**
         * Text of the bot command.
         */
        public String command;
        /**
         * Description of the bot command.
         */
        public String description;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1032140601;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Provides information about a bot and its supported commands.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BotInfo extends Object {
        /**
         * Long description shown on the user info page.
         */
        public String description;
        /**
         * A list of commands supported by the bot.
         */
        public BotCommand[] commands;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1296528907;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a call.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Call extends Object {
        /**
         * Call identifier, not persistent.
         */
        public int id;
        /**
         * Peer user identifier.
         */
        public int userId;
        /**
         * True, if the call is outgoing.
         */
        public boolean isOutgoing;
        /**
         * Call state.
         */
        public CallState state;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1837599107;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes the address of UDP reflectors.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallConnection extends Object {
        /**
         * Reflector identifier.
         */
        public long id;
        /**
         * IPv4 reflector address.
         */
        public String ip;
        /**
         * IPv6 reflector address.
         */
        public String ipv6;
        /**
         * Reflector port number.
         */
        public int port;
        /**
         * Connection peer tag.
         */
        public byte[] peerTag;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1318542714;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the reason why a call was discarded.
     */
    public abstract static class CallDiscardReason extends Object {
    }

    /**
     * The call wasn't discarded, or the reason is unknown.
     */
    public static class CallDiscardReasonEmpty extends CallDiscardReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1258917949;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call was ended before the conversation started. It was cancelled by the caller or missed by the other party.
     */
    public static class CallDiscardReasonMissed extends CallDiscardReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1680358012;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call was ended before the conversation started. It was declined by the other party.
     */
    public static class CallDiscardReasonDeclined extends CallDiscardReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1729926094;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call was ended during the conversation because the users were disconnected.
     */
    public static class CallDiscardReasonDisconnected extends CallDiscardReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1342872670;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call was ended because one of the parties hung up.
     */
    public static class CallDiscardReasonHungUp extends CallDiscardReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 438216166;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the call identifier.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallId extends Object {
        /**
         * Call identifier.
         */
        public int id;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 65717769;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the exact type of a problem with a call.
     */
    public abstract static class CallProblem extends Object {
    }

    /**
     * The user heard their own voice.
     */
    public static class CallProblemEcho extends CallProblem {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 801116548;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user heard background noise.
     */
    public static class CallProblemNoise extends CallProblem {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1053065359;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The other side kept disappearing.
     */
    public static class CallProblemInterruptions extends CallProblem {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1119493218;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The speech was distorted.
     */
    public static class CallProblemDistortedSpeech extends CallProblem {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 379960581;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user couldn't hear the other side.
     */
    public static class CallProblemSilentLocal extends CallProblem {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 253652790;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The other side couldn't hear the user.
     */
    public static class CallProblemSilentRemote extends CallProblem {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 573634714;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call ended unexpectedly.
     */
    public static class CallProblemDropped extends CallProblem {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1207311487;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Specifies the supported call protocols.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallProtocol extends Object {
        /**
         * True, if UDP peer-to-peer connections are supported.
         */
        public boolean udpP2p;
        /**
         * True, if connection through UDP reflectors is supported.
         */
        public boolean udpReflector;
        /**
         * Minimum supported API layer; use 65.
         */
        public int minLayer;
        /**
         * Maximum supported API layer; use 65.
         */
        public int maxLayer;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1042830667;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the current call state.
     */
    public abstract static class CallState extends Object {
    }

    /**
     * The call is pending, waiting to be accepted by a user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallStatePending extends CallState {
        /**
         * True, if the call has already been created by the server.
         */
        public boolean isCreated;
        /**
         * True, if the call has already been received by the other party.
         */
        public boolean isReceived;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1073048620;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call has been answered and encryption keys are being exchanged.
     */
    public static class CallStateExchangingKeys extends CallState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1848149403;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call is ready to use.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallStateReady extends CallState {
        /**
         * Call protocols supported by the peer.
         */
        public CallProtocol protocol;
        /**
         * Available UDP reflectors.
         */
        public CallConnection[] connections;
        /**
         * A JSON-encoded call config.
         */
        public String config;
        /**
         * Call encryption key.
         */
        public byte[] encryptionKey;
        /**
         * Encryption key emojis fingerprint.
         */
        public String[] emojis;
        /**
         * True, if peer-to-peer connection is allowed by users privacy settings.
         */
        public boolean allowP2p;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1848397705;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call is hanging up after discardCall has been called.
     */
    public static class CallStateHangingUp extends CallState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2133790038;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call has ended successfully.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallStateDiscarded extends CallState {
        /**
         * The reason, why the call has ended.
         */
        public CallDiscardReason reason;
        /**
         * True, if the call rating should be sent to the server.
         */
        public boolean needRating;
        /**
         * True, if the call debug information should be sent to the server.
         */
        public boolean needDebugInformation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -190853167;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The call has ended with an error.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallStateError extends CallState {
        /**
         * Error. An error with the code 4005000 will be returned if an outgoing call is missed because of an expired timeout.
         */
        public Error error;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -975215467;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a bot's answer to a callback query.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallbackQueryAnswer extends Object {
        /**
         * Text of the answer.
         */
        public String text;
        /**
         * True, if an alert should be shown to the user instead of a toast notification.
         */
        public boolean showAlert;
        /**
         * URL to be opened.
         */
        public String url;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 360867933;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a payload of a callback query.
     */
    public abstract static class CallbackQueryPayload extends Object {
    }

    /**
     * The payload from a general callback button.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallbackQueryPayloadData extends CallbackQueryPayload {
        /**
         * Data that was attached to the callback button.
         */
        public byte[] data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1977729946;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The payload from a game callback button.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CallbackQueryPayloadGame extends CallbackQueryPayload {
        /**
         * A short name of the game that was attached to the callback button.
         */
        public String gameShortName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1303571512;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat. (Can be a private chat, basic group, supergroup, or secret chat.)
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Chat extends Object {
        /**
         * Chat unique identifier.
         */
        public long id;
        /**
         * Type of the chat.
         */
        public ChatType type;
        /**
         * Chat title.
         */
        public String title;
        /**
         * Chat photo; may be null.
         */
        public ChatPhoto photo;
        /**
         * Actions that non-administrator chat members are allowed to take in the chat.
         */
        public ChatPermissions permissions;
        /**
         * Last message in the chat; may be null.
         */
        public Message lastMessage;
        /**
         * Descending parameter by which chats are sorted in the main chat list. If the order number of two chats is the same, they must be sorted in descending order by ID. If 0, the position of the chat in the list is undetermined.
         */
        public long order;
        /**
         * True, if the chat is pinned.
         */
        public boolean isPinned;
        /**
         * True, if the chat is marked as unread.
         */
        public boolean isMarkedAsUnread;
        /**
         * True, if the chat is sponsored by the user's MTProxy server.
         */
        public boolean isSponsored;
        /**
         * True, if the chat messages can be deleted only for the current user while other users will continue to see the messages.
         */
        public boolean canBeDeletedOnlyForSelf;
        /**
         * True, if the chat messages can be deleted for all users.
         */
        public boolean canBeDeletedForAllUsers;
        /**
         * True, if the chat can be reported to Telegram moderators through reportChat.
         */
        public boolean canBeReported;
        /**
         * Default value of the disableNotification parameter, used when a message is sent to the chat.
         */
        public boolean defaultDisableNotification;
        /**
         * Number of unread messages in the chat.
         */
        public int unreadCount;
        /**
         * Identifier of the last read incoming message.
         */
        public long lastReadInboxMessageId;
        /**
         * Identifier of the last read outgoing message.
         */
        public long lastReadOutboxMessageId;
        /**
         * Number of unread messages with a mention/reply in the chat.
         */
        public int unreadMentionCount;
        /**
         * Notification settings for this chat.
         */
        public ChatNotificationSettings notificationSettings;
        /**
         * Identifier of the pinned message in the chat; 0 if none.
         */
        public long pinnedMessageId;
        /**
         * Identifier of the message from which reply markup needs to be used; 0 if there is no default custom reply markup in the chat.
         */
        public long replyMarkupMessageId;
        /**
         * A draft of a message in the chat; may be null.
         */
        public DraftMessage draftMessage;
        /**
         * Contains client-specific data associated with the chat. (For example, the chat position or local chat notification settings can be stored here.) Persistent if a message database is used.
         */
        public String clientData;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1433927525;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the different types of activity in a chat.
     */
    public abstract static class ChatAction extends Object {
    }

    /**
     * The user is typing a message.
     */
    public static class ChatActionTyping extends ChatAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 380122167;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is recording a video.
     */
    public static class ChatActionRecordingVideo extends ChatAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 216553362;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is uploading a video.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatActionUploadingVideo extends ChatAction {
        /**
         * Upload progress, as a percentage.
         */
        public int progress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1234185270;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is recording a voice note.
     */
    public static class ChatActionRecordingVoiceNote extends ChatAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -808850058;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is uploading a voice note.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatActionUploadingVoiceNote extends ChatAction {
        /**
         * Upload progress, as a percentage.
         */
        public int progress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -613643666;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is uploading a photo.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatActionUploadingPhoto extends ChatAction {
        /**
         * Upload progress, as a percentage.
         */
        public int progress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 654240583;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is uploading a document.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatActionUploadingDocument extends ChatAction {
        /**
         * Upload progress, as a percentage.
         */
        public int progress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 167884362;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is picking a location or venue to send.
     */
    public static class ChatActionChoosingLocation extends ChatAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2017893596;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is picking a contact to send.
     */
    public static class ChatActionChoosingContact extends ChatAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1222507496;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user has started to play a game.
     */
    public static class ChatActionStartPlayingGame extends ChatAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -865884164;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is recording a video note.
     */
    public static class ChatActionRecordingVideoNote extends ChatAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 16523393;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is uploading a video note.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatActionUploadingVideoNote extends ChatAction {
        /**
         * Upload progress, as a percentage.
         */
        public int progress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1172364918;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user has cancelled the previous action.
     */
    public static class ChatActionCancel extends ChatAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1160523958;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a chat event.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEvent extends Object {
        /**
         * Chat event identifier.
         */
        public long id;
        /**
         * Point in time (Unix timestamp) when the event happened.
         */
        public int date;
        /**
         * Identifier of the user who performed the action that triggered the event.
         */
        public int userId;
        /**
         * Action performed by the user.
         */
        public ChatEventAction action;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -609912404;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a chat event.
     */
    public abstract static class ChatEventAction extends Object {
    }

    /**
     * A message was edited.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventMessageEdited extends ChatEventAction {
        /**
         * The original message before the edit.
         */
        public Message oldMessage;
        /**
         * The message after it was edited.
         */
        public Message newMessage;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -430967304;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message was deleted.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventMessageDeleted extends ChatEventAction {
        /**
         * Deleted message.
         */
        public Message message;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -892974601;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A poll in a message was stopped.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventPollStopped extends ChatEventAction {
        /**
         * The message with the poll.
         */
        public Message message;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2009893861;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message was pinned.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventMessagePinned extends ChatEventAction {
        /**
         * Pinned message.
         */
        public Message message;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 438742298;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message was unpinned.
     */
    public static class ChatEventMessageUnpinned extends ChatEventAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2002594849;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new member joined the chat.
     */
    public static class ChatEventMemberJoined extends ChatEventAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -235468508;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A member left the chat.
     */
    public static class ChatEventMemberLeft extends ChatEventAction {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -948420593;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new chat member was invited.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventMemberInvited extends ChatEventAction {
        /**
         * New member user identifier.
         */
        public int userId;
        /**
         * New member status.
         */
        public ChatMemberStatus status;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2093688706;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat member has gained/lost administrator status, or the list of their administrator privileges has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventMemberPromoted extends ChatEventAction {
        /**
         * Chat member user identifier.
         */
        public int userId;
        /**
         * Previous status of the chat member.
         */
        public ChatMemberStatus oldStatus;
        /**
         * New status of the chat member.
         */
        public ChatMemberStatus newStatus;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1887176186;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat member was restricted/unrestricted or banned/unbanned, or the list of their restrictions has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventMemberRestricted extends ChatEventAction {
        /**
         * Chat member user identifier.
         */
        public int userId;
        /**
         * Previous status of the chat member.
         */
        public ChatMemberStatus oldStatus;
        /**
         * New status of the chat member.
         */
        public ChatMemberStatus newStatus;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 584946294;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat title was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventTitleChanged extends ChatEventAction {
        /**
         * Previous chat title.
         */
        public String oldTitle;
        /**
         * New chat title.
         */
        public String newTitle;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1134103250;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat permissions was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventPermissionsChanged extends ChatEventAction {
        /**
         * Previous chat permissions.
         */
        public ChatPermissions oldPermissions;
        /**
         * New chat permissions.
         */
        public ChatPermissions newPermissions;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1311557720;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat description was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventDescriptionChanged extends ChatEventAction {
        /**
         * Previous chat description.
         */
        public String oldDescription;
        /**
         * New chat description.
         */
        public String newDescription;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 39112478;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat username was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventUsernameChanged extends ChatEventAction {
        /**
         * Previous chat username.
         */
        public String oldUsername;
        /**
         * New chat username.
         */
        public String newUsername;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1728558443;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat photo was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventPhotoChanged extends ChatEventAction {
        /**
         * Previous chat photo value; may be null.
         */
        public Photo oldPhoto;
        /**
         * New chat photo value; may be null.
         */
        public Photo newPhoto;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1037662734;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The canInviteUsers permission of a supergroup chat was toggled.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventInvitesToggled extends ChatEventAction {
        /**
         * New value of canInviteUsers permission.
         */
        public boolean canInviteUsers;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -62548373;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The signMessages setting of a channel was toggled.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventSignMessagesToggled extends ChatEventAction {
        /**
         * New value of signMessages.
         */
        public boolean signMessages;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1313265634;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The supergroup sticker set was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventStickerSetChanged extends ChatEventAction {
        /**
         * Previous identifier of the chat sticker set; 0 if none.
         */
        public long oldStickerSetId;
        /**
         * New identifier of the chat sticker set; 0 if none.
         */
        public long newStickerSetId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1243130481;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The isAllHistoryAvailable setting of a supergroup was toggled.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventIsAllHistoryAvailableToggled extends ChatEventAction {
        /**
         * New value of isAllHistoryAvailable.
         */
        public boolean isAllHistoryAvailable;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1599063019;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a set of filters used to obtain a chat event log.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEventLogFilters extends Object {
        /**
         * True, if message edits should be returned.
         */
        public boolean messageEdits;
        /**
         * True, if message deletions should be returned.
         */
        public boolean messageDeletions;
        /**
         * True, if pin/unpin events should be returned.
         */
        public boolean messagePins;
        /**
         * True, if members joining events should be returned.
         */
        public boolean memberJoins;
        /**
         * True, if members leaving events should be returned.
         */
        public boolean memberLeaves;
        /**
         * True, if invited member events should be returned.
         */
        public boolean memberInvites;
        /**
         * True, if member promotion/demotion events should be returned.
         */
        public boolean memberPromotions;
        /**
         * True, if member restricted/unrestricted/banned/unbanned events should be returned.
         */
        public boolean memberRestrictions;
        /**
         * True, if changes in chat information should be returned.
         */
        public boolean infoChanges;
        /**
         * True, if changes in chat settings should be returned.
         */
        public boolean settingChanges;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 941939684;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of chat events.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatEvents extends Object {
        /**
         * List of events.
         */
        public ChatEvent[] events;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -585329664;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a chat invite link.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatInviteLink extends Object {
        /**
         * Chat invite link.
         */
        public String inviteLink;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -882072492;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a chat invite link.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatInviteLinkInfo extends Object {
        /**
         * Chat identifier of the invite link; 0 if the user is not a member of this chat.
         */
        public long chatId;
        /**
         * Contains information about the type of the chat.
         */
        public ChatType type;
        /**
         * Title of the chat.
         */
        public String title;
        /**
         * Chat photo; may be null.
         */
        public ChatPhoto photo;
        /**
         * Number of members.
         */
        public int memberCount;
        /**
         * User identifiers of some chat members that may be known to the current user.
         */
        public int[] memberUserIds;
        /**
         * True, if the chat is a public supergroup or a channel with a username.
         */
        public boolean isPublic;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -323394424;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A user with information about joining/leaving a chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatMember extends Object {
        /**
         * User identifier of the chat member.
         */
        public int userId;
        /**
         * Identifier of a user that invited/promoted/banned this member in the chat; 0 if unknown.
         */
        public int inviterUserId;
        /**
         * Point in time (Unix timestamp) when the user joined a chat.
         */
        public int joinedChatDate;
        /**
         * Status of the member in the chat.
         */
        public ChatMemberStatus status;
        /**
         * If the user is a bot, information about the bot; may be null. Can be null even for a bot if the bot is not a chat member.
         */
        public BotInfo botInfo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -806137076;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Provides information about the status of a member in a chat.
     */
    public abstract static class ChatMemberStatus extends Object {
    }

    /**
     * The user is the creator of a chat and has all the administrator privileges.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatMemberStatusCreator extends ChatMemberStatus {
        /**
         * True, if the user is a member of the chat.
         */
        public boolean isMember;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1756320508;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is a member of a chat and has some additional privileges. In basic groups, administrators can edit and delete messages sent by others, add new members, and ban unprivileged members. In supergroups and channels, there are more detailed options for administrator privileges.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatMemberStatusAdministrator extends ChatMemberStatus {
        /**
         * True, if the current user can edit the administrator privileges for the called user.
         */
        public boolean canBeEdited;
        /**
         * True, if the administrator can change the chat title, photo, and other settings.
         */
        public boolean canChangeInfo;
        /**
         * True, if the administrator can create channel posts; applicable to channels only.
         */
        public boolean canPostMessages;
        /**
         * True, if the administrator can edit messages of other users and pin messages; applicable to channels only.
         */
        public boolean canEditMessages;
        /**
         * True, if the administrator can delete messages of other users.
         */
        public boolean canDeleteMessages;
        /**
         * True, if the administrator can invite new users to the chat.
         */
        public boolean canInviteUsers;
        /**
         * True, if the administrator can restrict, ban, or unban chat members.
         */
        public boolean canRestrictMembers;
        /**
         * True, if the administrator can pin messages; applicable to groups only.
         */
        public boolean canPinMessages;
        /**
         * True, if the administrator can add new administrators with a subset of their own privileges or demote administrators that were directly or indirectly promoted by him.
         */
        public boolean canPromoteMembers;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 45106688;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is a member of a chat, without any additional privileges or restrictions.
     */
    public static class ChatMemberStatusMember extends ChatMemberStatus {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 844723285;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is under certain restrictions in the chat. Not supported in basic groups and channels.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatMemberStatusRestricted extends ChatMemberStatus {
        /**
         * True, if the user is a member of the chat.
         */
        public boolean isMember;
        /**
         * Point in time (Unix timestamp) when restrictions will be lifted from the user; 0 if never. If the user is restricted for more than 366 days or for less than 30 seconds from the current time, the user is considered to be restricted forever.
         */
        public int restrictedUntilDate;
        /**
         * User permissions in the chat.
         */
        public ChatPermissions permissions;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1661432998;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is not a chat member.
     */
    public static class ChatMemberStatusLeft extends ChatMemberStatus {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -5815259;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user was banned (and hence is not a member of the chat). Implies the user can't return to the chat or view messages.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatMemberStatusBanned extends ChatMemberStatus {
        /**
         * Point in time (Unix timestamp) when the user will be unbanned; 0 if never. If the user is banned for more than 366 days or for less than 30 seconds from the current time, the user is considered to be banned forever.
         */
        public int bannedUntilDate;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1653518666;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of chat members.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatMembers extends Object {
        /**
         * Approximate total count of chat members found.
         */
        public int totalCount;
        /**
         * A list of chat members.
         */
        public ChatMember[] members;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -497558622;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Specifies the kind of chat members to return in searchChatMembers.
     */
    public abstract static class ChatMembersFilter extends Object {
    }

    /**
     * Returns contacts of the user.
     */
    public static class ChatMembersFilterContacts extends ChatMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1774485671;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the creator and administrators.
     */
    public static class ChatMembersFilterAdministrators extends ChatMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1266893796;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns all chat members, including restricted chat members.
     */
    public static class ChatMembersFilterMembers extends ChatMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 670504342;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns users under certain restrictions in the chat; can be used only by administrators in a supergroup.
     */
    public static class ChatMembersFilterRestricted extends ChatMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1256282813;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns users banned from the chat; can be used only by administrators in a supergroup or in a channel.
     */
    public static class ChatMembersFilterBanned extends ChatMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1863102648;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns bot members of the chat.
     */
    public static class ChatMembersFilterBots extends ChatMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1422567288;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about notification settings for a chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatNotificationSettings extends Object {
        /**
         * If true, muteFor is ignored and the value for the relevant type of chat is used instead.
         */
        public boolean useDefaultMuteFor;
        /**
         * Time left before notifications will be unmuted, in seconds.
         */
        public int muteFor;
        /**
         * If true, sound is ignored and the value for the relevant type of chat is used instead.
         */
        public boolean useDefaultSound;
        /**
         * The name of an audio file to be used for notification sounds; only applies to iOS applications.
         */
        public String sound;
        /**
         * If true, showPreview is ignored and the value for the relevant type of chat is used instead.
         */
        public boolean useDefaultShowPreview;
        /**
         * True, if message content should be displayed in notifications.
         */
        public boolean showPreview;
        /**
         * If true, disablePinnedMessageNotifications is ignored and the value for the relevant type of chat is used instead.
         */
        public boolean useDefaultDisablePinnedMessageNotifications;
        /**
         * If true, notifications for incoming pinned messages will be created as for an ordinary unread message.
         */
        public boolean disablePinnedMessageNotifications;
        /**
         * If true, disableMentionNotifications is ignored and the value for the relevant type of chat is used instead.
         */
        public boolean useDefaultDisableMentionNotifications;
        /**
         * If true, notifications for messages with mentions will be created as for an ordinary unread message.
         */
        public boolean disableMentionNotifications;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1503183218;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes actions that a user is allowed to take in a chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatPermissions extends Object {
        /**
         * True, if the user can send text messages, contacts, locations, and venues.
         */
        public boolean canSendMessages;
        /**
         * True, if the user can send audio files, documents, photos, videos, video notes, and voice notes. Implies canSendMessages permissions.
         */
        public boolean canSendMediaMessages;
        /**
         * True, if the user can send polls. Implies canSendMessages permissions.
         */
        public boolean canSendPolls;
        /**
         * True, if the user can send animations, games, and stickers and use inline bots. Implies canSendMessages permissions.
         */
        public boolean canSendOtherMessages;
        /**
         * True, if the user may add a web page preview to their messages. Implies canSendMessages permissions.
         */
        public boolean canAddWebPagePreviews;
        /**
         * True, if the user can change the chat title, photo, and other settings.
         */
        public boolean canChangeInfo;
        /**
         * True, if the user can invite new users to the chat.
         */
        public boolean canInviteUsers;
        /**
         * True, if the user can pin messages.
         */
        public boolean canPinMessages;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1584650463;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes the photo of a chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatPhoto extends Object {
        /**
         * A small (160x160) chat photo. The file can be downloaded only before the photo is changed.
         */
        public File small;
        /**
         * A big (640x640) chat photo. The file can be downloaded only before the photo is changed.
         */
        public File big;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -217062456;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the reason why a chat is reported.
     */
    public abstract static class ChatReportReason extends Object {
    }

    /**
     * The chat contains spam messages.
     */
    public static class ChatReportReasonSpam extends ChatReportReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -510848863;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat promotes violence.
     */
    public static class ChatReportReasonViolence extends ChatReportReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1330235395;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat contains pornographic messages.
     */
    public static class ChatReportReasonPornography extends ChatReportReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 722614385;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat has child abuse related content.
     */
    public static class ChatReportReasonChildAbuse extends ChatReportReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1070686531;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat contains copyrighted content.
     */
    public static class ChatReportReasonCopyright extends ChatReportReason {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 986898080;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A custom reason provided by the user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatReportReasonCustom extends ChatReportReason {
        /**
         * Report text.
         */
        public String text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 544575454;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about the availability of the &quot;Report spam&quot; action for a chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatReportSpamState extends Object {
        /**
         * True, if a prompt with the &quot;Report spam&quot; action should be shown to the user.
         */
        public boolean canReportSpam;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1919240972;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the type of a chat.
     */
    public abstract static class ChatType extends Object {
    }

    /**
     * An ordinary chat with a user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatTypePrivate extends ChatType {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1700720838;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A basic group (i.e., a chat with 0-200 other users).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatTypeBasicGroup extends ChatType {
        /**
         * Basic group identifier.
         */
        public int basicGroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 21815278;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A supergroup (i.e. a chat with up to GetOption(&quot;supergroup_max_size&quot;) other users), or channel (with unlimited members).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatTypeSupergroup extends ChatType {
        /**
         * Supergroup or channel identifier.
         */
        public int supergroupId;
        /**
         * True, if the supergroup is a channel.
         */
        public boolean isChannel;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 955152366;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A secret chat with a user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChatTypeSecret extends ChatType {
        /**
         * Secret chat identifier.
         */
        public int secretChatId;
        /**
         * User identifier of the secret chat peer.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 136722563;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a list of chats.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Chats extends Object {
        /**
         * List of chat identifiers.
         */
        public long[] chatIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1687756019;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents result of checking whether a username can be set for a chat.
     */
    public abstract static class CheckChatUsernameResult extends Object {
    }

    /**
     * The username can be set.
     */
    public static class CheckChatUsernameResultOk extends CheckChatUsernameResult {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1498956964;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The username is invalid.
     */
    public static class CheckChatUsernameResultUsernameInvalid extends CheckChatUsernameResult {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -636979370;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The username is occupied.
     */
    public static class CheckChatUsernameResultUsernameOccupied extends CheckChatUsernameResult {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1320892201;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user has too much chats with username, one of them should be made private first.
     */
    public static class CheckChatUsernameResultPublicChatsTooMuch extends CheckChatUsernameResult {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 858247741;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user can't be a member of a public supergroup.
     */
    public static class CheckChatUsernameResultPublicGroupsUnavailable extends CheckChatUsernameResult {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -51833641;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about one website the current user is logged in with Telegram.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ConnectedWebsite extends Object {
        /**
         * Website identifier.
         */
        public long id;
        /**
         * The domain name of the website.
         */
        public String domainName;
        /**
         * User identifier of a bot linked with the website.
         */
        public int botUserId;
        /**
         * The version of a browser used to log in.
         */
        public String browser;
        /**
         * Operating system the browser is running on.
         */
        public String platform;
        /**
         * Point in time (Unix timestamp) when the user was logged in.
         */
        public int logInDate;
        /**
         * Point in time (Unix timestamp) when obtained authorization was last used.
         */
        public int lastActiveDate;
        /**
         * IP address from which the user was logged in, in human-readable format.
         */
        public String ip;
        /**
         * Human-readable description of a country and a region, from which the user was logged in, based on the IP address.
         */
        public String location;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1538986855;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of websites the current user is logged in with Telegram.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ConnectedWebsites extends Object {
        /**
         * List of connected websites.
         */
        public ConnectedWebsite[] websites;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1727949694;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the current state of the connection to Telegram servers.
     */
    public abstract static class ConnectionState extends Object {
    }

    /**
     * Currently waiting for the network to become available. Use setNetworkType to change the available network type.
     */
    public static class ConnectionStateWaitingForNetwork extends ConnectionState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1695405912;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Currently establishing a connection with a proxy server.
     */
    public static class ConnectionStateConnectingToProxy extends ConnectionState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -93187239;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Currently establishing a connection to the Telegram servers.
     */
    public static class ConnectionStateConnecting extends ConnectionState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1298400670;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Downloading data received while the client was offline.
     */
    public static class ConnectionStateUpdating extends ConnectionState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -188104009;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * There is a working connection to the Telegram servers.
     */
    public static class ConnectionStateReady extends ConnectionState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 48608492;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a user contact.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Contact extends Object {
        /**
         * Phone number of the user.
         */
        public String phoneNumber;
        /**
         * First name of the user; 1-255 characters in length.
         */
        public String firstName;
        /**
         * Last name of the user.
         */
        public String lastName;
        /**
         * Additional data about the user in a form of vCard; 0-2048 bytes in length.
         */
        public String vcard;
        /**
         * Identifier of the user, if known; otherwise 0.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1483002540;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a counter.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Count extends Object {
        /**
         * Count.
         */
        public int count;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1295577348;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the result of a custom request.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CustomRequestResult extends Object {
        /**
         * A JSON-serialized result.
         */
        public String result;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2009960452;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains database statistics.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DatabaseStatistics extends Object {
        /**
         * Database statistics in an unspecified human-readable format.
         */
        public String statistics;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1123912880;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a date according to the Gregorian calendar.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Date extends Object {
        /**
         * Day of the month, 1-31.
         */
        public int day;
        /**
         * Month, 1-12.
         */
        public int month;
        /**
         * Year, 1-9999.
         */
        public int year;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -277956960;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * File with the date it was uploaded.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DatedFile extends Object {
        /**
         * The file.
         */
        public File file;
        /**
         * Point in time (Unix timestamp) when the file was uploaded.
         */
        public int date;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1840795491;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a tg:// deep link.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeepLinkInfo extends Object {
        /**
         * Text to be shown to the user.
         */
        public FormattedText text;
        /**
         * True, if user should be asked to update the application.
         */
        public boolean needUpdateApplication;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1864081662;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a data needed to subscribe for push notifications through registerDevice method. To use specific push notification service, you must specify the correct application platform and upload valid server authentication data at https://my.telegram.org.
     */
    public abstract static class DeviceToken extends Object {
    }

    /**
     * A token for Firebase Cloud Messaging.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenFirebaseCloudMessaging extends DeviceToken {
        /**
         * Device registration token; may be empty to de-register a device.
         */
        public String token;
        /**
         * True, if push notifications should be additionally encrypted.
         */
        public boolean encrypt;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -797881849;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for Apple Push Notification service.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenApplePush extends DeviceToken {
        /**
         * Device token; may be empty to de-register a device.
         */
        public String deviceToken;
        /**
         * True, if App Sandbox is enabled.
         */
        public boolean isAppSandbox;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 387541955;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for Apple Push Notification service VoIP notifications.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenApplePushVoIP extends DeviceToken {
        /**
         * Device token; may be empty to de-register a device.
         */
        public String deviceToken;
        /**
         * True, if App Sandbox is enabled.
         */
        public boolean isAppSandbox;
        /**
         * True, if push notifications should be additionally encrypted.
         */
        public boolean encrypt;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 804275689;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for Windows Push Notification Services.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenWindowsPush extends DeviceToken {
        /**
         * The access token that will be used to send notifications; may be empty to de-register a device.
         */
        public String accessToken;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1410514289;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for Microsoft Push Notification Service.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenMicrosoftPush extends DeviceToken {
        /**
         * Push notification channel URI; may be empty to de-register a device.
         */
        public String channelUri;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1224269900;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for Microsoft Push Notification Service VoIP channel.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenMicrosoftPushVoIP extends DeviceToken {
        /**
         * Push notification channel URI; may be empty to de-register a device.
         */
        public String channelUri;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -785603759;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for web Push API.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenWebPush extends DeviceToken {
        /**
         * Absolute URL exposed by the push service where the application server can send push messages; may be empty to de-register a device.
         */
        public String endpoint;
        /**
         * Base64url-encoded P-256 elliptic curve Diffie-Hellman public key.
         */
        public String p256dhBase64url;
        /**
         * Base64url-encoded authentication secret.
         */
        public String authBase64url;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1694507273;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for Simple Push API for Firefox OS.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenSimplePush extends DeviceToken {
        /**
         * Absolute URL exposed by the push service where the application server can send push messages; may be empty to de-register a device.
         */
        public String endpoint;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 49584736;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for Ubuntu Push Client service.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenUbuntuPush extends DeviceToken {
        /**
         * Token; may be empty to de-register a device.
         */
        public String token;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1782320422;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for BlackBerry Push Service.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenBlackBerryPush extends DeviceToken {
        /**
         * Token; may be empty to de-register a device.
         */
        public String token;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1559167234;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A token for Tizen Push Service.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeviceTokenTizenPush extends DeviceToken {
        /**
         * Push service registration identifier; may be empty to de-register a device.
         */
        public String regId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1359947213;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a document of any type.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Document extends Object {
        /**
         * Original name of the file; as defined by the sender.
         */
        public String fileName;
        /**
         * MIME type of the file; as defined by the sender.
         */
        public String mimeType;
        /**
         * Document minithumbnail; may be null.
         */
        public Minithumbnail minithumbnail;
        /**
         * Document thumbnail in JPEG or PNG format (PNG will be used only for background patterns); as defined by the sender; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * File containing the document.
         */
        public File document;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 21881988;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a message draft.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DraftMessage extends Object {
        /**
         * Identifier of the message to reply to; 0 if none.
         */
        public long replyToMessageId;
        /**
         * Content of the message draft; this should always be of type inputMessageText.
         */
        public InputMessageContent inputMessageText;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1902914742;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Information about the email address authentication code that was sent.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EmailAddressAuthenticationCodeInfo extends Object {
        /**
         * Pattern of the email address to which an authentication code was sent.
         */
        public String emailAddressPattern;
        /**
         * Length of the code; 0 if unknown.
         */
        public int length;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1151066659;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a list of emoji.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Emojis extends Object {
        /**
         * List of emojis.
         */
        public String[] emojis;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 950339552;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains encrypted Telegram Passport data credentials.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EncryptedCredentials extends Object {
        /**
         * The encrypted credentials.
         */
        public byte[] data;
        /**
         * The decrypted data hash.
         */
        public byte[] hash;
        /**
         * Secret for data decryption, encrypted with the service's public key.
         */
        public byte[] secret;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1331106766;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about an encrypted Telegram Passport element; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EncryptedPassportElement extends Object {
        /**
         * Type of Telegram Passport element.
         */
        public PassportElementType type;
        /**
         * Encrypted JSON-encoded data about the user.
         */
        public byte[] data;
        /**
         * The front side of an identity document.
         */
        public DatedFile frontSide;
        /**
         * The reverse side of an identity document; may be null.
         */
        public DatedFile reverseSide;
        /**
         * Selfie with the document; may be null.
         */
        public DatedFile selfie;
        /**
         * List of files containing a certified English translation of the document.
         */
        public DatedFile[] translation;
        /**
         * List of attached files.
         */
        public DatedFile[] files;
        /**
         * Unencrypted data, phone number or email address.
         */
        public String value;
        /**
         * Hash of the entire element.
         */
        public String hash;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2002386193;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An object of this type can be returned on every function call, in case of an error.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Error extends Object {
        /**
         * Error code; subject to future changes. If the error code is 406, the error message must not be processed in any way and must not be displayed to the user.
         */
        public int code;
        /**
         * Error message; subject to future changes.
         */
        public String message;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1679978726;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class File extends Object {
        /**
         * Unique file identifier.
         */
        public int id;
        /**
         * File size; 0 if unknown.
         */
        public int size;
        /**
         * Expected file size in case the exact file size is unknown, but an approximate size is known. Can be used to show download/upload progress.
         */
        public int expectedSize;
        /**
         * Information about the local copy of the file.
         */
        public LocalFile local;
        /**
         * Information about the remote copy of the file.
         */
        public RemoteFile remote;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 766337656;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a part of a file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FilePart extends Object {
        /**
         * File bytes.
         */
        public byte[] data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 911821878;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents the type of a file.
     */
    public abstract static class FileType extends Object {
    }

    /**
     * The data is not a file.
     */
    public static class FileTypeNone extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2003009189;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is an animation.
     */
    public static class FileTypeAnimation extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -290816582;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is an audio file.
     */
    public static class FileTypeAudio extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -709112160;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a document.
     */
    public static class FileTypeDocument extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -564722929;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a photo.
     */
    public static class FileTypePhoto extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1718914651;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a profile photo.
     */
    public static class FileTypeProfilePhoto extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1795089315;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file was sent to a secret chat (the file type is not known to the server).
     */
    public static class FileTypeSecret extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1871899401;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a thumbnail of a file from a secret chat.
     */
    public static class FileTypeSecretThumbnail extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1401326026;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a file from Secure storage used for storing Telegram Passport files.
     */
    public static class FileTypeSecure extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1419133146;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a sticker.
     */
    public static class FileTypeSticker extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 475233385;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a thumbnail of another file.
     */
    public static class FileTypeThumbnail extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -12443298;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file type is not yet known.
     */
    public static class FileTypeUnknown extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2011566768;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a video.
     */
    public static class FileTypeVideo extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1430816539;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a video note.
     */
    public static class FileTypeVideoNote extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -518412385;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a voice note.
     */
    public static class FileTypeVoiceNote extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -588681661;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file is a wallpaper or a background pattern.
     */
    public static class FileTypeWallpaper extends FileType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1854930076;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A text with some entities.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FormattedText extends Object {
        /**
         * The text.
         */
        public String text;
        /**
         * Entities contained in the text.
         */
        public TextEntity[] entities;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -252624564;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of messages found by a search.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FoundMessages extends Object {
        /**
         * List of messages.
         */
        public Message[] messages;
        /**
         * Value to pass as fromSearchId to get more results.
         */
        public long nextFromSearchId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2135623881;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a game.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Game extends Object {
        /**
         * Game ID.
         */
        public long id;
        /**
         * Game short name. To share a game use the URL https://t.me/{botUsername}?game={gameShortName}.
         */
        public String shortName;
        /**
         * Game title.
         */
        public String title;
        /**
         * Game text, usually containing scoreboards for a game.
         */
        public FormattedText text;
        /**
         * Game description.
         */
        public String description;
        /**
         * Game photo.
         */
        public Photo photo;
        /**
         * Game animation; may be null.
         */
        public Animation animation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1565597752;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains one row of the game high score table.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GameHighScore extends Object {
        /**
         * Position in the high score table.
         */
        public int position;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * User score.
         */
        public int score;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -30778358;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of game high scores.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GameHighScores extends Object {
        /**
         * A list of game high scores.
         */
        public GameHighScore[] scores;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -725770727;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of hashtags.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Hashtags extends Object {
        /**
         * A list of hashtags.
         */
        public String[] hashtags;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 676798885;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains an HTTP URL.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class HttpUrl extends Object {
        /**
         * The URL.
         */
        public String url;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2018019930;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An identity document.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class IdentityDocument extends Object {
        /**
         * Document number; 1-24 characters.
         */
        public String number;
        /**
         * Document expiry date; may be null.
         */
        public Date expiryDate;
        /**
         * Front side of the document.
         */
        public DatedFile frontSide;
        /**
         * Reverse side of the document; only for driver license and identity card.
         */
        public DatedFile reverseSide;
        /**
         * Selfie with the document; may be null.
         */
        public DatedFile selfie;
        /**
         * List of files containing a certified English translation of the document.
         */
        public DatedFile[] translation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 445952972;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents the result of an ImportContacts request.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ImportedContacts extends Object {
        /**
         * User identifiers of the imported contacts in the same order as they were specified in the request; 0 if the contact is not yet a registered user.
         */
        public int[] userIds;
        /**
         * The number of users that imported the corresponding contact; 0 for already registered users or if unavailable.
         */
        public int[] importerCount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -741685354;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a single button in an inline keyboard.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineKeyboardButton extends Object {
        /**
         * Text of the button.
         */
        public String text;
        /**
         * Type of the button.
         */
        public InlineKeyboardButtonType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -372105704;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the type of an inline keyboard button.
     */
    public abstract static class InlineKeyboardButtonType extends Object {
    }

    /**
     * A button that opens a specified URL.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineKeyboardButtonTypeUrl extends InlineKeyboardButtonType {
        /**
         * HTTP or tg:// URL to open.
         */
        public String url;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1130741420;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A button that opens a specified URL and automatically logs in in current user if they allowed to do that.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineKeyboardButtonTypeLoginUrl extends InlineKeyboardButtonType {
        /**
         * HTTP URL to open.
         */
        public String url;
        /**
         * Unique button identifier.
         */
        public int id;
        /**
         * If non-empty, new text of the button in forwarded messages.
         */
        public String forwardText;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 281435539;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A button that sends a special callback query to a bot.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineKeyboardButtonTypeCallback extends InlineKeyboardButtonType {
        /**
         * Data to be sent to the bot via a callback query.
         */
        public byte[] data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1127515139;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A button with a game that sends a special callback query to a bot. This button must be in the first column and row of the keyboard and can be attached only to a message with content of the type messageGame.
     */
    public static class InlineKeyboardButtonTypeCallbackGame extends InlineKeyboardButtonType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -383429528;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A button that forces an inline query to the bot to be inserted in the input field.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineKeyboardButtonTypeSwitchInline extends InlineKeyboardButtonType {
        /**
         * Inline query to be sent to the bot.
         */
        public String query;
        /**
         * True, if the inline query should be sent from the current chat.
         */
        public boolean inCurrentChat;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2035563307;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A button to buy something. This button must be in the first column and row of the keyboard and can be attached only to a message with content of the type messageInvoice.
     */
    public static class InlineKeyboardButtonTypeBuy extends InlineKeyboardButtonType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1360739440;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a single result of an inline query.
     */
    public abstract static class InlineQueryResult extends Object {
    }

    /**
     * Represents a link to an article or web page.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultArticle extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * URL of the result, if it exists.
         */
        public String url;
        /**
         * True, if the URL must be not shown.
         */
        public boolean hideUrl;
        /**
         * Title of the result.
         */
        public String title;
        /**
         * A short description of the result.
         */
        public String description;
        /**
         * Result thumbnail; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -518366710;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a user contact.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultContact extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * A user contact.
         */
        public Contact contact;
        /**
         * Result thumbnail; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 410081985;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a point on the map.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultLocation extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Location result.
         */
        public Location location;
        /**
         * Title of the result.
         */
        public String title;
        /**
         * Result thumbnail; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -158305341;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents information about a venue.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultVenue extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Venue result.
         */
        public Venue venue;
        /**
         * Result thumbnail; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1592932211;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents information about a game.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultGame extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Game result.
         */
        public Game game;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1706916987;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents an animation file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultAnimation extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Animation file.
         */
        public Animation animation;
        /**
         * Animation title.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2009984267;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents an audio file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultAudio extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Audio file.
         */
        public Audio audio;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 842650360;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a document.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultDocument extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Document.
         */
        public Document document;
        /**
         * Document title.
         */
        public String title;
        /**
         * Document description.
         */
        public String description;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1491268539;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a photo.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultPhoto extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Photo.
         */
        public Photo photo;
        /**
         * Title of the result, if known.
         */
        public String title;
        /**
         * A short description of the result, if known.
         */
        public String description;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1848319440;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a sticker.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultSticker extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Sticker.
         */
        public Sticker sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1848224245;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a video.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultVideo extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Video.
         */
        public Video video;
        /**
         * Title of the video.
         */
        public String title;
        /**
         * Description of the video.
         */
        public String description;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1373158683;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a voice note.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResultVoiceNote extends InlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Voice note.
         */
        public VoiceNote voiceNote;
        /**
         * Title of the voice note.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1897393105;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents the results of the inline query. Use sendInlineQueryResultMessage to send the result of the query.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InlineQueryResults extends Object {
        /**
         * Unique identifier of the inline query.
         */
        public long inlineQueryId;
        /**
         * The offset for the next request. If empty, there are no more results.
         */
        public String nextOffset;
        /**
         * Results of the query.
         */
        public InlineQueryResult[] results;
        /**
         * If non-empty, this text should be shown on the button, which opens a private chat with the bot and sends the bot a start message with the switchPmParameter.
         */
        public String switchPmText;
        /**
         * Parameter for the bot start message.
         */
        public String switchPmParameter;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1000709656;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains information about background to set.
     */
    public abstract static class InputBackground extends Object {
    }

    /**
     * A background from a local file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputBackgroundLocal extends InputBackground {
        /**
         * Background file to use. Only inputFileLocal and inputFileGenerated are supported. The file nust be in JPEG format for wallpapers and in PNG format for patterns.
         */
        public InputFile background;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1747094364;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A background from the server.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputBackgroundRemote extends InputBackground {
        /**
         * The background identifier.
         */
        public long backgroundId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -274976231;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains information about the payment method chosen by the user.
     */
    public abstract static class InputCredentials extends Object {
    }

    /**
     * Applies if a user chooses some previously saved payment credentials. To use their previously saved credentials, the user must have a valid temporary password.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputCredentialsSaved extends InputCredentials {
        /**
         * Identifier of the saved credentials.
         */
        public String savedCredentialsId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2034385364;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Applies if a user enters new credentials on a payment provider website.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputCredentialsNew extends InputCredentials {
        /**
         * Contains JSON-encoded data with a credential identifier from the payment provider.
         */
        public String data;
        /**
         * True, if the credential identifier can be saved on the server side.
         */
        public boolean allowSave;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -829689558;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Applies if a user enters new credentials using Android Pay.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputCredentialsAndroidPay extends InputCredentials {
        /**
         * JSON-encoded data with the credential identifier.
         */
        public String data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1979566832;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Applies if a user enters new credentials using Apple Pay.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputCredentialsApplePay extends InputCredentials {
        /**
         * JSON-encoded data with the credential identifier.
         */
        public String data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1246570799;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Points to a file.
     */
    public abstract static class InputFile extends Object {
    }

    /**
     * A file defined by its unique ID.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputFileId extends InputFile {
        /**
         * Unique file identifier.
         */
        public int id;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1788906253;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A file defined by its remote ID. The remote ID is guaranteed to work only if it was received after TDLib launch and the corresponding file is still accessible to the user. For example, if the file is from a message, then the message must be not deleted and accessible to the user. If a file database is disabled, then the corresponding object with the file must be preloaded by the client.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputFileRemote extends InputFile {
        /**
         * Remote file identifier.
         */
        public String id;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -107574466;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A file defined by a local path.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputFileLocal extends InputFile {
        /**
         * Local path to the file.
         */
        public String path;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2056030919;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A file generated by the client.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputFileGenerated extends InputFile {
        /**
         * Local path to a file from which the file is generated; may be empty if there is no such file.
         */
        public String originalPath;
        /**
         * String specifying the conversion applied to the original file; should be persistent across application restarts. Conversions beginning with '#' are reserved for internal TDLib usage.
         */
        public String conversion;
        /**
         * Expected size of the generated file; 0 if unknown.
         */
        public int expectedSize;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1781351885;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An identity document to be saved to Telegram Passport.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputIdentityDocument extends Object {
        /**
         * Document number; 1-24 characters.
         */
        public String number;
        /**
         * Document expiry date, if available.
         */
        public Date expiryDate;
        /**
         * Front side of the document.
         */
        public InputFile frontSide;
        /**
         * Reverse side of the document; only for driver license and identity card.
         */
        public InputFile reverseSide;
        /**
         * Selfie with the document, if available.
         */
        public InputFile selfie;
        /**
         * List of files containing a certified English translation of the document.
         */
        public InputFile[] translation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -381776063;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a single result of an inline query; for bots only.
     */
    public abstract static class InputInlineQueryResult extends Object {
    }

    /**
     * Represents a link to an animated GIF.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultAnimatedGif extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Title of the query result.
         */
        public String title;
        /**
         * URL of the static result thumbnail (JPEG or GIF), if it exists.
         */
        public String thumbnailUrl;
        /**
         * The URL of the GIF-file (file size must not exceed 1MB).
         */
        public String gifUrl;
        /**
         * Duration of the GIF, in seconds.
         */
        public int gifDuration;
        /**
         * Width of the GIF.
         */
        public int gifWidth;
        /**
         * Height of the GIF.
         */
        public int gifHeight;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageAnimation, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -891474894;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a link to an animated (i.e. without sound) H.264/MPEG-4 AVC video.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultAnimatedMpeg4 extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Title of the result.
         */
        public String title;
        /**
         * URL of the static result thumbnail (JPEG or GIF), if it exists.
         */
        public String thumbnailUrl;
        /**
         * The URL of the MPEG4-file (file size must not exceed 1MB).
         */
        public String mpeg4Url;
        /**
         * Duration of the video, in seconds.
         */
        public int mpeg4Duration;
        /**
         * Width of the video.
         */
        public int mpeg4Width;
        /**
         * Height of the video.
         */
        public int mpeg4Height;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageAnimation, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1629529888;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a link to an article or web page.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultArticle extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * URL of the result, if it exists.
         */
        public String url;
        /**
         * True, if the URL must be not shown.
         */
        public boolean hideUrl;
        /**
         * Title of the result.
         */
        public String title;
        /**
         * A short description of the result.
         */
        public String description;
        /**
         * URL of the result thumbnail, if it exists.
         */
        public String thumbnailUrl;
        /**
         * Thumbnail width, if known.
         */
        public int thumbnailWidth;
        /**
         * Thumbnail height, if known.
         */
        public int thumbnailHeight;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1973670156;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a link to an MP3 audio file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultAudio extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Title of the audio file.
         */
        public String title;
        /**
         * Performer of the audio file.
         */
        public String performer;
        /**
         * The URL of the audio file.
         */
        public String audioUrl;
        /**
         * Audio file duration, in seconds.
         */
        public int audioDuration;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageAudio, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1260139988;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a user contact.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultContact extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * User contact.
         */
        public Contact contact;
        /**
         * URL of the result thumbnail, if it exists.
         */
        public String thumbnailUrl;
        /**
         * Thumbnail width, if known.
         */
        public int thumbnailWidth;
        /**
         * Thumbnail height, if known.
         */
        public int thumbnailHeight;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1846064594;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a link to a file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultDocument extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Title of the resulting file.
         */
        public String title;
        /**
         * Short description of the result, if known.
         */
        public String description;
        /**
         * URL of the file.
         */
        public String documentUrl;
        /**
         * MIME type of the file content; only &quot;application/pdf&quot; and &quot;application/zip&quot; are currently allowed.
         */
        public String mimeType;
        /**
         * The URL of the file thumbnail, if it exists.
         */
        public String thumbnailUrl;
        /**
         * Width of the thumbnail.
         */
        public int thumbnailWidth;
        /**
         * Height of the thumbnail.
         */
        public int thumbnailHeight;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageDocument, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 578801869;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a game.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultGame extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Short name of the game.
         */
        public String gameShortName;
        /**
         * Message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 966074327;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a point on the map.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultLocation extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Location result.
         */
        public Location location;
        /**
         * Amount of time relative to the message sent time until the location can be updated, in seconds.
         */
        public int livePeriod;
        /**
         * Title of the result.
         */
        public String title;
        /**
         * URL of the result thumbnail, if it exists.
         */
        public String thumbnailUrl;
        /**
         * Thumbnail width, if known.
         */
        public int thumbnailWidth;
        /**
         * Thumbnail height, if known.
         */
        public int thumbnailHeight;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1887650218;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents link to a JPEG image.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultPhoto extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Title of the result, if known.
         */
        public String title;
        /**
         * A short description of the result, if known.
         */
        public String description;
        /**
         * URL of the photo thumbnail, if it exists.
         */
        public String thumbnailUrl;
        /**
         * The URL of the JPEG photo (photo size must not exceed 5MB).
         */
        public String photoUrl;
        /**
         * Width of the photo.
         */
        public int photoWidth;
        /**
         * Height of the photo.
         */
        public int photoHeight;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessagePhoto, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1123338721;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a link to a WEBP or a TGS sticker.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultSticker extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * URL of the sticker thumbnail, if it exists.
         */
        public String thumbnailUrl;
        /**
         * The URL of the WEBP or a TGS sticker (sticker file size must not exceed 5MB).
         */
        public String stickerUrl;
        /**
         * Width of the sticker.
         */
        public int stickerWidth;
        /**
         * Height of the sticker.
         */
        public int stickerHeight;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, inputMessageSticker, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 274007129;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents information about a venue.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultVenue extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Venue result.
         */
        public Venue venue;
        /**
         * URL of the result thumbnail, if it exists.
         */
        public String thumbnailUrl;
        /**
         * Thumbnail width, if known.
         */
        public int thumbnailWidth;
        /**
         * Thumbnail height, if known.
         */
        public int thumbnailHeight;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 541704509;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a link to a page containing an embedded video player or a video file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultVideo extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Title of the result.
         */
        public String title;
        /**
         * A short description of the result, if known.
         */
        public String description;
        /**
         * The URL of the video thumbnail (JPEG), if it exists.
         */
        public String thumbnailUrl;
        /**
         * URL of the embedded video player or video file.
         */
        public String videoUrl;
        /**
         * MIME type of the content of the video URL, only &quot;text/html&quot; or &quot;video/mp4&quot; are currently supported.
         */
        public String mimeType;
        /**
         * Width of the video.
         */
        public int videoWidth;
        /**
         * Height of the video.
         */
        public int videoHeight;
        /**
         * Video duration, in seconds.
         */
        public int videoDuration;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageVideo, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1724073191;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a link to an opus-encoded audio file within an OGG container, single channel audio.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputInlineQueryResultVoiceNote extends InputInlineQueryResult {
        /**
         * Unique identifier of the query result.
         */
        public String id;
        /**
         * Title of the voice note.
         */
        public String title;
        /**
         * The URL of the voice note file.
         */
        public String voiceNoteUrl;
        /**
         * Duration of the voice note, in seconds.
         */
        public int voiceNoteDuration;
        /**
         * The message reply markup. Must be of type replyMarkupInlineKeyboard or null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent. Must be one of the following types: InputMessageText, InputMessageVoiceNote, InputMessageLocation, InputMessageVenue or InputMessageContact.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1790072503;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * The content of a message to send.
     */
    public abstract static class InputMessageContent extends Object {
    }

    /**
     * A text message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageText extends InputMessageContent {
        /**
         * Formatted text to be sent; 1-GetOption(&quot;message_text_length_max&quot;) characters. Only Bold, Italic, Code, Pre, PreCode and TextUrl entities are allowed to be specified manually.
         */
        public FormattedText text;
        /**
         * True, if rich web page previews for URLs in the message text should be disabled.
         */
        public boolean disableWebPagePreview;
        /**
         * True, if a chat message draft should be deleted.
         */
        public boolean clearDraft;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 247050392;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An animation message (GIF-style).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageAnimation extends InputMessageContent {
        /**
         * Animation file to be sent.
         */
        public InputFile animation;
        /**
         * Animation thumbnail, if available.
         */
        public InputThumbnail thumbnail;
        /**
         * Duration of the animation, in seconds.
         */
        public int duration;
        /**
         * Width of the animation; may be replaced by the server.
         */
        public int width;
        /**
         * Height of the animation; may be replaced by the server.
         */
        public int height;
        /**
         * Animation caption; 0-GetOption(&quot;message_caption_length_max&quot;) characters.
         */
        public FormattedText caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 926542724;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An audio message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageAudio extends InputMessageContent {
        /**
         * Audio file to be sent.
         */
        public InputFile audio;
        /**
         * Thumbnail of the cover for the album, if available.
         */
        public InputThumbnail albumCoverThumbnail;
        /**
         * Duration of the audio, in seconds; may be replaced by the server.
         */
        public int duration;
        /**
         * Title of the audio; 0-64 characters; may be replaced by the server.
         */
        public String title;
        /**
         * Performer of the audio; 0-64 characters, may be replaced by the server.
         */
        public String performer;
        /**
         * Audio caption; 0-GetOption(&quot;message_caption_length_max&quot;) characters.
         */
        public FormattedText caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -626786126;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A document message (general file).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageDocument extends InputMessageContent {
        /**
         * Document to be sent.
         */
        public InputFile document;
        /**
         * Document thumbnail, if available.
         */
        public InputThumbnail thumbnail;
        /**
         * Document caption; 0-GetOption(&quot;message_caption_length_max&quot;) characters.
         */
        public FormattedText caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 937970604;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A photo message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessagePhoto extends InputMessageContent {
        /**
         * Photo to send.
         */
        public InputFile photo;
        /**
         * Photo thumbnail to be sent, this is sent to the other party in secret chats only.
         */
        public InputThumbnail thumbnail;
        /**
         * File identifiers of the stickers added to the photo, if applicable.
         */
        public int[] addedStickerFileIds;
        /**
         * Photo width.
         */
        public int width;
        /**
         * Photo height.
         */
        public int height;
        /**
         * Photo caption; 0-GetOption(&quot;message_caption_length_max&quot;) characters.
         */
        public FormattedText caption;
        /**
         * Photo TTL (Time To Live), in seconds (0-60). A non-zero TTL can be specified only in private chats.
         */
        public int ttl;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1648801584;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A sticker message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageSticker extends InputMessageContent {
        /**
         * Sticker to be sent.
         */
        public InputFile sticker;
        /**
         * Sticker thumbnail, if available.
         */
        public InputThumbnail thumbnail;
        /**
         * Sticker width.
         */
        public int width;
        /**
         * Sticker height.
         */
        public int height;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 740776325;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A video message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageVideo extends InputMessageContent {
        /**
         * Video to be sent.
         */
        public InputFile video;
        /**
         * Video thumbnail, if available.
         */
        public InputThumbnail thumbnail;
        /**
         * File identifiers of the stickers added to the video, if applicable.
         */
        public int[] addedStickerFileIds;
        /**
         * Duration of the video, in seconds.
         */
        public int duration;
        /**
         * Video width.
         */
        public int width;
        /**
         * Video height.
         */
        public int height;
        /**
         * True, if the video should be tried to be streamed.
         */
        public boolean supportsStreaming;
        /**
         * Video caption; 0-GetOption(&quot;message_caption_length_max&quot;) characters.
         */
        public FormattedText caption;
        /**
         * Video TTL (Time To Live), in seconds (0-60). A non-zero TTL can be specified only in private chats.
         */
        public int ttl;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2108486755;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A video note message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageVideoNote extends InputMessageContent {
        /**
         * Video note to be sent.
         */
        public InputFile videoNote;
        /**
         * Video thumbnail, if available.
         */
        public InputThumbnail thumbnail;
        /**
         * Duration of the video, in seconds.
         */
        public int duration;
        /**
         * Video width and height; must be positive and not greater than 640.
         */
        public int length;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 279108859;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A voice note message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageVoiceNote extends InputMessageContent {
        /**
         * Voice note to be sent.
         */
        public InputFile voiceNote;
        /**
         * Duration of the voice note, in seconds.
         */
        public int duration;
        /**
         * Waveform representation of the voice note, in 5-bit format.
         */
        public byte[] waveform;
        /**
         * Voice note caption; 0-GetOption(&quot;message_caption_length_max&quot;) characters.
         */
        public FormattedText caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2136519657;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a location.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageLocation extends InputMessageContent {
        /**
         * Location to be sent.
         */
        public Location location;
        /**
         * Period for which the location can be updated, in seconds; should bebetween 60 and 86400 for a live location and 0 otherwise.
         */
        public int livePeriod;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1624179655;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with information about a venue.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageVenue extends InputMessageContent {
        /**
         * Venue to send.
         */
        public Venue venue;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1447926269;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message containing a user contact.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageContact extends InputMessageContent {
        /**
         * Contact to send.
         */
        public Contact contact;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -982446849;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a game; not supported for channels or secret chats.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageGame extends InputMessageContent {
        /**
         * User identifier of the bot that owns the game.
         */
        public int botUserId;
        /**
         * Short name of the game.
         */
        public String gameShortName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1728000914;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with an invoice; can be used only by bots and only in private chats.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageInvoice extends InputMessageContent {
        /**
         * Invoice.
         */
        public Invoice invoice;
        /**
         * Product title; 1-32 characters.
         */
        public String title;
        /**
         * Product description; 0-255 characters.
         */
        public String description;
        /**
         * Product photo URL; optional.
         */
        public String photoUrl;
        /**
         * Product photo size.
         */
        public int photoSize;
        /**
         * Product photo width.
         */
        public int photoWidth;
        /**
         * Product photo height.
         */
        public int photoHeight;
        /**
         * The invoice payload.
         */
        public byte[] payload;
        /**
         * Payment provider token.
         */
        public String providerToken;
        /**
         * JSON-encoded data about the invoice, which will be shared with the payment provider.
         */
        public String providerData;
        /**
         * Unique invoice bot startParameter for the generation of this invoice.
         */
        public String startParameter;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1038812175;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a poll. Polls can't be sent to private or secret chats.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessagePoll extends InputMessageContent {
        /**
         * Poll question, 1-255 characters.
         */
        public String question;
        /**
         * List of poll answer options, 2-10 strings 1-100 characters each.
         */
        public String[] options;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1791140518;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A forwarded message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputMessageForwarded extends InputMessageContent {
        /**
         * Identifier for the chat this forwarded message came from.
         */
        public long fromChatId;
        /**
         * Identifier of the message to forward.
         */
        public long messageId;
        /**
         * True, if a game message should be shared within a launched game; applies only to game messages.
         */
        public boolean inGameShare;
        /**
         * True, if content of the message needs to be copied without a link to the original message. Always true if the message is forwarded to a secret chat.
         */
        public boolean sendCopy;
        /**
         * True, if media caption of the message copy needs to be removed. Ignored if sendCopy is false.
         */
        public boolean removeCaption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1503132333;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains information about a Telegram Passport element to be saved.
     */
    public abstract static class InputPassportElement extends Object {
    }

    /**
     * A Telegram Passport element to be saved containing the user's personal details.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementPersonalDetails extends InputPassportElement {
        /**
         * Personal details of the user.
         */
        public PersonalDetails personalDetails;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 164791359;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's passport.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementPassport extends InputPassportElement {
        /**
         * The passport to be saved.
         */
        public InputIdentityDocument passport;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -497011356;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's driver license.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementDriverLicense extends InputPassportElement {
        /**
         * The driver license to be saved.
         */
        public InputIdentityDocument driverLicense;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 304813264;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's identity card.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementIdentityCard extends InputPassportElement {
        /**
         * The identity card to be saved.
         */
        public InputIdentityDocument identityCard;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -9963390;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's internal passport.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementInternalPassport extends InputPassportElement {
        /**
         * The internal passport to be saved.
         */
        public InputIdentityDocument internalPassport;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 715360043;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's address.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementAddress extends InputPassportElement {
        /**
         * The address to be saved.
         */
        public Address address;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 461630480;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's utility bill.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementUtilityBill extends InputPassportElement {
        /**
         * The utility bill to be saved.
         */
        public InputPersonalDocument utilityBill;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1389203841;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's bank statement.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementBankStatement extends InputPassportElement {
        /**
         * The bank statement to be saved.
         */
        public InputPersonalDocument bankStatement;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -26585208;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's rental agreement.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementRentalAgreement extends InputPassportElement {
        /**
         * The rental agreement to be saved.
         */
        public InputPersonalDocument rentalAgreement;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1736154155;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's passport registration.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementPassportRegistration extends InputPassportElement {
        /**
         * The passport registration page to be saved.
         */
        public InputPersonalDocument passportRegistration;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1314562128;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's temporary registration.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementTemporaryRegistration extends InputPassportElement {
        /**
         * The temporary registration document to be saved.
         */
        public InputPersonalDocument temporaryRegistration;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1913238047;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's phone number.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementPhoneNumber extends InputPassportElement {
        /**
         * The phone number to be saved.
         */
        public String phoneNumber;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1319357497;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element to be saved containing the user's email address.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementEmailAddress extends InputPassportElement {
        /**
         * The email address to be saved.
         */
        public String emailAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -248605659;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the description of an error in a Telegram Passport element; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementError extends Object {
        /**
         * Type of Telegram Passport element that has the error.
         */
        public PassportElementType type;
        /**
         * Error message.
         */
        public String message;
        /**
         * Error source.
         */
        public InputPassportElementErrorSource source;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 285756898;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains the description of an error in a Telegram Passport element; for bots only.
     */
    public abstract static class InputPassportElementErrorSource extends Object {
    }

    /**
     * The element contains an error in an unspecified place. The error will be considered resolved when new data is added.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceUnspecified extends InputPassportElementErrorSource {
        /**
         * Current hash of the entire element.
         */
        public byte[] elementHash;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 267230319;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A data field contains an error. The error is considered resolved when the field's value changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceDataField extends InputPassportElementErrorSource {
        /**
         * Field name.
         */
        public String fieldName;
        /**
         * Current data hash.
         */
        public byte[] dataHash;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -426795002;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The front side of the document contains an error. The error is considered resolved when the file with the front side of the document changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceFrontSide extends InputPassportElementErrorSource {
        /**
         * Current hash of the file containing the front side.
         */
        public byte[] fileHash;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 588023741;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The reverse side of the document contains an error. The error is considered resolved when the file with the reverse side of the document changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceReverseSide extends InputPassportElementErrorSource {
        /**
         * Current hash of the file containing the reverse side.
         */
        public byte[] fileHash;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 413072891;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The selfie contains an error. The error is considered resolved when the file with the selfie changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceSelfie extends InputPassportElementErrorSource {
        /**
         * Current hash of the file containing the selfie.
         */
        public byte[] fileHash;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -773575528;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * One of the files containing the translation of the document contains an error. The error is considered resolved when the file with the translation changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceTranslationFile extends InputPassportElementErrorSource {
        /**
         * Current hash of the file containing the translation.
         */
        public byte[] fileHash;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 505842299;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The translation of the document contains an error. The error is considered resolved when the list of files changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceTranslationFiles extends InputPassportElementErrorSource {
        /**
         * Current hashes of all files with the translation.
         */
        public byte[][] fileHashes;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -527254048;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file contains an error. The error is considered resolved when the file changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceFile extends InputPassportElementErrorSource {
        /**
         * Current hash of the file which has the error.
         */
        public byte[] fileHash;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -298492469;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The list of attached files contains an error. The error is considered resolved when the file list changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPassportElementErrorSourceFiles extends InputPassportElementErrorSource {
        /**
         * Current hashes of all attached files.
         */
        public byte[][] fileHashes;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2008541640;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A personal document to be saved to Telegram Passport.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputPersonalDocument extends Object {
        /**
         * List of files containing the pages of the document.
         */
        public InputFile[] files;
        /**
         * List of files containing a certified English translation of the document.
         */
        public InputFile[] translation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1676966826;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a sticker that should be added to a sticker set.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputSticker extends Object {
        /**
         * PNG image with the sticker; must be up to 512 kB in size and fit in a 512x512 square.
         */
        public InputFile pngSticker;
        /**
         * Emoji corresponding to the sticker.
         */
        public String emojis;
        /**
         * For masks, position where the mask should be placed; may be null.
         */
        public MaskPosition maskPosition;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1998602205;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A thumbnail to be sent along with a file; should be in JPEG or WEBP format for stickers, and less than 200 kB in size.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InputThumbnail extends Object {
        /**
         * Thumbnail file to send. Sending thumbnails by fileId is currently not supported.
         */
        public InputFile thumbnail;
        /**
         * Thumbnail width, usually shouldn't exceed 320. Use 0 if unknown.
         */
        public int width;
        /**
         * Thumbnail height, usually shouldn't exceed 320. Use 0 if unknown.
         */
        public int height;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1582387236;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Product invoice.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Invoice extends Object {
        /**
         * ISO 4217 currency code.
         */
        public String currency;
        /**
         * A list of objects used to calculate the total price of the product.
         */
        public LabeledPricePart[] priceParts;
        /**
         * True, if the payment is a test payment.
         */
        public boolean isTest;
        /**
         * True, if the user's name is needed for payment.
         */
        public boolean needName;
        /**
         * True, if the user's phone number is needed for payment.
         */
        public boolean needPhoneNumber;
        /**
         * True, if the user's email address is needed for payment.
         */
        public boolean needEmailAddress;
        /**
         * True, if the user's shipping address is needed for payment.
         */
        public boolean needShippingAddress;
        /**
         * True, if the user's phone number will be sent to the provider.
         */
        public boolean sendPhoneNumberToProvider;
        /**
         * True, if the user's email address will be sent to the provider.
         */
        public boolean sendEmailAddressToProvider;
        /**
         * True, if the total price depends on the shipping method.
         */
        public boolean isFlexible;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -368451690;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents one member of a JSON object.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JsonObjectMember extends Object {
        /**
         * Member's key.
         */
        public String key;
        /**
         * Member's value.
         */
        public JsonValue value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1803309418;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a JSON value.
     */
    public abstract static class JsonValue extends Object {
    }

    /**
     * Represents a null JSON value.
     */
    public static class JsonValueNull extends JsonValue {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -92872499;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a boolean JSON value.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JsonValueBoolean extends JsonValue {
        /**
         * The value.
         */
        public boolean value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2142186576;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a numeric JSON value.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JsonValueNumber extends JsonValue {
        /**
         * The value.
         */
        public double value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1010822033;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a string JSON value.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JsonValueString extends JsonValue {
        /**
         * The value.
         */
        public String value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1597947313;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a JSON array.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JsonValueArray extends JsonValue {
        /**
         * The list of array elements.
         */
        public JsonValue[] values;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -183913546;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a JSON object.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JsonValueObject extends JsonValue {
        /**
         * The list of object members.
         */
        public JsonObjectMember[] members;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 520252026;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a single button in a bot keyboard.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class KeyboardButton extends Object {
        /**
         * Text of the button.
         */
        public String text;
        /**
         * Type of the button.
         */
        public KeyboardButtonType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2069836172;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes a keyboard button type.
     */
    public abstract static class KeyboardButtonType extends Object {
    }

    /**
     * A simple button, with text that should be sent when the button is pressed.
     */
    public static class KeyboardButtonTypeText extends KeyboardButtonType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1773037256;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A button that sends the user's phone number when pressed; available only in private chats.
     */
    public static class KeyboardButtonTypeRequestPhoneNumber extends KeyboardButtonType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1529235527;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A button that sends the user's location when pressed; available only in private chats.
     */
    public static class KeyboardButtonTypeRequestLocation extends KeyboardButtonType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -125661955;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Portion of the price of a product (e.g., &quot;delivery cost&quot;, &quot;tax amount&quot;).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LabeledPricePart extends Object {
        /**
         * Label for this portion of the product price.
         */
        public String label;
        /**
         * Currency amount in minimal quantity of the currency.
         */
        public long amount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 552789798;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a language pack.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LanguagePackInfo extends Object {
        /**
         * Unique language pack identifier.
         */
        public String id;
        /**
         * Identifier of a base language pack; may be empty. If a string is missed in the language pack, then it should be fetched from base language pack. Unsupported in custom language packs.
         */
        public String baseLanguagePackId;
        /**
         * Language name.
         */
        public String name;
        /**
         * Name of the language in that language.
         */
        public String nativeName;
        /**
         * A language code to be used to apply plural forms. See https://www.unicode.org/cldr/charts/latest/supplemental/language_plural_rules.html for more info.
         */
        public String pluralCode;
        /**
         * True, if the language pack is official.
         */
        public boolean isOfficial;
        /**
         * True, if the language pack strings are RTL.
         */
        public boolean isRtl;
        /**
         * True, if the language pack is a beta language pack.
         */
        public boolean isBeta;
        /**
         * True, if the language pack is installed by the current user.
         */
        public boolean isInstalled;
        /**
         * Total number of non-deleted strings from the language pack.
         */
        public int totalStringCount;
        /**
         * Total number of translated strings from the language pack.
         */
        public int translatedStringCount;
        /**
         * Total number of non-deleted strings from the language pack available locally.
         */
        public int localStringCount;
        /**
         * Link to language translation interface; empty for custom local language packs.
         */
        public String translationUrl;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 542199642;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents one language pack string.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LanguagePackString extends Object {
        /**
         * String key.
         */
        public String key;
        /**
         * String value.
         */
        public LanguagePackStringValue value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1307632736;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents the value of a string in a language pack.
     */
    public abstract static class LanguagePackStringValue extends Object {
    }

    /**
     * An ordinary language pack string.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LanguagePackStringValueOrdinary extends LanguagePackStringValue {
        /**
         * String value.
         */
        public String value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -249256352;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A language pack string which has different forms based on the number of some object it mentions. See https://www.unicode.org/cldr/charts/latest/supplemental/language_plural_rules.html for more info.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LanguagePackStringValuePluralized extends LanguagePackStringValue {
        /**
         * Value for zero objects.
         */
        public String zeroValue;
        /**
         * Value for one object.
         */
        public String oneValue;
        /**
         * Value for two objects.
         */
        public String twoValue;
        /**
         * Value for few objects.
         */
        public String fewValue;
        /**
         * Value for many objects.
         */
        public String manyValue;
        /**
         * Default value.
         */
        public String otherValue;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1906840261;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A deleted language pack string, the value should be taken from the built-in english language pack.
     */
    public static class LanguagePackStringValueDeleted extends LanguagePackStringValue {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1834792698;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of language pack strings.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LanguagePackStrings extends Object {
        /**
         * A list of language pack strings.
         */
        public LanguagePackString[] strings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1172082922;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents the relationship between user A and user B. For incomingLink, user A is the current user; for outgoingLink, user B is the current user.
     */
    public abstract static class LinkState extends Object {
    }

    /**
     * The phone number of user A is not known to user B.
     */
    public static class LinkStateNone extends LinkState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 951430287;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The phone number of user A is known but that number has not been saved to the contact list of user B.
     */
    public static class LinkStateKnowsPhoneNumber extends LinkState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 380898199;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The phone number of user A has been saved to the contact list of user B.
     */
    public static class LinkStateIsContact extends LinkState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1000499465;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a local file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LocalFile extends Object {
        /**
         * Local path to the locally available file part; may be empty.
         */
        public String path;
        /**
         * True, if it is possible to try to download or generate the file.
         */
        public boolean canBeDownloaded;
        /**
         * True, if the file can be deleted.
         */
        public boolean canBeDeleted;
        /**
         * True, if the file is currently being downloaded (or a local copy is being generated by some other means).
         */
        public boolean isDownloadingActive;
        /**
         * True, if the local copy is fully available.
         */
        public boolean isDownloadingCompleted;
        /**
         * Download will be started from this offset. downloadedPrefixSize is calculated from this offset.
         */
        public int downloadOffset;
        /**
         * If isDownloadingCompleted is false, then only some prefix of the file starting from downloadOffset is ready to be read. downloadedPrefixSize is the size of that prefix.
         */
        public int downloadedPrefixSize;
        /**
         * Total downloaded file bytes. Should be used only for calculating download progress. The actual file size may be bigger, and some parts of it may contain garbage.
         */
        public int downloadedSize;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1166400317;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about the current localization target.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LocalizationTargetInfo extends Object {
        /**
         * List of available language packs for this application.
         */
        public LanguagePackInfo[] languagePacks;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2048670809;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a location on planet Earth.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Location extends Object {
        /**
         * Latitude of the location in degrees; as defined by the sender.
         */
        public double latitude;
        /**
         * Longitude of the location, in degrees; as defined by the sender.
         */
        public double longitude;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 749028016;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes a stream to which TDLib internal log is written.
     */
    public abstract static class LogStream extends Object {
    }

    /**
     * The log is written to stderr or an OS specific log.
     */
    public static class LogStreamDefault extends LogStream {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1390581436;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The log is written to a file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LogStreamFile extends LogStream {
        /**
         * Path to the file to where the internal TDLib log will be written.
         */
        public String path;
        /**
         * Maximum size of the file to where the internal TDLib log is written before the file will be auto-rotated.
         */
        public long maxFileSize;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1880085930;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The log is written nowhere.
     */
    public static class LogStreamEmpty extends LogStream {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -499912244;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of available TDLib internal log tags.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LogTags extends Object {
        /**
         * List of log tags.
         */
        public String[] tags;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1604930601;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a TDLib internal log verbosity level.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LogVerbosityLevel extends Object {
        /**
         * Log verbosity level.
         */
        public int verbosityLevel;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1734624234;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Part of the face, relative to which a mask should be placed.
     */
    public abstract static class MaskPoint extends Object {
    }

    /**
     * A mask should be placed relatively to the forehead.
     */
    public static class MaskPointForehead extends MaskPoint {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1027512005;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A mask should be placed relatively to the eyes.
     */
    public static class MaskPointEyes extends MaskPoint {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1748310861;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A mask should be placed relatively to the mouth.
     */
    public static class MaskPointMouth extends MaskPoint {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 411773406;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A mask should be placed relatively to the chin.
     */
    public static class MaskPointChin extends MaskPoint {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 534995335;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Position on a photo where a mask should be placed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MaskPosition extends Object {
        /**
         * Part of the face, relative to which the mask should be placed.
         */
        public MaskPoint point;
        /**
         * Shift by X-axis measured in widths of the mask scaled to the face size, from left to right. (For example, -1.0 will place the mask just to the left of the default mask position.)
         */
        public double xShift;
        /**
         * Shift by Y-axis measured in heights of the mask scaled to the face size, from top to bottom. (For example, 1.0 will place the mask just below the default mask position.)
         */
        public double yShift;
        /**
         * Mask scaling coefficient. (For example, 2.0 means a doubled size.)
         */
        public double scale;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2097433026;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Message extends Object {
        /**
         * Message identifier, unique for the chat to which the message belongs.
         */
        public long id;
        /**
         * Identifier of the user who sent the message; 0 if unknown. Currently, it is unknown for channel posts and for channel posts automatically forwarded to discussion group.
         */
        public int senderUserId;
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Information about the sending state of the message; may be null.
         */
        public MessageSendingState sendingState;
        /**
         * True, if the message is outgoing.
         */
        public boolean isOutgoing;
        /**
         * True, if the message can be edited. For live location and poll messages this fields shows, whether editMessageLiveLocation or stopPoll can be used with this message by the client.
         */
        public boolean canBeEdited;
        /**
         * True, if the message can be forwarded.
         */
        public boolean canBeForwarded;
        /**
         * True, if the message can be deleted only for the current user while other users will continue to see it.
         */
        public boolean canBeDeletedOnlyForSelf;
        /**
         * True, if the message can be deleted for all users.
         */
        public boolean canBeDeletedForAllUsers;
        /**
         * True, if the message is a channel post. All messages to channels are channel posts, all other messages are not channel posts.
         */
        public boolean isChannelPost;
        /**
         * True, if the message contains an unread mention for the current user.
         */
        public boolean containsUnreadMention;
        /**
         * Point in time (Unix timestamp) when the message was sent.
         */
        public int date;
        /**
         * Point in time (Unix timestamp) when the message was last edited.
         */
        public int editDate;
        /**
         * Information about the initial message sender; may be null.
         */
        public MessageForwardInfo forwardInfo;
        /**
         * If non-zero, the identifier of the message this message is replying to; can be the identifier of a deleted message.
         */
        public long replyToMessageId;
        /**
         * For self-destructing messages, the message's TTL (Time To Live), in seconds; 0 if none. TDLib will send updateDeleteMessages or updateMessageContent once the TTL expires.
         */
        public int ttl;
        /**
         * Time left before the message expires, in seconds.
         */
        public double ttlExpiresIn;
        /**
         * If non-zero, the user identifier of the bot through which this message was sent.
         */
        public int viaBotUserId;
        /**
         * For channel posts, optional author signature.
         */
        public String authorSignature;
        /**
         * Number of times this message was viewed.
         */
        public int views;
        /**
         * Unique identifier of an album this message belongs to. Only photos and videos can be grouped together in albums.
         */
        public long mediaAlbumId;
        /**
         * Content of the message.
         */
        public MessageContent content;
        /**
         * Reply markup for the message; may be null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1804824068;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains the content of a message.
     */
    public abstract static class MessageContent extends Object {
    }

    /**
     * A text message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageText extends MessageContent {
        /**
         * Text of the message.
         */
        public FormattedText text;
        /**
         * A preview of the web page that's mentioned in the text; may be null.
         */
        public WebPage webPage;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1989037971;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An animation message (GIF-style).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageAnimation extends MessageContent {
        /**
         * Message content.
         */
        public Animation animation;
        /**
         * Animation caption.
         */
        public FormattedText caption;
        /**
         * True, if the animation thumbnail must be blurred and the animation must be shown only while tapped.
         */
        public boolean isSecret;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1306939396;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An audio message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageAudio extends MessageContent {
        /**
         * Message content.
         */
        public Audio audio;
        /**
         * Audio caption.
         */
        public FormattedText caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 276722716;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A document message (general file).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageDocument extends MessageContent {
        /**
         * Message content.
         */
        public Document document;
        /**
         * Document caption.
         */
        public FormattedText caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 596945783;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A photo message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessagePhoto extends MessageContent {
        /**
         * Message content.
         */
        public Photo photo;
        /**
         * Photo caption.
         */
        public FormattedText caption;
        /**
         * True, if the photo must be blurred and must be shown only while tapped.
         */
        public boolean isSecret;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1851395174;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An expired photo message (self-destructed after TTL has elapsed).
     */
    public static class MessageExpiredPhoto extends MessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1404641801;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A sticker message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageSticker extends MessageContent {
        /**
         * Message content.
         */
        public Sticker sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1779022878;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A video message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageVideo extends MessageContent {
        /**
         * Message content.
         */
        public Video video;
        /**
         * Video caption.
         */
        public FormattedText caption;
        /**
         * True, if the video thumbnail must be blurred and the video must be shown only while tapped.
         */
        public boolean isSecret;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2021281344;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An expired video message (self-destructed after TTL has elapsed).
     */
    public static class MessageExpiredVideo extends MessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1212209981;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A video note message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageVideoNote extends MessageContent {
        /**
         * Message content.
         */
        public VideoNote videoNote;
        /**
         * True, if at least one of the recipients has viewed the video note.
         */
        public boolean isViewed;
        /**
         * True, if the video note thumbnail must be blurred and the video note must be shown only while tapped.
         */
        public boolean isSecret;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 963323014;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A voice note message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageVoiceNote extends MessageContent {
        /**
         * Message content.
         */
        public VoiceNote voiceNote;
        /**
         * Voice note caption.
         */
        public FormattedText caption;
        /**
         * True, if at least one of the recipients has listened to the voice note.
         */
        public boolean isListened;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 527777781;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a location.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageLocation extends MessageContent {
        /**
         * Message content.
         */
        public Location location;
        /**
         * Time relative to the message sent date until which the location can be updated, in seconds.
         */
        public int livePeriod;
        /**
         * Left time for which the location can be updated, in seconds. updateMessageContent is not sent when this field changes.
         */
        public int expiresIn;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1301887786;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with information about a venue.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageVenue extends MessageContent {
        /**
         * Message content.
         */
        public Venue venue;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2146492043;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a user contact.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageContact extends MessageContent {
        /**
         * Message content.
         */
        public Contact contact;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -512684966;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a game.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageGame extends MessageContent {
        /**
         * Game.
         */
        public Game game;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -69441162;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a poll.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessagePoll extends MessageContent {
        /**
         * Poll.
         */
        public Poll poll;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -662130099;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with an invoice from a bot.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageInvoice extends MessageContent {
        /**
         * Product title.
         */
        public String title;
        /**
         * Product description.
         */
        public String description;
        /**
         * Product photo; may be null.
         */
        public Photo photo;
        /**
         * Currency for the product price.
         */
        public String currency;
        /**
         * Product total price in the minimal quantity of the currency.
         */
        public long totalAmount;
        /**
         * Unique invoice bot startParameter. To share an invoice use the URL https://t.me/{botUsername}?start={startParameter}.
         */
        public String startParameter;
        /**
         * True, if the invoice is a test invoice.
         */
        public boolean isTest;
        /**
         * True, if the shipping address should be specified.
         */
        public boolean needShippingAddress;
        /**
         * The identifier of the message with the receipt, after the product has been purchased.
         */
        public long receiptMessageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1916671476;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with information about an ended call.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageCall extends MessageContent {
        /**
         * Reason why the call was discarded.
         */
        public CallDiscardReason discardReason;
        /**
         * Call duration, in seconds.
         */
        public int duration;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 366512596;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A newly created basic group.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageBasicGroupChatCreate extends MessageContent {
        /**
         * Title of the basic group.
         */
        public String title;
        /**
         * User identifiers of members in the basic group.
         */
        public int[] memberUserIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1575377646;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A newly created supergroup or channel.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageSupergroupChatCreate extends MessageContent {
        /**
         * Title of the supergroup or channel.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -434325733;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An updated chat title.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageChatChangeTitle extends MessageContent {
        /**
         * New chat title.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 748272449;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An updated chat photo.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageChatChangePhoto extends MessageContent {
        /**
         * New chat photo.
         */
        public Photo photo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 319630249;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A deleted chat photo.
     */
    public static class MessageChatDeletePhoto extends MessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -184374809;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * New chat members were added.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageChatAddMembers extends MessageContent {
        /**
         * User identifiers of the new members.
         */
        public int[] memberUserIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 401228326;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new member joined the chat by invite link.
     */
    public static class MessageChatJoinByLink extends MessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1846493311;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat member was deleted.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageChatDeleteMember extends MessageContent {
        /**
         * User identifier of the deleted chat member.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1164414043;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A basic group was upgraded to a supergroup and was deactivated as the result.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageChatUpgradeTo extends MessageContent {
        /**
         * Identifier of the supergroup to which the basic group was upgraded.
         */
        public int supergroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1957816681;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A supergroup has been created from a basic group.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageChatUpgradeFrom extends MessageContent {
        /**
         * Title of the newly created supergroup.
         */
        public String title;
        /**
         * The identifier of the original basic group.
         */
        public int basicGroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1642272558;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message has been pinned.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessagePinMessage extends MessageContent {
        /**
         * Identifier of the pinned message, can be an identifier of a deleted message or 0.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 953503801;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A screenshot of a message in the chat has been taken.
     */
    public static class MessageScreenshotTaken extends MessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1564971605;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The TTL (Time To Live) setting messages in a secret chat has been changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageChatSetTtl extends MessageContent {
        /**
         * New TTL.
         */
        public int ttl;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1810060209;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A non-standard action has happened in the chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageCustomServiceAction extends MessageContent {
        /**
         * Message text to be shown in the chat.
         */
        public String text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1435879282;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new high score was achieved in a game.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageGameScore extends MessageContent {
        /**
         * Identifier of the message with the game, can be an identifier of a deleted message.
         */
        public long gameMessageId;
        /**
         * Identifier of the game; may be different from the games presented in the message with the game.
         */
        public long gameId;
        /**
         * New score.
         */
        public int score;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1344904575;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A payment has been completed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessagePaymentSuccessful extends MessageContent {
        /**
         * Identifier of the message with the corresponding invoice; can be an identifier of a deleted message.
         */
        public long invoiceMessageId;
        /**
         * Currency for the price of the product.
         */
        public String currency;
        /**
         * Total price for the product, in the minimal quantity of the currency.
         */
        public long totalAmount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -595962993;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A payment has been completed; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessagePaymentSuccessfulBot extends MessageContent {
        /**
         * Identifier of the message with the corresponding invoice; can be an identifier of a deleted message.
         */
        public long invoiceMessageId;
        /**
         * Currency for price of the product.
         */
        public String currency;
        /**
         * Total price for the product, in the minimal quantity of the currency.
         */
        public long totalAmount;
        /**
         * Invoice payload.
         */
        public byte[] invoicePayload;
        /**
         * Identifier of the shipping option chosen by the user; may be empty if not applicable.
         */
        public String shippingOptionId;
        /**
         * Information about the order; may be null.
         */
        public OrderInfo orderInfo;
        /**
         * Telegram payment identifier.
         */
        public String telegramPaymentChargeId;
        /**
         * Provider payment identifier.
         */
        public String providerPaymentChargeId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -412310696;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A contact has registered with Telegram.
     */
    public static class MessageContactRegistered extends MessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1502020353;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The current user has connected a website by logging in using Telegram Login Widget on it.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageWebsiteConnected extends MessageContent {
        /**
         * Domain name of the connected website.
         */
        public String domainName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1074551800;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Telegram Passport data has been sent.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessagePassportDataSent extends MessageContent {
        /**
         * List of Telegram Passport element types sent.
         */
        public PassportElementType[] types;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1017405171;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Telegram Passport data has been received; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessagePassportDataReceived extends MessageContent {
        /**
         * List of received Telegram Passport elements.
         */
        public EncryptedPassportElement[] elements;
        /**
         * Encrypted data credentials.
         */
        public EncryptedCredentials credentials;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1367863624;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Message content that is not supported by the client.
     */
    public static class MessageUnsupported extends MessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1816726139;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a forwarded message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageForwardInfo extends Object {
        /**
         * Origin of a forwarded message.
         */
        public MessageForwardOrigin origin;
        /**
         * Point in time (Unix timestamp) when the message was originally sent.
         */
        public int date;
        /**
         * For messages forwarded to the chat with the current user (saved messages) or to the channel discussion supergroup, the identifier of the chat from which the message was forwarded last time; 0 if unknown.
         */
        public long fromChatId;
        /**
         * For messages forwarded to the chat with the current user (saved messages) or to the channel discussion supergroup, the identifier of the original message from which the new message was forwarded last time; 0 if unknown.
         */
        public long fromMessageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1622371186;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains information about the origin of a forwarded message.
     */
    public abstract static class MessageForwardOrigin extends Object {
    }

    /**
     * The message was originally written by a known user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageForwardOriginUser extends MessageForwardOrigin {
        /**
         * Identifier of the user that originally sent the message.
         */
        public int senderUserId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2781520;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The message was originally written by a user, which is hidden by their privacy settings.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageForwardOriginHiddenUser extends MessageForwardOrigin {
        /**
         * Name of the sender.
         */
        public String senderName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -271257885;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The message was originally a post in a channel.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageForwardOriginChannel extends MessageForwardOrigin {
        /**
         * Identifier of the chat from which the message was originally forwarded.
         */
        public long chatId;
        /**
         * Message identifier of the original message; 0 if unknown.
         */
        public long messageId;
        /**
         * Original post author signature.
         */
        public String authorSignature;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1490730723;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a link to a message in a chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageLinkInfo extends Object {
        /**
         * True, if the link is a public link for a message in a chat.
         */
        public boolean isPublic;
        /**
         * If found, identifier of the chat to which the message belongs, 0 otherwise.
         */
        public long chatId;
        /**
         * If found, the linked message; may be null.
         */
        public Message message;
        /**
         * True, if the whole media album to which the message belongs is linked.
         */
        public boolean forAlbum;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 657372995;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains information about the sending state of the message.
     */
    public abstract static class MessageSendingState extends Object {
    }

    /**
     * The message is being sent now, but has not yet been delivered to the server.
     */
    public static class MessageSendingStatePending extends MessageSendingState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1381803582;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The message failed to be sent.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MessageSendingStateFailed extends MessageSendingState {
        /**
         * An error code; 0 if unknown.
         */
        public int errorCode;
        /**
         * Error message.
         */
        public String errorMessage;
        /**
         * True, if the message can be re-sent.
         */
        public boolean canRetry;
        /**
         * Time left before the message can be re-sent, in seconds. No update is sent when this field changes.
         */
        public double retryAfter;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2054476087;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of messages.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Messages extends Object {
        /**
         * Approximate total count of messages found.
         */
        public int totalCount;
        /**
         * List of messages; messages may be null.
         */
        public Message[] messages;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -16498159;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Thumbnail image of a very poor quality and low resolution.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Minithumbnail extends Object {
        /**
         * Thumbnail width, usually doesn't exceed 40.
         */
        public int width;
        /**
         * Thumbnail height, usually doesn't exceed 40.
         */
        public int height;
        /**
         * The thumbnail in JPEG format.
         */
        public byte[] data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -328540758;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A full list of available network statistic entries.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NetworkStatistics extends Object {
        /**
         * Point in time (Unix timestamp) when the app began collecting statistics.
         */
        public int sinceDate;
        /**
         * Network statistics entries.
         */
        public NetworkStatisticsEntry[] entries;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1615554212;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains statistics about network usage.
     */
    public abstract static class NetworkStatisticsEntry extends Object {
    }

    /**
     * Contains information about the total amount of data that was used to send and receive files.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NetworkStatisticsEntryFile extends NetworkStatisticsEntry {
        /**
         * Type of the file the data is part of.
         */
        public FileType fileType;
        /**
         * Type of the network the data was sent through. Call setNetworkType to maintain the actual network type.
         */
        public NetworkType networkType;
        /**
         * Total number of bytes sent.
         */
        public long sentBytes;
        /**
         * Total number of bytes received.
         */
        public long receivedBytes;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 188452706;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about the total amount of data that was used for calls.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NetworkStatisticsEntryCall extends NetworkStatisticsEntry {
        /**
         * Type of the network the data was sent through. Call setNetworkType to maintain the actual network type.
         */
        public NetworkType networkType;
        /**
         * Total number of bytes sent.
         */
        public long sentBytes;
        /**
         * Total number of bytes received.
         */
        public long receivedBytes;
        /**
         * Total call duration, in seconds.
         */
        public double duration;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 737000365;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents the type of a network.
     */
    public abstract static class NetworkType extends Object {
    }

    /**
     * The network is not available.
     */
    public static class NetworkTypeNone extends NetworkType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1971691759;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A mobile network.
     */
    public static class NetworkTypeMobile extends NetworkType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 819228239;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A mobile roaming network.
     */
    public static class NetworkTypeMobileRoaming extends NetworkType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1435199760;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Wi-Fi network.
     */
    public static class NetworkTypeWiFi extends NetworkType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -633872070;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A different network type (e.g., Ethernet network).
     */
    public static class NetworkTypeOther extends NetworkType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1942128539;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a notification.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Notification extends Object {
        /**
         * Unique persistent identifier of this notification.
         */
        public int id;
        /**
         * Notification date.
         */
        public int date;
        /**
         * True, if the notification was initially silent.
         */
        public boolean isSilent;
        /**
         * Notification type.
         */
        public NotificationType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 788743120;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a group of notifications.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NotificationGroup extends Object {
        /**
         * Unique persistent auto-incremented from 1 identifier of the notification group.
         */
        public int id;
        /**
         * Type of the group.
         */
        public NotificationGroupType type;
        /**
         * Identifier of a chat to which all notifications in the group belong.
         */
        public long chatId;
        /**
         * Total number of active notifications in the group.
         */
        public int totalCount;
        /**
         * The list of active notifications.
         */
        public Notification[] notifications;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 780691541;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes type of notifications in the group.
     */
    public abstract static class NotificationGroupType extends Object {
    }

    /**
     * A group containing notifications of type notificationTypeNewMessage and notificationTypeNewPushMessage with ordinary unread messages.
     */
    public static class NotificationGroupTypeMessages extends NotificationGroupType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1702481123;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A group containing notifications of type notificationTypeNewMessage and notificationTypeNewPushMessage with unread mentions of the current user, replies to their messages, or a pinned message.
     */
    public static class NotificationGroupTypeMentions extends NotificationGroupType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2050324051;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A group containing a notification of type notificationTypeNewSecretChat.
     */
    public static class NotificationGroupTypeSecretChat extends NotificationGroupType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1390759476;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A group containing notifications of type notificationTypeNewCall.
     */
    public static class NotificationGroupTypeCalls extends NotificationGroupType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1379123538;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the types of chats to which notification settings are applied.
     */
    public abstract static class NotificationSettingsScope extends Object {
    }

    /**
     * Notification settings applied to all private and secret chats when the corresponding chat setting has a default value.
     */
    public static class NotificationSettingsScopePrivateChats extends NotificationSettingsScope {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 937446759;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Notification settings applied to all basic groups and supergroups when the corresponding chat setting has a default value.
     */
    public static class NotificationSettingsScopeGroupChats extends NotificationSettingsScope {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1212142067;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Notification settings applied to all channels when the corresponding chat setting has a default value.
     */
    public static class NotificationSettingsScopeChannelChats extends NotificationSettingsScope {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 548013448;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains detailed information about a notification.
     */
    public abstract static class NotificationType extends Object {
    }

    /**
     * New message was received.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NotificationTypeNewMessage extends NotificationType {
        /**
         * The message.
         */
        public Message message;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1885935159;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * New secret chat was created.
     */
    public static class NotificationTypeNewSecretChat extends NotificationType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1198638768;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * New call was received.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NotificationTypeNewCall extends NotificationType {
        /**
         * Call identifier.
         */
        public int callId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1712734585;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * New message was received through a push notification.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NotificationTypeNewPushMessage extends NotificationType {
        /**
         * The message identifier. The message will not be available in the chat history, but the ID can be used in viewMessages and as replyToMessageId.
         */
        public long messageId;
        /**
         * Sender of the message. Corresponding user may be inaccessible.
         */
        public int senderUserId;
        /**
         * Push message content.
         */
        public PushMessageContent content;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1167232404;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An object of this type is returned on a successful function call for certain functions.
     */
    public static class Ok extends Object {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -722616727;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents the value of an option.
     */
    public abstract static class OptionValue extends Object {
    }

    /**
     * Represents a boolean option.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OptionValueBoolean extends OptionValue {
        /**
         * The value of the option.
         */
        public boolean value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 63135518;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents an unknown option or an option which has a default value.
     */
    public static class OptionValueEmpty extends OptionValue {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 918955155;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents an integer option.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OptionValueInteger extends OptionValue {
        /**
         * The value of the option.
         */
        public int value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1400911104;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a string option.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OptionValueString extends OptionValue {
        /**
         * The value of the option.
         */
        public String value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 756248212;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Order information.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OrderInfo extends Object {
        /**
         * Name of the user.
         */
        public String name;
        /**
         * Phone number of the user.
         */
        public String phoneNumber;
        /**
         * Email address of the user.
         */
        public String emailAddress;
        /**
         * Shipping address for this order; may be null.
         */
        public Address shippingAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 783997294;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes a block of an instant view web page.
     */
    public abstract static class PageBlock extends Object {
    }

    /**
     * The title of a page.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockTitle extends PageBlock {
        /**
         * Title.
         */
        public RichText title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1629664784;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The subtitle of a page.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockSubtitle extends PageBlock {
        /**
         * Subtitle.
         */
        public RichText subtitle;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 264524263;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The author and publishing date of a page.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockAuthorDate extends PageBlock {
        /**
         * Author.
         */
        public RichText author;
        /**
         * Point in time (Unix timestamp) when the article was published; 0 if unknown.
         */
        public int publishDate;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1300231184;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A header.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockHeader extends PageBlock {
        /**
         * Header.
         */
        public RichText header;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1402854811;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A subheader.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockSubheader extends PageBlock {
        /**
         * Subheader.
         */
        public RichText subheader;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1263956774;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A kicker.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockKicker extends PageBlock {
        /**
         * Kicker.
         */
        public RichText kicker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1361282635;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A text paragraph.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockParagraph extends PageBlock {
        /**
         * Paragraph text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1182402406;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A preformatted text paragraph.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockPreformatted extends PageBlock {
        /**
         * Paragraph text.
         */
        public RichText text;
        /**
         * Programming language for which the text should be formatted.
         */
        public String language;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1066346178;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The footer of a page.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockFooter extends PageBlock {
        /**
         * Footer.
         */
        public RichText footer;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 886429480;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An empty block separating a page.
     */
    public static class PageBlockDivider extends PageBlock {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -618614392;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An invisible anchor on a page, which can be used in a URL to open the page from the specified anchor.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockAnchor extends PageBlock {
        /**
         * Name of the anchor.
         */
        public String name;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -837994576;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A list of data blocks.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockList extends PageBlock {
        /**
         * The items of the list.
         */
        public PageBlockListItem[] items;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1037074852;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A block quote.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockBlockQuote extends PageBlock {
        /**
         * Quote text.
         */
        public RichText text;
        /**
         * Quote credit.
         */
        public RichText credit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1657834142;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A pull quote.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockPullQuote extends PageBlock {
        /**
         * Quote text.
         */
        public RichText text;
        /**
         * Quote credit.
         */
        public RichText credit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 490242317;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An animation.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockAnimation extends PageBlock {
        /**
         * Animation file; may be null.
         */
        public Animation animation;
        /**
         * Animation caption.
         */
        public PageBlockCaption caption;
        /**
         * True, if the animation should be played automatically.
         */
        public boolean needAutoplay;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1355669513;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An audio file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockAudio extends PageBlock {
        /**
         * Audio file; may be null.
         */
        public Audio audio;
        /**
         * Audio file caption.
         */
        public PageBlockCaption caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -63371245;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A photo.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockPhoto extends PageBlock {
        /**
         * Photo file; may be null.
         */
        public Photo photo;
        /**
         * Photo caption.
         */
        public PageBlockCaption caption;
        /**
         * URL that needs to be opened when the photo is clicked.
         */
        public String url;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 417601156;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A video.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockVideo extends PageBlock {
        /**
         * Video file; may be null.
         */
        public Video video;
        /**
         * Video caption.
         */
        public PageBlockCaption caption;
        /**
         * True, if the video should be played automatically.
         */
        public boolean needAutoplay;
        /**
         * True, if the video should be looped.
         */
        public boolean isLooped;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 510041394;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A voice note.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockVoiceNote extends PageBlock {
        /**
         * Voice note; may be null.
         */
        public VoiceNote voiceNote;
        /**
         * Voice note caption.
         */
        public PageBlockCaption caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1823310463;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A page cover.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockCover extends PageBlock {
        /**
         * Cover.
         */
        public PageBlock cover;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 972174080;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An embedded web page.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockEmbedded extends PageBlock {
        /**
         * Web page URL, if available.
         */
        public String url;
        /**
         * HTML-markup of the embedded page.
         */
        public String html;
        /**
         * Poster photo, if available; may be null.
         */
        public Photo posterPhoto;
        /**
         * Block width; 0 if unknown.
         */
        public int width;
        /**
         * Block height; 0 if unknown.
         */
        public int height;
        /**
         * Block caption.
         */
        public PageBlockCaption caption;
        /**
         * True, if the block should be full width.
         */
        public boolean isFullWidth;
        /**
         * True, if scrolling should be allowed.
         */
        public boolean allowScrolling;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1942577763;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An embedded post.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockEmbeddedPost extends PageBlock {
        /**
         * Web page URL.
         */
        public String url;
        /**
         * Post author.
         */
        public String author;
        /**
         * Post author photo; may be null.
         */
        public Photo authorPhoto;
        /**
         * Point in time (Unix timestamp) when the post was created; 0 if unknown.
         */
        public int date;
        /**
         * Post content.
         */
        public PageBlock[] pageBlocks;
        /**
         * Post caption.
         */
        public PageBlockCaption caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 397600949;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A collage.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockCollage extends PageBlock {
        /**
         * Collage item contents.
         */
        public PageBlock[] pageBlocks;
        /**
         * Block caption.
         */
        public PageBlockCaption caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1163760110;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A slideshow.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockSlideshow extends PageBlock {
        /**
         * Slideshow item contents.
         */
        public PageBlock[] pageBlocks;
        /**
         * Block caption.
         */
        public PageBlockCaption caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 539217375;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A link to a chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockChatLink extends PageBlock {
        /**
         * Chat title.
         */
        public String title;
        /**
         * Chat photo; may be null.
         */
        public ChatPhoto photo;
        /**
         * Chat username, by which all other information about the chat should be resolved.
         */
        public String username;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 214606645;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A table.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockTable extends PageBlock {
        /**
         * Table caption.
         */
        public RichText caption;
        /**
         * Table cells.
         */
        public PageBlockTableCell[][] cells;
        /**
         * True, if the table is bordered.
         */
        public boolean isBordered;
        /**
         * True, if the table is striped.
         */
        public boolean isStriped;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -942649288;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A collapsible block.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockDetails extends PageBlock {
        /**
         * Always visible heading for the block.
         */
        public RichText header;
        /**
         * Block contents.
         */
        public PageBlock[] pageBlocks;
        /**
         * True, if the block is open by default.
         */
        public boolean isOpen;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1599869809;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Related articles.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockRelatedArticles extends PageBlock {
        /**
         * Block header.
         */
        public RichText header;
        /**
         * List of related articles.
         */
        public PageBlockRelatedArticle[] articles;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1807324374;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A map.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockMap extends PageBlock {
        /**
         * Location of the map center.
         */
        public Location location;
        /**
         * Map zoom level.
         */
        public int zoom;
        /**
         * Map width.
         */
        public int width;
        /**
         * Map height.
         */
        public int height;
        /**
         * Block caption.
         */
        public PageBlockCaption caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1510961171;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a caption of an instant view web page block, consisting of a text and a trailing credit.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockCaption extends Object {
        /**
         * Content of the caption.
         */
        public RichText text;
        /**
         * Block credit (like HTML tag &lt;cite&gt;).
         */
        public RichText credit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1180064650;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes a horizontal alignment of a table cell content.
     */
    public abstract static class PageBlockHorizontalAlignment extends Object {
    }

    /**
     * The content should be left-aligned.
     */
    public static class PageBlockHorizontalAlignmentLeft extends PageBlockHorizontalAlignment {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 848701417;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The content should be center-aligned.
     */
    public static class PageBlockHorizontalAlignmentCenter extends PageBlockHorizontalAlignment {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1009203990;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The content should be right-aligned.
     */
    public static class PageBlockHorizontalAlignmentRight extends PageBlockHorizontalAlignment {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1371369214;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes an item of a list page block.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockListItem extends Object {
        /**
         * Item label.
         */
        public String label;
        /**
         * Item blocks.
         */
        public PageBlock[] pageBlocks;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 323186259;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a related article.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockRelatedArticle extends Object {
        /**
         * Related article URL.
         */
        public String url;
        /**
         * Article title; may be empty.
         */
        public String title;
        /**
         * Article description; may be empty.
         */
        public String description;
        /**
         * Article photo; may be null.
         */
        public Photo photo;
        /**
         * Article author; may be empty.
         */
        public String author;
        /**
         * Point in time (Unix timestamp) when the article was published; 0 if unknown.
         */
        public int publishDate;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 481199251;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a cell of a table.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PageBlockTableCell extends Object {
        /**
         * Cell text; may be null. If the text is null, then the cell should be invisible.
         */
        public RichText text;
        /**
         * True, if it is a header cell.
         */
        public boolean isHeader;
        /**
         * The number of columns the cell should span.
         */
        public int colspan;
        /**
         * The number of rows the cell should span.
         */
        public int rowspan;
        /**
         * Horizontal cell content alignment.
         */
        public PageBlockHorizontalAlignment align;
        /**
         * Vertical cell content alignment.
         */
        public PageBlockVerticalAlignment valign;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1417658214;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes a Vertical alignment of a table cell content.
     */
    public abstract static class PageBlockVerticalAlignment extends Object {
    }

    /**
     * The content should be top-aligned.
     */
    public static class PageBlockVerticalAlignmentTop extends PageBlockVerticalAlignment {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 195500454;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The content should be middle-aligned.
     */
    public static class PageBlockVerticalAlignmentMiddle extends PageBlockVerticalAlignment {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2123096587;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The content should be bottom-aligned.
     */
    public static class PageBlockVerticalAlignmentBottom extends PageBlockVerticalAlignment {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2092531158;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a Telegram Passport authorization form that was requested.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportAuthorizationForm extends Object {
        /**
         * Unique identifier of the authorization form.
         */
        public int id;
        /**
         * Information about the Telegram Passport elements that need to be provided to complete the form.
         */
        public PassportRequiredElement[] requiredElements;
        /**
         * URL for the privacy policy of the service; may be empty.
         */
        public String privacyPolicyUrl;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1070673218;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains information about a Telegram Passport element.
     */
    public abstract static class PassportElement extends Object {
    }

    /**
     * A Telegram Passport element containing the user's personal details.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementPersonalDetails extends PassportElement {
        /**
         * Personal details of the user.
         */
        public PersonalDetails personalDetails;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1217724035;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's passport.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementPassport extends PassportElement {
        /**
         * Passport.
         */
        public IdentityDocument passport;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -263985373;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's driver license.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementDriverLicense extends PassportElement {
        /**
         * Driver license.
         */
        public IdentityDocument driverLicense;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1643580589;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's identity card.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementIdentityCard extends PassportElement {
        /**
         * Identity card.
         */
        public IdentityDocument identityCard;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2083775797;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's internal passport.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementInternalPassport extends PassportElement {
        /**
         * Internal passport.
         */
        public IdentityDocument internalPassport;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 36220295;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's address.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementAddress extends PassportElement {
        /**
         * Address.
         */
        public Address address;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -782625232;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's utility bill.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementUtilityBill extends PassportElement {
        /**
         * Utility bill.
         */
        public PersonalDocument utilityBill;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -234611246;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's bank statement.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementBankStatement extends PassportElement {
        /**
         * Bank statement.
         */
        public PersonalDocument bankStatement;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -366464408;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's rental agreement.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementRentalAgreement extends PassportElement {
        /**
         * Rental agreement.
         */
        public PersonalDocument rentalAgreement;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -290141400;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's passport registration pages.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementPassportRegistration extends PassportElement {
        /**
         * Passport registration pages.
         */
        public PersonalDocument passportRegistration;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 618323071;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's temporary registration.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementTemporaryRegistration extends PassportElement {
        /**
         * Temporary registration.
         */
        public PersonalDocument temporaryRegistration;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1237626864;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's phone number.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementPhoneNumber extends PassportElement {
        /**
         * Phone number.
         */
        public String phoneNumber;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1320118375;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's email address.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementEmailAddress extends PassportElement {
        /**
         * Email address.
         */
        public String emailAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1528129531;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the description of an error in a Telegram Passport element.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementError extends Object {
        /**
         * Type of the Telegram Passport element which has the error.
         */
        public PassportElementType type;
        /**
         * Error message.
         */
        public String message;
        /**
         * Error source.
         */
        public PassportElementErrorSource source;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1861902395;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains the description of an error in a Telegram Passport element.
     */
    public abstract static class PassportElementErrorSource extends Object {
    }

    /**
     * The element contains an error in an unspecified place. The error will be considered resolved when new data is added.
     */
    public static class PassportElementErrorSourceUnspecified extends PassportElementErrorSource {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -378320830;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * One of the data fields contains an error. The error will be considered resolved when the value of the field changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementErrorSourceDataField extends PassportElementErrorSource {
        /**
         * Field name.
         */
        public String fieldName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -308650776;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The front side of the document contains an error. The error will be considered resolved when the file with the front side changes.
     */
    public static class PassportElementErrorSourceFrontSide extends PassportElementErrorSource {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1895658292;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The reverse side of the document contains an error. The error will be considered resolved when the file with the reverse side changes.
     */
    public static class PassportElementErrorSourceReverseSide extends PassportElementErrorSource {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1918630391;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The selfie with the document contains an error. The error will be considered resolved when the file with the selfie changes.
     */
    public static class PassportElementErrorSourceSelfie extends PassportElementErrorSource {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -797043672;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * One of files with the translation of the document contains an error. The error will be considered resolved when the file changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementErrorSourceTranslationFile extends PassportElementErrorSource {
        /**
         * Index of a file with the error.
         */
        public int fileIndex;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -689621228;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The translation of the document contains an error. The error will be considered resolved when the list of translation files changes.
     */
    public static class PassportElementErrorSourceTranslationFiles extends PassportElementErrorSource {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 581280796;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file contains an error. The error will be considered resolved when the file changes.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementErrorSourceFile extends PassportElementErrorSource {
        /**
         * Index of a file with the error.
         */
        public int fileIndex;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2020358960;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The list of attached files contains an error. The error will be considered resolved when the list of files changes.
     */
    public static class PassportElementErrorSourceFiles extends PassportElementErrorSource {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1894164178;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains the type of a Telegram Passport element.
     */
    public abstract static class PassportElementType extends Object {
    }

    /**
     * A Telegram Passport element containing the user's personal details.
     */
    public static class PassportElementTypePersonalDetails extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1032136365;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's passport.
     */
    public static class PassportElementTypePassport extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -436360376;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's driver license.
     */
    public static class PassportElementTypeDriverLicense extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1827298379;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's identity card.
     */
    public static class PassportElementTypeIdentityCard extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -502356132;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's internal passport.
     */
    public static class PassportElementTypeInternalPassport extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -793781959;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's address.
     */
    public static class PassportElementTypeAddress extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 496327874;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's utility bill.
     */
    public static class PassportElementTypeUtilityBill extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 627084906;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's bank statement.
     */
    public static class PassportElementTypeBankStatement extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 574095667;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's rental agreement.
     */
    public static class PassportElementTypeRentalAgreement extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2060583280;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the registration page of the user's passport.
     */
    public static class PassportElementTypePassportRegistration extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -159478209;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's temporary registration.
     */
    public static class PassportElementTypeTemporaryRegistration extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1092498527;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's phone number.
     */
    public static class PassportElementTypePhoneNumber extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -995361172;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A Telegram Passport element containing the user's email address.
     */
    public static class PassportElementTypeEmailAddress extends PassportElementType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -79321405;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about saved Telegram Passport elements.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElements extends Object {
        /**
         * Telegram Passport elements.
         */
        public PassportElement[] elements;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1264617556;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a Telegram Passport elements and corresponding errors.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportElementsWithErrors extends Object {
        /**
         * Telegram Passport elements.
         */
        public PassportElement[] elements;
        /**
         * Errors in the elements that are already available.
         */
        public PassportElementError[] errors;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1308923044;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a description of the required Telegram Passport element that was requested by a service.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportRequiredElement extends Object {
        /**
         * List of Telegram Passport elements any of which is enough to provide.
         */
        public PassportSuitableElement[] suitableElements;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1983641651;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a Telegram Passport element that was requested by a service.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PassportSuitableElement extends Object {
        /**
         * Type of the element.
         */
        public PassportElementType type;
        /**
         * True, if a selfie is required with the identity document.
         */
        public boolean isSelfieRequired;
        /**
         * True, if a certified English translation is required with the document.
         */
        public boolean isTranslationRequired;
        /**
         * True, if personal details must include the user's name in the language of their country of residence.
         */
        public boolean isNativeNameRequired;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -789019876;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents the current state of 2-step verification.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PasswordState extends Object {
        /**
         * True, if a 2-step verification password is set.
         */
        public boolean hasPassword;
        /**
         * Hint for the password; may be empty.
         */
        public String passwordHint;
        /**
         * True, if a recovery email is set.
         */
        public boolean hasRecoveryEmailAddress;
        /**
         * True, if some Telegram Passport elements were saved.
         */
        public boolean hasPassportData;
        /**
         * Information about the recovery email address to which the confirmation email was sent; may be null.
         */
        public EmailAddressAuthenticationCodeInfo recoveryEmailAddressCodeInfo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1154797731;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about an invoice payment form.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PaymentForm extends Object {
        /**
         * Full information of the invoice.
         */
        public Invoice invoice;
        /**
         * Payment form URL.
         */
        public String url;
        /**
         * Contains information about the payment provider, if available, to support it natively without the need for opening the URL; may be null.
         */
        public PaymentsProviderStripe paymentsProvider;
        /**
         * Saved server-side order information; may be null.
         */
        public OrderInfo savedOrderInfo;
        /**
         * Contains information about saved card credentials; may be null.
         */
        public SavedCredentials savedCredentials;
        /**
         * True, if the user can choose to save credentials.
         */
        public boolean canSaveCredentials;
        /**
         * True, if the user will be able to save credentials protected by a password they set up.
         */
        public boolean needPassword;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -200418230;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a successful payment.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PaymentReceipt extends Object {
        /**
         * Point in time (Unix timestamp) when the payment was made.
         */
        public int date;
        /**
         * User identifier of the payment provider bot.
         */
        public int paymentsProviderUserId;
        /**
         * Contains information about the invoice.
         */
        public Invoice invoice;
        /**
         * Contains order information; may be null.
         */
        public OrderInfo orderInfo;
        /**
         * Chosen shipping option; may be null.
         */
        public ShippingOption shippingOption;
        /**
         * Title of the saved credentials.
         */
        public String credentialsTitle;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1171223545;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the result of a payment request.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PaymentResult extends Object {
        /**
         * True, if the payment request was successful; otherwise the verificationUrl will be not empty.
         */
        public boolean success;
        /**
         * URL for additional payment credentials verification.
         */
        public String verificationUrl;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -804263843;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Stripe payment provider.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PaymentsProviderStripe extends Object {
        /**
         * Stripe API publishable key.
         */
        public String publishableKey;
        /**
         * True, if the user country must be provided.
         */
        public boolean needCountry;
        /**
         * True, if the user ZIP/postal code must be provided.
         */
        public boolean needPostalCode;
        /**
         * True, if the cardholder name must be provided.
         */
        public boolean needCardholderName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1090791032;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the user's personal details.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PersonalDetails extends Object {
        /**
         * First name of the user written in English; 1-255 characters.
         */
        public String firstName;
        /**
         * Middle name of the user written in English; 0-255 characters.
         */
        public String middleName;
        /**
         * Last name of the user written in English; 1-255 characters.
         */
        public String lastName;
        /**
         * Native first name of the user; 1-255 characters.
         */
        public String nativeFirstName;
        /**
         * Native middle name of the user; 0-255 characters.
         */
        public String nativeMiddleName;
        /**
         * Native last name of the user; 1-255 characters.
         */
        public String nativeLastName;
        /**
         * Birthdate of the user.
         */
        public Date birthdate;
        /**
         * Gender of the user, &quot;male&quot; or &quot;female&quot;.
         */
        public String gender;
        /**
         * A two-letter ISO 3166-1 alpha-2 country code of the user's country.
         */
        public String countryCode;
        /**
         * A two-letter ISO 3166-1 alpha-2 country code of the user's residence country.
         */
        public String residenceCountryCode;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1061656137;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A personal document, containing some information about a user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PersonalDocument extends Object {
        /**
         * List of files containing the pages of the document.
         */
        public DatedFile[] files;
        /**
         * List of files containing a certified English translation of the document.
         */
        public DatedFile[] translation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1011634661;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains settings for the authentication of the user's phone number.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PhoneNumberAuthenticationSettings extends Object {
        /**
         * Pass true if the authentication code may be sent via flash call to the specified phone number.
         */
        public boolean allowFlashCall;
        /**
         * Pass true if the authenticated phone number is used on the current device.
         */
        public boolean isCurrentPhoneNumber;
        /**
         * For official applications only. True, if the app can use Android SMS Retriever API (requires Google Play Services &gt;= 10.2) to automatically receive the authentication code from the SMS. See https://developers.google.com/identity/sms-retriever/ for more details.
         */
        public boolean allowSmsRetrieverApi;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -859198743;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a photo.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Photo extends Object {
        /**
         * True, if stickers were added to the photo.
         */
        public boolean hasStickers;
        /**
         * Photo minithumbnail; may be null.
         */
        public Minithumbnail minithumbnail;
        /**
         * Available variants of the photo, in different sizes.
         */
        public PhotoSize[] sizes;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2022871583;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Photo description.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PhotoSize extends Object {
        /**
         * Thumbnail type (see https://core.telegram.org/constructor/photoSize).
         */
        public String type;
        /**
         * Information about the photo file.
         */
        public File photo;
        /**
         * Photo width.
         */
        public int width;
        /**
         * Photo height.
         */
        public int height;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 421980227;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a poll.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Poll extends Object {
        /**
         * Unique poll identifier.
         */
        public long id;
        /**
         * Poll question, 1-255 characters.
         */
        public String question;
        /**
         * List of poll answer options.
         */
        public PollOption[] options;
        /**
         * Total number of voters, participating in the poll.
         */
        public int totalVoterCount;
        /**
         * True, if the poll is closed.
         */
        public boolean isClosed;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -959396214;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes one answer option of a poll.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PollOption extends Object {
        /**
         * Option text, 1-100 characters.
         */
        public String text;
        /**
         * Number of voters for this option, available only for closed or voted polls.
         */
        public int voterCount;
        /**
         * The percentage of votes for this option, 0-100.
         */
        public int votePercentage;
        /**
         * True, if the option was chosen by the user.
         */
        public boolean isChosen;
        /**
         * True, if the option is being chosen by a pending setPollAnswer request.
         */
        public boolean isBeingChosen;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1473893797;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a user profile photo.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ProfilePhoto extends Object {
        /**
         * Photo identifier; 0 for an empty photo. Can be used to find a photo in a list of userProfilePhotos.
         */
        public long id;
        /**
         * A small (160x160) user profile photo. The file can be downloaded only before the photo is changed.
         */
        public File small;
        /**
         * A big (640x640) user profile photo. The file can be downloaded only before the photo is changed.
         */
        public File big;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 978085937;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a list of proxy servers.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Proxies extends Object {
        /**
         * List of proxy servers.
         */
        public Proxy[] proxies;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1200447205;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about a proxy server.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Proxy extends Object {
        /**
         * Unique identifier of the proxy.
         */
        public int id;
        /**
         * Proxy server IP address.
         */
        public String server;
        /**
         * Proxy server port.
         */
        public int port;
        /**
         * Point in time (Unix timestamp) when the proxy was last used; 0 if never.
         */
        public int lastUsedDate;
        /**
         * True, if the proxy is enabled now.
         */
        public boolean isEnabled;
        /**
         * Type of the proxy.
         */
        public ProxyType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 196049779;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the type of the proxy server.
     */
    public abstract static class ProxyType extends Object {
    }

    /**
     * A SOCKS5 proxy server.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ProxyTypeSocks5 extends ProxyType {
        /**
         * Username for logging in; may be empty.
         */
        public String username;
        /**
         * Password for logging in; may be empty.
         */
        public String password;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -890027341;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A HTTP transparent proxy server.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ProxyTypeHttp extends ProxyType {
        /**
         * Username for logging in; may be empty.
         */
        public String username;
        /**
         * Password for logging in; may be empty.
         */
        public String password;
        /**
         * Pass true, if the proxy supports only HTTP requests and doesn't support transparent TCP connections via HTTP CONNECT method.
         */
        public boolean httpOnly;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1547188361;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An MTProto proxy server.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ProxyTypeMtproto extends ProxyType {
        /**
         * The proxy's secret in hexadecimal encoding.
         */
        public String secret;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1964826627;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a public HTTPS link to a message in a public supergroup or channel with a username.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PublicMessageLink extends Object {
        /**
         * Message link.
         */
        public String link;
        /**
         * HTML-code for embedding the message.
         */
        public String html;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -679603433;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains content of a push message notification.
     */
    public abstract static class PushMessageContent extends Object {
    }

    /**
     * A general message with hidden content.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentHidden extends PushMessageContent {
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -316950436;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An animation message (GIF-style).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentAnimation extends PushMessageContent {
        /**
         * Message content; may be null.
         */
        public Animation animation;
        /**
         * Animation caption.
         */
        public String caption;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1034215396;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An audio message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentAudio extends PushMessageContent {
        /**
         * Message content; may be null.
         */
        public Audio audio;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 381581426;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a user contact.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentContact extends PushMessageContent {
        /**
         * Contact's name.
         */
        public String name;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -12219820;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A contact has registered with Telegram.
     */
    public static class PushMessageContentContactRegistered extends PushMessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -303962720;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A document message (a general file).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentDocument extends PushMessageContent {
        /**
         * Message content; may be null.
         */
        public Document document;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -458379775;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a game.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentGame extends PushMessageContent {
        /**
         * Game title, empty for pinned game message.
         */
        public String title;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -515131109;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new high score was achieved in a game.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentGameScore extends PushMessageContent {
        /**
         * Game title, empty for pinned message.
         */
        public String title;
        /**
         * New score, 0 for pinned message.
         */
        public int score;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 901303688;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with an invoice from a bot.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentInvoice extends PushMessageContent {
        /**
         * Product price.
         */
        public String price;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1731687492;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a location.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentLocation extends PushMessageContent {
        /**
         * True, if the location is live.
         */
        public boolean isLive;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1288005709;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A photo message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentPhoto extends PushMessageContent {
        /**
         * Message content; may be null.
         */
        public Photo photo;
        /**
         * Photo caption.
         */
        public String caption;
        /**
         * True, if the photo is secret.
         */
        public boolean isSecret;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 140631122;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a poll.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentPoll extends PushMessageContent {
        /**
         * Poll question.
         */
        public String question;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1545438580;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A screenshot of a message in the chat has been taken.
     */
    public static class PushMessageContentScreenshotTaken extends PushMessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 214245369;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with a sticker.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentSticker extends PushMessageContent {
        /**
         * Message content; may be null.
         */
        public Sticker sticker;
        /**
         * Emoji corresponding to the sticker; may be empty.
         */
        public String emoji;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1553513939;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A text message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentText extends PushMessageContent {
        /**
         * Message text.
         */
        public String text;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 274587305;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A video message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentVideo extends PushMessageContent {
        /**
         * Message content; may be null.
         */
        public Video video;
        /**
         * Video caption.
         */
        public String caption;
        /**
         * True, if the video is secret.
         */
        public boolean isSecret;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 310038831;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A video note message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentVideoNote extends PushMessageContent {
        /**
         * Message content; may be null.
         */
        public VideoNote videoNote;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1122764417;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A voice note message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentVoiceNote extends PushMessageContent {
        /**
         * Message content; may be null.
         */
        public VoiceNote voiceNote;
        /**
         * True, if the message is a pinned message with the specified content.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 88910987;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A newly created basic group.
     */
    public static class PushMessageContentBasicGroupChatCreate extends PushMessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2114855172;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * New chat members were invited to a group.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentChatAddMembers extends PushMessageContent {
        /**
         * Name of the added member.
         */
        public String memberName;
        /**
         * True, if the current user was added to the group.
         */
        public boolean isCurrentUser;
        /**
         * True, if the user has returned to the group himself.
         */
        public boolean isReturned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1087145158;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat photo was edited.
     */
    public static class PushMessageContentChatChangePhoto extends PushMessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1114222051;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat title was edited.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentChatChangeTitle extends PushMessageContent {
        /**
         * New chat title.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1964902749;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat member was deleted.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentChatDeleteMember extends PushMessageContent {
        /**
         * Name of the deleted member.
         */
        public String memberName;
        /**
         * True, if the current user was deleted from the group.
         */
        public boolean isCurrentUser;
        /**
         * True, if the user has left the group himself.
         */
        public boolean isLeft;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 598714783;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new member joined the chat by invite link.
     */
    public static class PushMessageContentChatJoinByLink extends PushMessageContent {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1553719113;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A forwarded messages.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentMessageForwards extends PushMessageContent {
        /**
         * Number of forwarded messages.
         */
        public int totalCount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1913083876;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A media album.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushMessageContentMediaAlbum extends PushMessageContent {
        /**
         * Number of messages in the album.
         */
        public int totalCount;
        /**
         * True, if the album has at least one photo.
         */
        public boolean hasPhotos;
        /**
         * True, if the album has at least one video.
         */
        public boolean hasVideos;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -874278109;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a globally unique push receiver identifier, which can be used to identify which account has received a push notification.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PushReceiverId extends Object {
        /**
         * The globally unique identifier of push notification subscription.
         */
        public long id;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 371056428;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about the current recovery email address.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RecoveryEmailAddress extends Object {
        /**
         * Recovery email address.
         */
        public String recoveryEmailAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1290526187;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a remote file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoteFile extends Object {
        /**
         * Remote file identifier; may be empty. Can be used across application restarts or even from other devices for the current user. If the ID starts with &quot;http://&quot; or &quot;https://&quot;, it represents the HTTP URL of the file. TDLib is currently unable to download files if only their URL is known. If downloadFile is called on such a file or if it is sent to a secret chat, TDLib starts a file generation process by sending updateFileGenerationStart to the client with the HTTP URL in the originalPath and &quot;#url#&quot; as the conversion string. Clients should generate the file by downloading it to the specified location.
         */
        public String id;
        /**
         * True, if the file is currently being uploaded (or a remote copy is being generated by some other means).
         */
        public boolean isUploadingActive;
        /**
         * True, if a remote copy is fully available.
         */
        public boolean isUploadingCompleted;
        /**
         * Size of the remote available part of the file; 0 if unknown.
         */
        public int uploadedSize;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1761289748;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains a description of a custom keyboard and actions that can be done with it to quickly reply to bots.
     */
    public abstract static class ReplyMarkup extends Object {
    }

    /**
     * Instructs clients to remove the keyboard once this message has been received. This kind of keyboard can't be received in an incoming message; instead, UpdateChatReplyMarkup with messageId == 0 will be sent.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReplyMarkupRemoveKeyboard extends ReplyMarkup {
        /**
         * True, if the keyboard is removed only for the mentioned users or the target user of a reply.
         */
        public boolean isPersonal;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -691252879;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Instructs clients to force a reply to this message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReplyMarkupForceReply extends ReplyMarkup {
        /**
         * True, if a forced reply must automatically be shown to the current user. For outgoing messages, specify true to show the forced reply only for the mentioned users and for the target user of a reply.
         */
        public boolean isPersonal;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1039104593;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a custom keyboard layout to quickly reply to bots.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReplyMarkupShowKeyboard extends ReplyMarkup {
        /**
         * A list of rows of bot keyboard buttons.
         */
        public KeyboardButton[][] rows;
        /**
         * True, if the client needs to resize the keyboard vertically.
         */
        public boolean resizeKeyboard;
        /**
         * True, if the client needs to hide the keyboard after use.
         */
        public boolean oneTime;
        /**
         * True, if the keyboard must automatically be shown to the current user. For outgoing messages, specify true to show the keyboard only for the mentioned users and for the target user of a reply.
         */
        public boolean isPersonal;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -992627133;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains an inline keyboard layout.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReplyMarkupInlineKeyboard extends ReplyMarkup {
        /**
         * A list of rows of inline keyboard buttons.
         */
        public InlineKeyboardButton[][] rows;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -619317658;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes a text object inside an instant-view web page.
     */
    public abstract static class RichText extends Object {
    }

    /**
     * A plain text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextPlain extends RichText {
        /**
         * Text.
         */
        public String text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 482617702;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A bold rich text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextBold extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1670844268;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An italicized rich text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextItalic extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1853354047;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An underlined rich text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextUnderline extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -536019572;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A strike-through rich text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextStrikethrough extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 723413585;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A fixed-width rich text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextFixed extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1271496249;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rich text URL link.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextUrl extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * URL.
         */
        public String url;
        /**
         * True, if the URL has cached instant view server-side.
         */
        public boolean isCached;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 83939092;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rich text email link.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextEmailAddress extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Email address.
         */
        public String emailAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 40018679;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A subscript rich text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextSubscript extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -868197812;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A superscript rich text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextSuperscript extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -382241437;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A marked rich text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextMarked extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1271999614;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rich text phone number.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextPhoneNumber extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Phone number.
         */
        public String phoneNumber;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 128521539;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A small image inside the text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextIcon extends RichText {
        /**
         * The image represented as a document. The image can be in GIF, JPEG or PNG format.
         */
        public Document document;
        /**
         * Width of a bounding box in which the image should be shown; 0 if unknown.
         */
        public int width;
        /**
         * Height of a bounding box in which the image should be shown; 0 if unknown.
         */
        public int height;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1480316158;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rich text anchor.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTextAnchor extends RichText {
        /**
         * Text.
         */
        public RichText text;
        /**
         * Anchor name.
         */
        public String name;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 673137292;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A concatenation of rich texts.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RichTexts extends RichText {
        /**
         * Texts.
         */
        public RichText[] texts;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1647457821;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about saved card credentials.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SavedCredentials extends Object {
        /**
         * Unique identifier of the saved credentials.
         */
        public String id;
        /**
         * Title of the saved credentials.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -370273060;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about notification settings for several chats.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ScopeNotificationSettings extends Object {
        /**
         * Time left before notifications will be unmuted, in seconds.
         */
        public int muteFor;
        /**
         * The name of an audio file to be used for notification sounds; only applies to iOS applications.
         */
        public String sound;
        /**
         * True, if message content should be displayed in notifications.
         */
        public boolean showPreview;
        /**
         * True, if notifications for incoming pinned messages will be created as for an ordinary unread message.
         */
        public boolean disablePinnedMessageNotifications;
        /**
         * True, if notifications for messages with mentions will be created as for an ordinary unread message.
         */
        public boolean disableMentionNotifications;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -426103745;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a filter for message search results.
     */
    public abstract static class SearchMessagesFilter extends Object {
    }

    /**
     * Returns all found messages, no filter is applied.
     */
    public static class SearchMessagesFilterEmpty extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -869395657;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only animation messages.
     */
    public static class SearchMessagesFilterAnimation extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -155713339;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only audio messages.
     */
    public static class SearchMessagesFilterAudio extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 867505275;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only document messages.
     */
    public static class SearchMessagesFilterDocument extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1526331215;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only photo messages.
     */
    public static class SearchMessagesFilterPhoto extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 925932293;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only video messages.
     */
    public static class SearchMessagesFilterVideo extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 115538222;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only voice note messages.
     */
    public static class SearchMessagesFilterVoiceNote extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1841439357;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only photo and video messages.
     */
    public static class SearchMessagesFilterPhotoAndVideo extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1352130963;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only messages containing URLs.
     */
    public static class SearchMessagesFilterUrl extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1828724341;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only messages containing chat photos.
     */
    public static class SearchMessagesFilterChatPhoto extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1247751329;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only call messages.
     */
    public static class SearchMessagesFilterCall extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1305231012;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only incoming call messages with missed/declined discard reasons.
     */
    public static class SearchMessagesFilterMissedCall extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 970663098;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only video note messages.
     */
    public static class SearchMessagesFilterVideoNote extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 564323321;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only voice and video note messages.
     */
    public static class SearchMessagesFilterVoiceAndVideoNote extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 664174819;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only messages with mentions of the current user, or messages that are replies to their messages.
     */
    public static class SearchMessagesFilterMention extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2001258652;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns only messages with unread mentions of the current user, or messages that are replies to their messages. When using this filter the results can't be additionally filtered by a query or by the sending user.
     */
    public static class SearchMessagesFilterUnreadMention extends SearchMessagesFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -95769149;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a value representing a number of seconds.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Seconds extends Object {
        /**
         * Number of seconds.
         */
        public double seconds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 959899022;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a secret chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SecretChat extends Object {
        /**
         * Secret chat identifier.
         */
        public int id;
        /**
         * Identifier of the chat partner.
         */
        public int userId;
        /**
         * State of the secret chat.
         */
        public SecretChatState state;
        /**
         * True, if the chat was created by the current user; otherwise false.
         */
        public boolean isOutbound;
        /**
         * Current message Time To Live setting (self-destruct timer) for the chat, in seconds.
         */
        public int ttl;
        /**
         * Hash of the currently used key for comparison with the hash of the chat partner's key. This is a string of 36 bytes, which must be used to make a 12x12 square image with a color depth of 4. The first 16 bytes should be used to make a central 8x8 square, while the remaining 20 bytes should be used to construct a 2-pixel-wide border around that square. Alternatively, the first 32 bytes of the hash can be converted to the hexadecimal format and printed as 32 2-digit hex numbers.
         */
        public byte[] keyHash;
        /**
         * Secret chat layer; determines features supported by the other client. Video notes are supported if the layer &gt;= 66.
         */
        public int layer;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1279231629;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the current secret chat state.
     */
    public abstract static class SecretChatState extends Object {
    }

    /**
     * The secret chat is not yet created; waiting for the other user to get online.
     */
    public static class SecretChatStatePending extends SecretChatState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1637050756;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The secret chat is ready to use.
     */
    public static class SecretChatStateReady extends SecretChatState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1611352087;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The secret chat is closed.
     */
    public static class SecretChatStateClosed extends SecretChatState {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1945106707;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains information about one session in a Telegram application used by the current user. Sessions should be shown to the user in the returned order.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Session extends Object {
        /**
         * Session identifier.
         */
        public long id;
        /**
         * True, if this session is the current session.
         */
        public boolean isCurrent;
        /**
         * True, if a password is needed to complete authorization of the session.
         */
        public boolean isPasswordPending;
        /**
         * Telegram API identifier, as provided by the application.
         */
        public int apiId;
        /**
         * Name of the application, as provided by the application.
         */
        public String applicationName;
        /**
         * The version of the application, as provided by the application.
         */
        public String applicationVersion;
        /**
         * True, if the application is an official application or uses the apiId of an official application.
         */
        public boolean isOfficialApplication;
        /**
         * Model of the device the application has been run or is running on, as provided by the application.
         */
        public String deviceModel;
        /**
         * Operating system the application has been run or is running on, as provided by the application.
         */
        public String platform;
        /**
         * Version of the operating system the application has been run or is running on, as provided by the application.
         */
        public String systemVersion;
        /**
         * Point in time (Unix timestamp) when the user has logged in.
         */
        public int logInDate;
        /**
         * Point in time (Unix timestamp) when the session was last used.
         */
        public int lastActiveDate;
        /**
         * IP address from which the session was created, in human-readable format.
         */
        public String ip;
        /**
         * A two-letter country code for the country from which the session was created, based on the IP address.
         */
        public String country;
        /**
         * Region code from which the session was created, based on the IP address.
         */
        public String region;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1920553176;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of sessions.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Sessions extends Object {
        /**
         * List of sessions.
         */
        public Session[] sessions;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -463118121;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * One shipping option.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ShippingOption extends Object {
        /**
         * Shipping option identifier.
         */
        public String id;
        /**
         * Option title.
         */
        public String title;
        /**
         * A list of objects used to calculate the total shipping costs.
         */
        public LabeledPricePart[] priceParts;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1425690001;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a sticker.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Sticker extends Object {
        /**
         * The identifier of the sticker set to which the sticker belongs; 0 if none.
         */
        public long setId;
        /**
         * Sticker width; as defined by the sender.
         */
        public int width;
        /**
         * Sticker height; as defined by the sender.
         */
        public int height;
        /**
         * Emoji corresponding to the sticker.
         */
        public String emoji;
        /**
         * True, if the sticker is an animated sticker in TGS format.
         */
        public boolean isAnimated;
        /**
         * True, if the sticker is a mask.
         */
        public boolean isMask;
        /**
         * Position where the mask should be placed; may be null.
         */
        public MaskPosition maskPosition;
        /**
         * Sticker thumbnail in WEBP or JPEG format; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * File containing the sticker.
         */
        public File sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1835470627;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a sticker set.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class StickerSet extends Object {
        /**
         * Identifier of the sticker set.
         */
        public long id;
        /**
         * Title of the sticker set.
         */
        public String title;
        /**
         * Name of the sticker set.
         */
        public String name;
        /**
         * Sticker set thumbnail in WEBP format with width and height 100; may be null. The file can be downloaded only before the thumbnail is changed.
         */
        public PhotoSize thumbnail;
        /**
         * True, if the sticker set has been installed by the current user.
         */
        public boolean isInstalled;
        /**
         * True, if the sticker set has been archived. A sticker set can't be installed and archived simultaneously.
         */
        public boolean isArchived;
        /**
         * True, if the sticker set is official.
         */
        public boolean isOfficial;
        /**
         * True, is the stickers in the set are animated.
         */
        public boolean isAnimated;
        /**
         * True, if the stickers in the set are masks.
         */
        public boolean isMasks;
        /**
         * True for already viewed trending sticker sets.
         */
        public boolean isViewed;
        /**
         * List of stickers in this set.
         */
        public Sticker[] stickers;
        /**
         * A list of emoji corresponding to the stickers in the same order. The list is only for informational purposes, because a sticker is always sent with a fixed emoji from the corresponding Sticker object.
         */
        public Emojis[] emojis;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 734588298;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents short information about a sticker set.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class StickerSetInfo extends Object {
        /**
         * Identifier of the sticker set.
         */
        public long id;
        /**
         * Title of the sticker set.
         */
        public String title;
        /**
         * Name of the sticker set.
         */
        public String name;
        /**
         * Sticker set thumbnail in WEBP format with width and height 100; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * True, if the sticker set has been installed by current user.
         */
        public boolean isInstalled;
        /**
         * True, if the sticker set has been archived. A sticker set can't be installed and archived simultaneously.
         */
        public boolean isArchived;
        /**
         * True, if the sticker set is official.
         */
        public boolean isOfficial;
        /**
         * True, is the stickers in the set are animated.
         */
        public boolean isAnimated;
        /**
         * True, if the stickers in the set are masks.
         */
        public boolean isMasks;
        /**
         * True for already viewed trending sticker sets.
         */
        public boolean isViewed;
        /**
         * Total number of stickers in the set.
         */
        public int size;
        /**
         * Contains up to the first 5 stickers from the set, depending on the context. If the client needs more stickers the full set should be requested.
         */
        public Sticker[] covers;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 228054782;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a list of sticker sets.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class StickerSets extends Object {
        /**
         * Approximate total number of sticker sets found.
         */
        public int totalCount;
        /**
         * List of sticker sets.
         */
        public StickerSetInfo[] sets;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1883828812;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a list of stickers.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Stickers extends Object {
        /**
         * List of stickers.
         */
        public Sticker[] stickers;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1974859260;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the exact storage usage statistics split by chats and file type.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class StorageStatistics extends Object {
        /**
         * Total size of files.
         */
        public long size;
        /**
         * Total number of files.
         */
        public int count;
        /**
         * Statistics split by chats.
         */
        public StorageStatisticsByChat[] byChat;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 217237013;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the storage usage statistics for a specific chat.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class StorageStatisticsByChat extends Object {
        /**
         * Chat identifier; 0 if none.
         */
        public long chatId;
        /**
         * Total size of the files in the chat.
         */
        public long size;
        /**
         * Total number of files in the chat.
         */
        public int count;
        /**
         * Statistics split by file types.
         */
        public StorageStatisticsByFileType[] byFileType;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 635434531;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the storage usage statistics for a specific file type.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class StorageStatisticsByFileType extends Object {
        /**
         * File type.
         */
        public FileType fileType;
        /**
         * Total size of the files.
         */
        public long size;
        /**
         * Total number of files.
         */
        public int count;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 714012840;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains approximate storage usage statistics, excluding files of unknown file type.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class StorageStatisticsFast extends Object {
        /**
         * Approximate total size of files.
         */
        public long filesSize;
        /**
         * Approximate number of files.
         */
        public int fileCount;
        /**
         * Size of the database.
         */
        public long databaseSize;
        /**
         * Size of the language pack database.
         */
        public long languagePackDatabaseSize;
        /**
         * Size of the TDLib internal log.
         */
        public long logSize;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -884922271;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a supergroup or channel with zero or more members (subscribers in the case of channels). From the point of view of the system, a channel is a special kind of a supergroup: only administrators can post and see the list of members, and posts from all administrators use the name and photo of the channel instead of individual names and profile photos. Unlike supergroups, channels can have an unlimited number of subscribers.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Supergroup extends Object {
        /**
         * Supergroup or channel identifier.
         */
        public int id;
        /**
         * Username of the supergroup or channel; empty for private supergroups or channels.
         */
        public String username;
        /**
         * Point in time (Unix timestamp) when the current user joined, or the point in time when the supergroup or channel was created, in case the user is not a member.
         */
        public int date;
        /**
         * Status of the current user in the supergroup or channel.
         */
        public ChatMemberStatus status;
        /**
         * Member count; 0 if unknown. Currently it is guaranteed to be known only if the supergroup or channel was found through SearchPublicChats.
         */
        public int memberCount;
        /**
         * True, if messages sent to the channel should contain information about the sender. This field is only applicable to channels.
         */
        public boolean signMessages;
        /**
         * True, if the supergroup is a channel.
         */
        public boolean isChannel;
        /**
         * True, if the supergroup or channel is verified.
         */
        public boolean isVerified;
        /**
         * If non-empty, contains the reason why access to this supergroup or channel must be restricted. Format of the string is &quot;{type}: {description}&quot;. {type} Contains the type of the restriction and at least one of the suffixes &quot;-all&quot;, &quot;-ios&quot;, &quot;-android&quot;, or &quot;-wp&quot;, which describe the platforms on which access should be restricted. (For example, &quot;terms-ios-android&quot;. {description} contains a human-readable description of the restriction, which can be shown to the user.)
         */
        public String restrictionReason;
        /**
         * True, if many users reported this supergroup as a scam.
         */
        public boolean isScam;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1622883426;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains full information about a supergroup or channel.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SupergroupFullInfo extends Object {
        /**
         * Supergroup or channel description.
         */
        public String description;
        /**
         * Number of members in the supergroup or channel; 0 if unknown.
         */
        public int memberCount;
        /**
         * Number of privileged users in the supergroup or channel; 0 if unknown.
         */
        public int administratorCount;
        /**
         * Number of restricted users in the supergroup; 0 if unknown.
         */
        public int restrictedCount;
        /**
         * Number of users banned from chat; 0 if unknown.
         */
        public int bannedCount;
        /**
         * True, if members of the chat can be retrieved.
         */
        public boolean canGetMembers;
        /**
         * True, if the chat can be made public.
         */
        public boolean canSetUsername;
        /**
         * True, if the supergroup sticker set can be changed.
         */
        public boolean canSetStickerSet;
        /**
         * True, if the channel statistics is available through getChatStatisticsUrl.
         */
        public boolean canViewStatistics;
        /**
         * True, if new chat members will have access to old messages. In public supergroups and both public and private channels, old messages are always available, so this option affects only private supergroups. The value of this field is only available for chat administrators.
         */
        public boolean isAllHistoryAvailable;
        /**
         * Identifier of the supergroup sticker set; 0 if none.
         */
        public long stickerSetId;
        /**
         * Invite link for this chat.
         */
        public String inviteLink;
        /**
         * Identifier of the basic group from which supergroup was upgraded; 0 if none.
         */
        public int upgradedFromBasicGroupId;
        /**
         * Identifier of the last message in the basic group from which supergroup was upgraded; 0 if none.
         */
        public long upgradedFromMaxMessageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1524634784;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Specifies the kind of chat members to return in getSupergroupMembers.
     */
    public abstract static class SupergroupMembersFilter extends Object {
    }

    /**
     * Returns recently active users in reverse chronological order.
     */
    public static class SupergroupMembersFilterRecent extends SupergroupMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1178199509;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns contacts of the user, which are members of the supergroup or channel.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SupergroupMembersFilterContacts extends SupergroupMembersFilter {
        /**
         * Query to search for.
         */
        public String query;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1282910856;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the creator and administrators.
     */
    public static class SupergroupMembersFilterAdministrators extends SupergroupMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2097380265;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Used to search for supergroup or channel members via a (string) query.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SupergroupMembersFilterSearch extends SupergroupMembersFilter {
        /**
         * Query to search for.
         */
        public String query;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1696358469;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns restricted supergroup members; can be used only by administrators.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SupergroupMembersFilterRestricted extends SupergroupMembersFilter {
        /**
         * Query to search for.
         */
        public String query;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1107800034;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns users banned from the supergroup or channel; can be used only by administrators.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SupergroupMembersFilterBanned extends SupergroupMembersFilter {
        /**
         * Query to search for.
         */
        public String query;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1210621683;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns bot members of the supergroup or channel.
     */
    public static class SupergroupMembersFilterBots extends SupergroupMembersFilter {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 492138918;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a URL linking to an internal Telegram entity.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TMeUrl extends Object {
        /**
         * URL.
         */
        public String url;
        /**
         * Type of the URL.
         */
        public TMeUrlType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1140786622;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the type of a URL linking to an internal Telegram entity.
     */
    public abstract static class TMeUrlType extends Object {
    }

    /**
     * A URL linking to a user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TMeUrlTypeUser extends TMeUrlType {
        /**
         * Identifier of the user.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1198700130;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A URL linking to a public supergroup or channel.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TMeUrlTypeSupergroup extends TMeUrlType {
        /**
         * Identifier of the supergroup or channel.
         */
        public long supergroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1353369944;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat invite link.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TMeUrlTypeChatInvite extends TMeUrlType {
        /**
         * Chat invite link info.
         */
        public ChatInviteLinkInfo info;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 313907785;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A URL linking to a sticker set.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TMeUrlTypeStickerSet extends TMeUrlType {
        /**
         * Identifier of the sticker set.
         */
        public long stickerSetId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1602473196;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of t.me URLs.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TMeUrls extends Object {
        /**
         * List of URLs.
         */
        public TMeUrl[] urls;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1130595098;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains parameters for TDLib initialization.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TdlibParameters extends Object {
        /**
         * If set to true, the Telegram test environment will be used instead of the production environment.
         */
        public boolean useTestDc;
        /**
         * The path to the directory for the persistent database; if empty, the current working directory will be used.
         */
        public String databaseDirectory;
        /**
         * The path to the directory for storing files; if empty, databaseDirectory will be used.
         */
        public String filesDirectory;
        /**
         * If set to true, information about downloaded and uploaded files will be saved between application restarts.
         */
        public boolean useFileDatabase;
        /**
         * If set to true, the library will maintain a cache of users, basic groups, supergroups, channels and secret chats. Implies useFileDatabase.
         */
        public boolean useChatInfoDatabase;
        /**
         * If set to true, the library will maintain a cache of chats and messages. Implies useChatInfoDatabase.
         */
        public boolean useMessageDatabase;
        /**
         * If set to true, support for secret chats will be enabled.
         */
        public boolean useSecretChats;
        /**
         * Application identifier for Telegram API access, which can be obtained at https://my.telegram.org.
         */
        public int apiId;
        /**
         * Application identifier hash for Telegram API access, which can be obtained at https://my.telegram.org.
         */
        public String apiHash;
        /**
         * IETF language tag of the user's operating system language; must be non-empty.
         */
        public String systemLanguageCode;
        /**
         * Model of the device the application is being run on; must be non-empty.
         */
        public String deviceModel;
        /**
         * Version of the operating system the application is being run on; must be non-empty.
         */
        public String systemVersion;
        /**
         * Application version; must be non-empty.
         */
        public String applicationVersion;
        /**
         * If set to true, old files will automatically be deleted.
         */
        public boolean enableStorageOptimizer;
        /**
         * If set to true, original file names will be ignored. Otherwise, downloaded files will be saved under names as close as possible to the original name.
         */
        public boolean ignoreFileNames;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -761520773;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about the availability of a temporary password, which can be used for payments.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TemporaryPasswordState extends Object {
        /**
         * True, if a temporary password is available.
         */
        public boolean hasPassword;
        /**
         * Time left before the temporary password expires, in seconds.
         */
        public int validFor;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 939837410;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains Telegram terms of service.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TermsOfService extends Object {
        /**
         * Text of the terms of service.
         */
        public FormattedText text;
        /**
         * Minimum age of a user to be able to accept the terms; 0 if any.
         */
        public int minUserAge;
        /**
         * True, if a blocking popup with terms of service must be shown to the user.
         */
        public boolean showPopup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 739422597;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A simple object containing a sequence of bytes; for testing only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestBytes extends Object {
        /**
         * Bytes.
         */
        public byte[] value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1541225250;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A simple object containing a number; for testing only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestInt extends Object {
        /**
         * Number.
         */
        public int value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -574804983;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A simple object containing a string; for testing only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestString extends Object {
        /**
         * String.
         */
        public String value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -27891572;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A simple object containing a vector of numbers; for testing only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestVectorInt extends Object {
        /**
         * Vector of numbers.
         */
        public int[] value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 593682027;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A simple object containing a vector of objects that hold a number; for testing only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestVectorIntObject extends Object {
        /**
         * Vector of objects.
         */
        public TestInt[] value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 125891546;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A simple object containing a vector of strings; for testing only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestVectorString extends Object {
        /**
         * Vector of strings.
         */
        public String[] value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 79339995;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A simple object containing a vector of objects that hold a string; for testing only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestVectorStringObject extends Object {
        /**
         * Vector of objects.
         */
        public TestString[] value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 80780537;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains some text.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Text extends Object {
        /**
         * Text.
         */
        public String text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 578181272;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of text entities.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TextEntities extends Object {
        /**
         * List of text entities.
         */
        public TextEntity[] entities;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -933199172;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a part of the text that needs to be formatted in some unusual way.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TextEntity extends Object {
        /**
         * Offset of the entity in UTF-16 code points.
         */
        public int offset;
        /**
         * Length of the entity, in UTF-16 code points.
         */
        public int length;
        /**
         * Type of the entity.
         */
        public TextEntityType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1951688280;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a part of the text which must be formatted differently.
     */
    public abstract static class TextEntityType extends Object {
    }

    /**
     * A mention of a user by their username.
     */
    public static class TextEntityTypeMention extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 934535013;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A hashtag text, beginning with &quot;#&quot;.
     */
    public static class TextEntityTypeHashtag extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1023958307;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A cashtag text, beginning with &quot;$&quot; and consisting of capital english letters (i.e. &quot;$USD&quot;).
     */
    public static class TextEntityTypeCashtag extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1222915915;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A bot command, beginning with &quot;/&quot;. This shouldn't be highlighted if there are no bots in the chat.
     */
    public static class TextEntityTypeBotCommand extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1150997581;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An HTTP URL.
     */
    public static class TextEntityTypeUrl extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1312762756;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An email address.
     */
    public static class TextEntityTypeEmailAddress extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1425545249;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A bold text.
     */
    public static class TextEntityTypeBold extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1128210000;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An italic text.
     */
    public static class TextEntityTypeItalic extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -118253987;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Text that must be formatted as if inside a code HTML tag.
     */
    public static class TextEntityTypeCode extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -974534326;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Text that must be formatted as if inside a pre HTML tag.
     */
    public static class TextEntityTypePre extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1648958606;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Text that must be formatted as if inside pre, and code HTML tags.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TextEntityTypePreCode extends TextEntityType {
        /**
         * Programming language of the code; as defined by the sender.
         */
        public String language;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -945325397;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A text description shown instead of a raw URL.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TextEntityTypeTextUrl extends TextEntityType {
        /**
         * HTTP or tg:// URL to be opened when the link is clicked.
         */
        public String url;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 445719651;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A text shows instead of a raw mention of the user (e.g., when the user has no username).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TextEntityTypeMentionName extends TextEntityType {
        /**
         * Identifier of the mentioned user.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -791517091;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A phone number.
     */
    public static class TextEntityTypePhoneNumber extends TextEntityType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1160140246;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the way the text should be parsed for TextEntities.
     */
    public abstract static class TextParseMode extends Object {
    }

    /**
     * The text should be parsed in markdown-style.
     */
    public static class TextParseModeMarkdown extends TextParseMode {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 969225580;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The text should be parsed in HTML-style.
     */
    public static class TextParseModeHTML extends TextParseMode {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1660208627;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the response of a request to TON lite server.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TonLiteServerResponse extends Object {
        /**
         * The response.
         */
        public byte[] response;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 928306959;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains the salt to be used with locally stored password to access a local TON-based wallet.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TonWalletPasswordSalt extends Object {
        /**
         * The salt.
         */
        public byte[] salt;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1406795233;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents the categories of chats for which a list of frequently used chats can be retrieved.
     */
    public abstract static class TopChatCategory extends Object {
    }

    /**
     * A category containing frequently used private chats with non-bot users.
     */
    public static class TopChatCategoryUsers extends TopChatCategory {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1026706816;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A category containing frequently used private chats with bot users.
     */
    public static class TopChatCategoryBots extends TopChatCategory {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1577129195;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A category containing frequently used basic groups and supergroups.
     */
    public static class TopChatCategoryGroups extends TopChatCategory {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1530056846;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A category containing frequently used channels.
     */
    public static class TopChatCategoryChannels extends TopChatCategory {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -500825885;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A category containing frequently used chats with inline bots sorted by their usage in inline mode.
     */
    public static class TopChatCategoryInlineBots extends TopChatCategory {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 377023356;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A category containing frequently used chats used for calls.
     */
    public static class TopChatCategoryCalls extends TopChatCategory {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 356208861;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Contains notifications about data changes.
     */
    public abstract static class Update extends Object {
    }

    /**
     * The user authorization state has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateAuthorizationState extends Update {
        /**
         * New authorization state.
         */
        public AuthorizationState authorizationState;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1622347490;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new message was received; can also be an outgoing message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewMessage extends Update {
        /**
         * The new message.
         */
        public Message message;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -563105266;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A request to send a message has reached the Telegram server. This doesn't mean that the message will be sent successfully or even that the send message request will be processed. This update will be sent only if the option &quot;use_quick_ack&quot; is set to true. This update may be sent multiple times for the same message.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateMessageSendAcknowledged extends Update {
        /**
         * The chat identifier of the sent message.
         */
        public long chatId;
        /**
         * A temporary message identifier.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1302843961;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message has been successfully sent.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateMessageSendSucceeded extends Update {
        /**
         * Information about the sent message. Usually only the message identifier, date, and content are changed, but almost all other fields can also change.
         */
        public Message message;
        /**
         * The previous temporary message identifier.
         */
        public long oldMessageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1815715197;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message failed to send. Be aware that some messages being sent can be irrecoverably deleted, in which case updateDeleteMessages will be received instead of this update.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateMessageSendFailed extends Update {
        /**
         * Contains information about the message which failed to send.
         */
        public Message message;
        /**
         * The previous temporary message identifier.
         */
        public long oldMessageId;
        /**
         * An error code.
         */
        public int errorCode;
        /**
         * Error message.
         */
        public String errorMessage;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1032335779;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The message content has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateMessageContent extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * New message content.
         */
        public MessageContent newContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 506903332;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message was edited. Changes in the message content will come in a separate updateMessageContent.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateMessageEdited extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * Point in time (Unix timestamp) when the message was edited.
         */
        public int editDate;
        /**
         * New message reply markup; may be null.
         */
        public ReplyMarkup replyMarkup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -559545626;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The view count of the message has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateMessageViews extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * New value of the view count.
         */
        public int views;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1854131125;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The message content was opened. Updates voice note messages to &quot;listened&quot;, video note messages to &quot;viewed&quot; and starts the TTL timer for self-destructing messages.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateMessageContentOpened extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1520523131;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A message with an unread mention was read.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateMessageMentionRead extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * The new number of unread mention messages left in the chat.
         */
        public int unreadMentionCount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -252228282;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new chat has been loaded/created. This update is guaranteed to come before the chat identifier is returned to the client. The chat field changes will be reported through separate updates.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewChat extends Update {
        /**
         * The chat.
         */
        public Chat chat;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2075757773;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The title of a chat was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatTitle extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The new chat title.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -175405660;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat photo was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatPhoto extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The new chat photo; may be null.
         */
        public ChatPhoto photo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -209353966;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Chat permissions was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatPermissions extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The new chat permissions.
         */
        public ChatPermissions permissions;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1622010003;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The last message of a chat was changed. If lastMessage is null, then the last message in the chat became unknown. Some new unknown messages might be added to the chat in this case.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatLastMessage extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The new last message in the chat; may be null.
         */
        public Message lastMessage;
        /**
         * New value of the chat order.
         */
        public long order;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 580348828;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The order of the chat in the chat list has changed. Instead of this update updateChatLastMessage, updateChatIsPinned or updateChatDraftMessage might be sent.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatOrder extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New value of the order.
         */
        public long order;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1601888026;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat was pinned or unpinned.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatIsPinned extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New value of isPinned.
         */
        public boolean isPinned;
        /**
         * New value of the chat order.
         */
        public long order;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 488876260;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat was marked as unread or was read.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatIsMarkedAsUnread extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New value of isMarkedAsUnread.
         */
        public boolean isMarkedAsUnread;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1468347188;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat's isSponsored field has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatIsSponsored extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New value of isSponsored.
         */
        public boolean isSponsored;
        /**
         * New value of chat order.
         */
        public long order;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1196180070;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The value of the default disableNotification parameter, used when a message is sent to the chat, was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatDefaultDisableNotification extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The new defaultDisableNotification value.
         */
        public boolean defaultDisableNotification;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 464087707;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Incoming messages were read or number of unread messages has been changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatReadInbox extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier of the last read incoming message.
         */
        public long lastReadInboxMessageId;
        /**
         * The number of unread messages left in the chat.
         */
        public int unreadCount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -797952281;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Outgoing messages were read.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatReadOutbox extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier of last read outgoing message.
         */
        public long lastReadOutboxMessageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 708334213;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat unreadMentionCount has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatUnreadMentionCount extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The number of unread mention messages left in the chat.
         */
        public int unreadMentionCount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2131461348;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Notification settings for a chat were changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatNotificationSettings extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The new notification settings.
         */
        public ChatNotificationSettings notificationSettings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -803163050;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Notification settings for some type of chats were updated.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateScopeNotificationSettings extends Update {
        /**
         * Types of chats for which notification settings were updated.
         */
        public NotificationSettingsScope scope;
        /**
         * The new notification settings.
         */
        public ScopeNotificationSettings notificationSettings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1203975309;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The chat pinned message was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatPinnedMessage extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The new identifier of the pinned message; 0 if there is no pinned message in the chat.
         */
        public long pinnedMessageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 802160507;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The default chat reply markup was changed. Can occur because new messages with reply markup were received or because an old reply markup was hidden by the user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatReplyMarkup extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier of the message from which reply markup needs to be used; 0 if there is no default custom reply markup in the chat.
         */
        public long replyMarkupMessageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1309386144;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A chat draft has changed. Be aware that the update may come in the currently opened chat but with old content of the draft. If the user has changed the content of the draft, this update shouldn't be applied.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatDraftMessage extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The new draft message; may be null.
         */
        public DraftMessage draftMessage;
        /**
         * New value of the chat order.
         */
        public long order;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1436617498;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The number of online group members has changed. This update with non-zero count is sent only for currently opened chats. There is no guarantee that it will be sent just after the count has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateChatOnlineMemberCount extends Update {
        /**
         * Identifier of the chat.
         */
        public long chatId;
        /**
         * New number of online members in the chat, or 0 if unknown.
         */
        public int onlineMemberCount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 487369373;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A notification was changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNotification extends Update {
        /**
         * Unique notification group identifier.
         */
        public int notificationGroupId;
        /**
         * Changed notification.
         */
        public Notification notification;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1897496876;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A list of active notifications in a notification group has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNotificationGroup extends Update {
        /**
         * Unique notification group identifier.
         */
        public int notificationGroupId;
        /**
         * New type of the notification group.
         */
        public NotificationGroupType type;
        /**
         * Identifier of a chat to which all notifications in the group belong.
         */
        public long chatId;
        /**
         * Chat identifier, which notification settings must be applied to the added notifications.
         */
        public long notificationSettingsChatId;
        /**
         * True, if the notifications should be shown without sound.
         */
        public boolean isSilent;
        /**
         * Total number of unread notifications in the group, can be bigger than number of active notifications.
         */
        public int totalCount;
        /**
         * List of added group notifications, sorted by notification ID.
         */
        public Notification[] addedNotifications;
        /**
         * Identifiers of removed group notifications, sorted by notification ID.
         */
        public int[] removedNotificationIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2049005665;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains active notifications that was shown on previous application launches. This update is sent only if a message database is used. In that case it comes once before any updateNotification and updateNotificationGroup update.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateActiveNotifications extends Update {
        /**
         * Lists of active notification groups.
         */
        public NotificationGroup[] groups;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1306672221;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes, whether there are some pending notification updates. Can be used to prevent application from killing, while there are some pending notifications.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateHavePendingNotifications extends Update {
        /**
         * True, if there are some delayed notification updates, which will be sent soon.
         */
        public boolean haveDelayedNotifications;
        /**
         * True, if there can be some yet unreceived notifications, which are being fetched from the server.
         */
        public boolean haveUnreceivedNotifications;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 179233243;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some messages were deleted.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateDeleteMessages extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifiers of the deleted messages.
         */
        public long[] messageIds;
        /**
         * True, if the messages are permanently deleted by a user (as opposed to just becoming inaccessible).
         */
        public boolean isPermanent;
        /**
         * True, if the messages are deleted only from the cache and can possibly be retrieved again in the future.
         */
        public boolean fromCache;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1669252686;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * User activity in the chat has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateUserChatAction extends Update {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier of a user performing an action.
         */
        public int userId;
        /**
         * The action description.
         */
        public ChatAction action;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1444133514;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user went online or offline.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateUserStatus extends Update {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * New status of the user.
         */
        public UserStatus status;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1443545195;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some data of a user has changed. This update is guaranteed to come before the user identifier is returned to the client.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateUser extends Update {
        /**
         * New data about the user.
         */
        public User user;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1183394041;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some data of a basic group has changed. This update is guaranteed to come before the basic group identifier is returned to the client.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateBasicGroup extends Update {
        /**
         * New data about the group.
         */
        public BasicGroup basicGroup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1003239581;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some data of a supergroup or a channel has changed. This update is guaranteed to come before the supergroup identifier is returned to the client.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateSupergroup extends Update {
        /**
         * New data about the supergroup.
         */
        public Supergroup supergroup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -76782300;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some data of a secret chat has changed. This update is guaranteed to come before the secret chat identifier is returned to the client.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateSecretChat extends Update {
        /**
         * New data about the secret chat.
         */
        public SecretChat secretChat;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1666903253;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some data from userFullInfo has been changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateUserFullInfo extends Update {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * New full information about the user.
         */
        public UserFullInfo userFullInfo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 222103874;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some data from basicGroupFullInfo has been changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateBasicGroupFullInfo extends Update {
        /**
         * Identifier of a basic group.
         */
        public int basicGroupId;
        /**
         * New full information about the group.
         */
        public BasicGroupFullInfo basicGroupFullInfo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 924030531;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some data from supergroupFullInfo has been changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateSupergroupFullInfo extends Update {
        /**
         * Identifier of the supergroup or channel.
         */
        public int supergroupId;
        /**
         * New full information about the supergroup.
         */
        public SupergroupFullInfo supergroupFullInfo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1288828758;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Service notification from the server. Upon receiving this the client must show a popup with the content of the notification.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateServiceNotification extends Update {
        /**
         * Notification type. If type begins with &quot;AUTH_KEY_DROP_&quot;, then two buttons &quot;Cancel&quot; and &quot;Log out&quot; should be shown under notification; if user presses the second, all local data should be destroyed using Destroy method.
         */
        public String type;
        /**
         * Notification content.
         */
        public MessageContent content;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1318622637;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Information about a file was updated.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateFile extends Update {
        /**
         * New data about the file.
         */
        public File file;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 114132831;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The file generation process needs to be started by the client.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateFileGenerationStart extends Update {
        /**
         * Unique identifier for the generation process.
         */
        public long generationId;
        /**
         * The path to a file from which a new file is generated; may be empty.
         */
        public String originalPath;
        /**
         * The path to a file that should be created and where the new file should be generated.
         */
        public String destinationPath;
        /**
         * String specifying the conversion applied to the original file. If conversion is &quot;#url#&quot; than originalPath contains an HTTP/HTTPS URL of a file, which should be downloaded by the client.
         */
        public String conversion;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 216817388;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * File generation is no longer needed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateFileGenerationStop extends Update {
        /**
         * Unique identifier for the generation process.
         */
        public long generationId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1894449685;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * New call was created or information about a call was updated.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateCall extends Update {
        /**
         * New data about a call.
         */
        public Call call;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1337184477;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some privacy setting rules have been changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateUserPrivacySettingRules extends Update {
        /**
         * The privacy setting.
         */
        public UserPrivacySetting setting;
        /**
         * New privacy rules.
         */
        public UserPrivacySettingRules rules;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -912960778;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Number of unread messages has changed. This update is sent only if a message database is used.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateUnreadMessageCount extends Update {
        /**
         * Total number of unread messages.
         */
        public int unreadCount;
        /**
         * Total number of unread messages in unmuted chats.
         */
        public int unreadUnmutedCount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -824420376;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Number of unread chats, i.e. with unread messages or marked as unread, has changed. This update is sent only if a message database is used.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateUnreadChatCount extends Update {
        /**
         * Total number of unread chats.
         */
        public int unreadCount;
        /**
         * Total number of unread unmuted chats.
         */
        public int unreadUnmutedCount;
        /**
         * Total number of chats marked as unread.
         */
        public int markedAsUnreadCount;
        /**
         * Total number of unmuted chats marked as unread.
         */
        public int markedAsUnreadUnmutedCount;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 891150304;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * An option changed its value.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateOption extends Update {
        /**
         * The option name.
         */
        public String name;
        /**
         * The new option value.
         */
        public OptionValue value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 900822020;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The list of installed sticker sets was updated.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateInstalledStickerSets extends Update {
        /**
         * True, if the list of installed mask sticker sets was updated.
         */
        public boolean isMasks;
        /**
         * The new list of installed ordinary sticker sets.
         */
        public long[] stickerSetIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1125575977;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The list of trending sticker sets was updated or some of them were viewed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateTrendingStickerSets extends Update {
        /**
         * The new list of trending sticker sets.
         */
        public StickerSets stickerSets;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 450714593;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The list of recently used stickers was updated.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateRecentStickers extends Update {
        /**
         * True, if the list of stickers attached to photo or video files was updated, otherwise the list of sent stickers is updated.
         */
        public boolean isAttached;
        /**
         * The new list of file identifiers of recently used stickers.
         */
        public int[] stickerIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1906403540;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The list of favorite stickers was updated.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateFavoriteStickers extends Update {
        /**
         * The new list of file identifiers of favorite stickers.
         */
        public int[] stickerIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1662240999;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The list of saved animations was updated.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateSavedAnimations extends Update {
        /**
         * The new list of file identifiers of saved animations.
         */
        public int[] animationIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 65563814;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The selected background has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateSelectedBackground extends Update {
        /**
         * True, if background for dark theme has changed.
         */
        public boolean forDarkTheme;
        /**
         * The new selected background; may be null.
         */
        public Background background;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1715658659;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Some language pack strings have been updated.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateLanguagePackStrings extends Update {
        /**
         * Localization target to which the language pack belongs.
         */
        public String localizationTarget;
        /**
         * Identifier of the updated language pack.
         */
        public String languagePackId;
        /**
         * List of changed language pack strings.
         */
        public LanguagePackString[] strings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1056319886;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The connection state has changed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateConnectionState extends Update {
        /**
         * The new connection state.
         */
        public ConnectionState state;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1469292078;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * New terms of service must be accepted by the user. If the terms of service are declined, then the deleteAccount method should be called with the reason &quot;Decline ToS update&quot;.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateTermsOfService extends Update {
        /**
         * Identifier of the terms of service.
         */
        public String termsOfServiceId;
        /**
         * The new terms of service.
         */
        public TermsOfService termsOfService;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1304640162;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new incoming inline query; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewInlineQuery extends Update {
        /**
         * Unique query identifier.
         */
        public long id;
        /**
         * Identifier of the user who sent the query.
         */
        public int senderUserId;
        /**
         * User location, provided by the client; may be null.
         */
        public Location userLocation;
        /**
         * Text of the query.
         */
        public String query;
        /**
         * Offset of the first entry to return.
         */
        public String offset;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2064730634;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user has chosen a result of an inline query; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewChosenInlineResult extends Update {
        /**
         * Identifier of the user who sent the query.
         */
        public int senderUserId;
        /**
         * User location, provided by the client; may be null.
         */
        public Location userLocation;
        /**
         * Text of the query.
         */
        public String query;
        /**
         * Identifier of the chosen result.
         */
        public String resultId;
        /**
         * Identifier of the sent inline message, if known.
         */
        public String inlineMessageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 527526965;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new incoming callback query; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewCallbackQuery extends Update {
        /**
         * Unique query identifier.
         */
        public long id;
        /**
         * Identifier of the user who sent the query.
         */
        public int senderUserId;
        /**
         * Identifier of the chat, in which the query was sent.
         */
        public long chatId;
        /**
         * Identifier of the message, from which the query originated.
         */
        public long messageId;
        /**
         * Identifier that uniquely corresponds to the chat to which the message was sent.
         */
        public long chatInstance;
        /**
         * Query payload.
         */
        public CallbackQueryPayload payload;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2044226370;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new incoming callback query from a message sent via a bot; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewInlineCallbackQuery extends Update {
        /**
         * Unique query identifier.
         */
        public long id;
        /**
         * Identifier of the user who sent the query.
         */
        public int senderUserId;
        /**
         * Identifier of the inline message, from which the query originated.
         */
        public String inlineMessageId;
        /**
         * An identifier uniquely corresponding to the chat a message was sent to.
         */
        public long chatInstance;
        /**
         * Query payload.
         */
        public CallbackQueryPayload payload;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1879154829;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new incoming shipping query; for bots only. Only for invoices with flexible price.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewShippingQuery extends Update {
        /**
         * Unique query identifier.
         */
        public long id;
        /**
         * Identifier of the user who sent the query.
         */
        public int senderUserId;
        /**
         * Invoice payload.
         */
        public String invoicePayload;
        /**
         * User shipping address.
         */
        public Address shippingAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -817474682;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new incoming pre-checkout query; for bots only. Contains full information about a checkout.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewPreCheckoutQuery extends Update {
        /**
         * Unique query identifier.
         */
        public long id;
        /**
         * Identifier of the user who sent the query.
         */
        public int senderUserId;
        /**
         * Currency for the product price.
         */
        public String currency;
        /**
         * Total price for the product, in the minimal quantity of the currency.
         */
        public long totalAmount;
        /**
         * Invoice payload.
         */
        public byte[] invoicePayload;
        /**
         * Identifier of a shipping option chosen by the user; may be empty if not applicable.
         */
        public String shippingOptionId;
        /**
         * Information about the order; may be null.
         */
        public OrderInfo orderInfo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 87964006;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new incoming event; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewCustomEvent extends Update {
        /**
         * A JSON-serialized event.
         */
        public String event;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1994222092;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A new incoming query; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdateNewCustomQuery extends Update {
        /**
         * The query identifier.
         */
        public long id;
        /**
         * JSON-serialized query data.
         */
        public String data;
        /**
         * Query timeout.
         */
        public int timeout;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -687670874;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Information about a poll was updated; for bots only.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpdatePoll extends Update {
        /**
         * New data about the poll.
         */
        public Poll poll;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1771342902;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a list of updates.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Updates extends Object {
        /**
         * List of updates.
         */
        public Update[] updates;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 475842347;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a user.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class User extends Object {
        /**
         * User identifier.
         */
        public int id;
        /**
         * First name of the user.
         */
        public String firstName;
        /**
         * Last name of the user.
         */
        public String lastName;
        /**
         * Username of the user.
         */
        public String username;
        /**
         * Phone number of the user.
         */
        public String phoneNumber;
        /**
         * Current online status of the user.
         */
        public UserStatus status;
        /**
         * Profile photo of the user; may be null.
         */
        public ProfilePhoto profilePhoto;
        /**
         * Relationship from the current user to the other user.
         */
        public LinkState outgoingLink;
        /**
         * Relationship from the other user to the current user.
         */
        public LinkState incomingLink;
        /**
         * True, if the user is verified.
         */
        public boolean isVerified;
        /**
         * True, if the user is Telegram support account.
         */
        public boolean isSupport;
        /**
         * If non-empty, it contains the reason why access to this user must be restricted. The format of the string is &quot;{type}: {description}&quot;. {type} contains the type of the restriction and at least one of the suffixes &quot;-all&quot;, &quot;-ios&quot;, &quot;-android&quot;, or &quot;-wp&quot;, which describe the platforms on which access should be restricted. (For example, &quot;terms-ios-android&quot;. {description} contains a human-readable description of the restriction, which can be shown to the user.)
         */
        public String restrictionReason;
        /**
         * True, if many users reported this user as a scam.
         */
        public boolean isScam;
        /**
         * If false, the user is inaccessible, and the only information known about the user is inside this class. It can't be passed to any method except GetUser.
         */
        public boolean haveAccess;
        /**
         * Type of the user.
         */
        public UserType type;
        /**
         * IETF language tag of the user's language; only available to bots.
         */
        public String languageCode;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 56535118;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains full information about a user (except the full list of profile photos).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserFullInfo extends Object {
        /**
         * True, if the user is blacklisted by the current user.
         */
        public boolean isBlocked;
        /**
         * True, if the user can be called.
         */
        public boolean canBeCalled;
        /**
         * True, if the user can't be called due to their privacy settings.
         */
        public boolean hasPrivateCalls;
        /**
         * A short user bio.
         */
        public String bio;
        /**
         * For bots, the text that is included with the link when users share the bot.
         */
        public String shareText;
        /**
         * Number of group chats where both the other user and the current user are a member; 0 for the current user.
         */
        public int groupInCommonCount;
        /**
         * If the user is a bot, information about the bot; may be null.
         */
        public BotInfo botInfo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1076948004;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes available user privacy settings.
     */
    public abstract static class UserPrivacySetting extends Object {
    }

    /**
     * A privacy setting for managing whether the user's online status is visible.
     */
    public static class UserPrivacySettingShowStatus extends UserPrivacySetting {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1862829310;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A privacy setting for managing whether the user's profile photo is visible.
     */
    public static class UserPrivacySettingShowProfilePhoto extends UserPrivacySetting {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1408485877;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A privacy setting for managing whether a link to the user's account is included in forwarded messages.
     */
    public static class UserPrivacySettingShowLinkInForwardedMessages extends UserPrivacySetting {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 592688870;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A privacy setting for managing whether the user can be invited to chats.
     */
    public static class UserPrivacySettingAllowChatInvites extends UserPrivacySetting {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1271668007;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A privacy setting for managing whether the user can be called.
     */
    public static class UserPrivacySettingAllowCalls extends UserPrivacySetting {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -906967291;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A privacy setting for managing whether peer-to-peer connections can be used for calls.
     */
    public static class UserPrivacySettingAllowPeerToPeerCalls extends UserPrivacySetting {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 352500032;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents a single rule for managing privacy settings.
     */
    public abstract static class UserPrivacySettingRule extends Object {
    }

    /**
     * A rule to allow all users to do something.
     */
    public static class UserPrivacySettingRuleAllowAll extends UserPrivacySettingRule {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1967186881;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rule to allow all of a user's contacts to do something.
     */
    public static class UserPrivacySettingRuleAllowContacts extends UserPrivacySettingRule {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1892733680;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rule to allow certain specified users to do something.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserPrivacySettingRuleAllowUsers extends UserPrivacySettingRule {
        /**
         * The user identifiers.
         */
        public int[] userIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 427601278;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rule to restrict all users from doing something.
     */
    public static class UserPrivacySettingRuleRestrictAll extends UserPrivacySettingRule {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1406495408;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rule to restrict all contacts of a user from doing something.
     */
    public static class UserPrivacySettingRuleRestrictContacts extends UserPrivacySettingRule {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1008389378;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A rule to restrict all specified users from doing something.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserPrivacySettingRuleRestrictUsers extends UserPrivacySettingRule {
        /**
         * The user identifiers.
         */
        public int[] userIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2119951802;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A list of privacy rules. Rules are matched in the specified order. The first matched rule defines the privacy setting for a given user. If no rule matches, the action is not allowed.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserPrivacySettingRules extends Object {
        /**
         * A list of rules.
         */
        public UserPrivacySettingRule[] rules;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 322477541;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains full information about a user profile photo.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserProfilePhoto extends Object {
        /**
         * Unique user profile photo identifier.
         */
        public long id;
        /**
         * Point in time (Unix timestamp) when the photo has been added.
         */
        public int addedDate;
        /**
         * Available variants of the user photo, in different sizes.
         */
        public PhotoSize[] sizes;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1882596466;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains part of the list of user photos.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserProfilePhotos extends Object {
        /**
         * Total number of user profile photos.
         */
        public int totalCount;
        /**
         * A list of photos.
         */
        public UserProfilePhoto[] photos;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1512709690;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Describes the last time the user was online.
     */
    public abstract static class UserStatus extends Object {
    }

    /**
     * The user status was never changed.
     */
    public static class UserStatusEmpty extends UserStatus {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 164646985;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is online.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserStatusOnline extends UserStatus {
        /**
         * Point in time (Unix timestamp) when the user's online status will expire.
         */
        public int expires;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1529460876;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is offline.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserStatusOffline extends UserStatus {
        /**
         * Point in time (Unix timestamp) when the user was last online.
         */
        public int wasOnline;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -759984891;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user was online recently.
     */
    public static class UserStatusRecently extends UserStatus {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -496024847;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is offline, but was online last week.
     */
    public static class UserStatusLastWeek extends UserStatus {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 129960444;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * The user is offline, but was online last month.
     */
    public static class UserStatusLastMonth extends UserStatus {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2011940674;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * This class is an abstract base class.
     * Represents the type of the user. The following types are possible: regular users, deleted users and bots.
     */
    public abstract static class UserType extends Object {
    }

    /**
     * A regular user.
     */
    public static class UserTypeRegular extends UserType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -598644325;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A deleted user or deleted bot. No information on the user besides the userId is available. It is not possible to perform any active actions on this type of user.
     */
    public static class UserTypeDeleted extends UserType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1807729372;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * A bot (see https://core.telegram.org/bots).
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserTypeBot extends UserType {
        /**
         * True, if the bot can be invited to basic group and supergroup chats.
         */
        public boolean canJoinGroups;
        /**
         * True, if the bot can read all messages in basic group or supergroup chats and not just those addressed to the bot. In private and channel chats a bot can always read all messages.
         */
        public boolean canReadAllGroupMessages;
        /**
         * True, if the bot supports inline queries.
         */
        public boolean isInline;
        /**
         * Placeholder for inline queries (displayed on the client input field).
         */
        public String inlineQueryPlaceholder;
        /**
         * True, if the location of the user should be sent with every inline query to this bot.
         */
        public boolean needLocation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1262387765;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * No information on the user besides the userId is available, yet this user has not been deleted. This object is extremely rare and must be handled like a deleted user. It is not possible to perform any actions on users of this type.
     */
    public static class UserTypeUnknown extends UserType {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -724541123;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Represents a list of users.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Users extends Object {
        /**
         * Approximate total count of users found.
         */
        public int totalCount;
        /**
         * A list of user identifiers.
         */
        public int[] userIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 273760088;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Contains a temporary identifier of validated order information, which is stored for one hour. Also contains the available shipping options.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ValidatedOrderInfo extends Object {
        /**
         * Temporary identifier of the order information.
         */
        public String orderInfoId;
        /**
         * Available shipping options.
         */
        public ShippingOption[] shippingOptions;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1511451484;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a venue.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Venue extends Object {
        /**
         * Venue location; as defined by the sender.
         */
        public Location location;
        /**
         * Venue name; as defined by the sender.
         */
        public String title;
        /**
         * Venue address; as defined by the sender.
         */
        public String address;
        /**
         * Provider of the venue database; as defined by the sender. Currently only &quot;foursquare&quot; needs to be supported.
         */
        public String provider;
        /**
         * Identifier of the venue in the provider database; as defined by the sender.
         */
        public String id;
        /**
         * Type of the venue in the provider database; as defined by the sender.
         */
        public String type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1070406393;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a video file.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Video extends Object {
        /**
         * Duration of the video, in seconds; as defined by the sender.
         */
        public int duration;
        /**
         * Video width; as defined by the sender.
         */
        public int width;
        /**
         * Video height; as defined by the sender.
         */
        public int height;
        /**
         * Original name of the file; as defined by the sender.
         */
        public String fileName;
        /**
         * MIME type of the file; as defined by the sender.
         */
        public String mimeType;
        /**
         * True, if stickers were added to the video.
         */
        public boolean hasStickers;
        /**
         * True, if the video should be tried to be streamed.
         */
        public boolean supportsStreaming;
        /**
         * Video minithumbnail; may be null.
         */
        public Minithumbnail minithumbnail;
        /**
         * Video thumbnail; as defined by the sender; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * File containing the video.
         */
        public File video;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -536898740;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a video note. The video must be equal in width and height, cropped to a circle, and stored in MPEG4 format.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class VideoNote extends Object {
        /**
         * Duration of the video, in seconds; as defined by the sender.
         */
        public int duration;
        /**
         * Video width and height; as defined by the sender.
         */
        public int length;
        /**
         * Video minithumbnail; may be null.
         */
        public Minithumbnail minithumbnail;
        /**
         * Video thumbnail; as defined by the sender; may be null.
         */
        public PhotoSize thumbnail;
        /**
         * File containing the video.
         */
        public File video;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1080075672;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a voice note. The voice note must be encoded with the Opus codec, and stored inside an OGG container. Voice notes can have only a single audio channel.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class VoiceNote extends Object {
        /**
         * Duration of the voice note, in seconds; as defined by the sender.
         */
        public int duration;
        /**
         * A waveform representation of the voice note in 5-bit format.
         */
        public byte[] waveform;
        /**
         * MIME type of the file; as defined by the sender.
         */
        public String mimeType;
        /**
         * File containing the voice note.
         */
        public File voice;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2066012058;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes a web page preview.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class WebPage extends Object {
        /**
         * Original URL of the link.
         */
        public String url;
        /**
         * URL to display.
         */
        public String displayUrl;
        /**
         * Type of the web page. Can be: article, photo, audio, video, document, profile, app, or something else.
         */
        public String type;
        /**
         * Short name of the site (e.g., Google Docs, App Store).
         */
        public String siteName;
        /**
         * Title of the content.
         */
        public String title;
        /**
         * Description of the content.
         */
        public String description;
        /**
         * Image representing the content; may be null.
         */
        public Photo photo;
        /**
         * URL to show in the embedded preview.
         */
        public String embedUrl;
        /**
         * MIME type of the embedded preview, (e.g., text/html or video/mp4).
         */
        public String embedType;
        /**
         * Width of the embedded preview.
         */
        public int embedWidth;
        /**
         * Height of the embedded preview.
         */
        public int embedHeight;
        /**
         * Duration of the content, in seconds.
         */
        public int duration;
        /**
         * Author of the content.
         */
        public String author;
        /**
         * Preview of the content as an animation, if available; may be null.
         */
        public Animation animation;
        /**
         * Preview of the content as an audio file, if available; may be null.
         */
        public Audio audio;
        /**
         * Preview of the content as a document, if available (currently only available for small PDF files and ZIP archives); may be null.
         */
        public Document document;
        /**
         * Preview of the content as a sticker for small WEBP files, if available; may be null.
         */
        public Sticker sticker;
        /**
         * Preview of the content as a video, if available; may be null.
         */
        public Video video;
        /**
         * Preview of the content as a video note, if available; may be null.
         */
        public VideoNote videoNote;
        /**
         * Preview of the content as a voice note, if available; may be null.
         */
        public VoiceNote voiceNote;
        /**
         * Version of instant view, available for the web page (currently can be 1 or 2), 0 if none.
         */
        public int instantViewVersion;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1092898169;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Describes an instant view page for a web page.
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class WebPageInstantView extends Object {
        /**
         * Content of the web page.
         */
        public PageBlock[] pageBlocks;
        /**
         * Version of the instant view, currently can be 1 or 2.
         */
        public int version;
        /**
         * Instant view URL; may be different from WebPage.url and must be used for the correct anchors handling.
         */
        public String url;
        /**
         * True, if the instant view must be shown from right to left.
         */
        public boolean isRtl;
        /**
         * True, if the instant view contains the full page. A network request might be needed to get the full web page instant view.
         */
        public boolean isFull;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 957287214;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Accepts an incoming call.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AcceptCall extends Function {
        /**
         * Call identifier.
         */
        public int callId;
        /**
         * Description of the call protocols supported by the client.
         */
        public CallProtocol protocol;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -646618416;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Accepts Telegram terms of services.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AcceptTermsOfService extends Function {
        /**
         * Terms of service identifier.
         */
        public String termsOfServiceId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2130576356;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a new member to a chat. Members can't be added to private or secret chats. Members will not be added until the chat state has been synchronized with the server.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddChatMember extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier of the user.
         */
        public int userId;
        /**
         * The number of earlier messages from the chat to be forwarded to the new member; up to 100. Ignored for supergroups and channels.
         */
        public int forwardLimit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1182817962;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds multiple new members to a chat. Currently this option is only available for supergroups and channels. This option can't be used to join a chat. Members can't be added to a channel if it has more than 200 members. Members will not be added until the chat state has been synchronized with the server.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddChatMembers extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifiers of the users to be added to the chat.
         */
        public int[] userIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1234094617;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a custom server language pack to the list of installed language packs in current localization target. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddCustomServerLanguagePack extends Function {
        /**
         * Identifier of a language pack to be added; may be different from a name that is used in an &quot;https://t.me/setlanguage/&quot; link.
         */
        public String languagePackId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 4492771;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a new sticker to the list of favorite stickers. The new sticker is added to the top of the list. If the sticker was already in the list, it is removed from the list first. Only stickers belonging to a sticker set can be added to this list.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddFavoriteSticker extends Function {
        /**
         * Sticker file to add.
         */
        public InputFile sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 324504799;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a local message to a chat. The message is persistent across application restarts only if the message database is used. Returns the added message.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddLocalMessage extends Function {
        /**
         * Target chat.
         */
        public long chatId;
        /**
         * Identifier of the user who will be shown as the sender of the message; may be 0 for channel posts.
         */
        public int senderUserId;
        /**
         * Identifier of the message to reply to or 0.
         */
        public long replyToMessageId;
        /**
         * Pass true to disable notification for the message.
         */
        public boolean disableNotification;
        /**
         * The content of the message to be added.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -348943149;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a message to TDLib internal log. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddLogMessage extends Function {
        /**
         * Minimum verbosity level needed for the message to be logged, 0-1023.
         */
        public int verbosityLevel;
        /**
         * Text of a message to log.
         */
        public String text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1597427692;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds the specified data to data usage statistics. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddNetworkStatistics extends Function {
        /**
         * The network statistics entry with the data to be added to statistics.
         */
        public NetworkStatisticsEntry entry;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1264825305;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a proxy server for network requests. Can be called before authorization.
     *
     * <p> Returns {@link Proxy Proxy} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddProxy extends Function {
        /**
         * Proxy server IP address.
         */
        public String server;
        /**
         * Proxy server port.
         */
        public int port;
        /**
         * True, if the proxy should be enabled.
         */
        public boolean enable;
        /**
         * Proxy type.
         */
        public ProxyType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 331529432;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Manually adds a new sticker to the list of recently used stickers. The new sticker is added to the top of the list. If the sticker was already in the list, it is removed from the list first. Only stickers belonging to a sticker set can be added to this list.
     *
     * <p> Returns {@link Stickers Stickers} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddRecentSticker extends Function {
        /**
         * Pass true to add the sticker to the list of stickers recently attached to photo or video files; pass false to add the sticker to the list of recently sent stickers.
         */
        public boolean isAttached;
        /**
         * Sticker file to add.
         */
        public InputFile sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1478109026;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a chat to the list of recently found chats. The chat is added to the beginning of the list. If the chat is already in the list, it will be removed from the list first.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddRecentlyFoundChat extends Function {
        /**
         * Identifier of the chat to add.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1746396787;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Manually adds a new animation to the list of saved animations. The new animation is added to the beginning of the list. If the animation was already in the list, it is removed first. Only non-secret video animations with MIME type &quot;video/mp4&quot; can be added to the list.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddSavedAnimation extends Function {
        /**
         * The animation file to be added. Only animations known to the server (i.e. successfully sent via a message) can be added to the list.
         */
        public InputFile animation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1538525088;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a new sticker to a set; for bots only. Returns the sticker set.
     *
     * <p> Returns {@link StickerSet StickerSet} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AddStickerToSet extends Function {
        /**
         * Sticker set owner.
         */
        public int userId;
        /**
         * Sticker set name.
         */
        public String name;
        /**
         * Sticker to add to the set.
         */
        public InputSticker sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1422402800;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the result of a callback query; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AnswerCallbackQuery extends Function {
        /**
         * Identifier of the callback query.
         */
        public long callbackQueryId;
        /**
         * Text of the answer.
         */
        public String text;
        /**
         * If true, an alert should be shown to the user instead of a toast notification.
         */
        public boolean showAlert;
        /**
         * URL to be opened.
         */
        public String url;
        /**
         * Time during which the result of the query can be cached, in seconds.
         */
        public int cacheTime;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1153028490;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Answers a custom query; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AnswerCustomQuery extends Function {
        /**
         * Identifier of a custom query.
         */
        public long customQueryId;
        /**
         * JSON-serialized answer to the query.
         */
        public String data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1293603521;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the result of an inline query; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AnswerInlineQuery extends Function {
        /**
         * Identifier of the inline query.
         */
        public long inlineQueryId;
        /**
         * True, if the result of the query can be cached for the specified user.
         */
        public boolean isPersonal;
        /**
         * The results of the query.
         */
        public InputInlineQueryResult[] results;
        /**
         * Allowed time to cache the results of the query, in seconds.
         */
        public int cacheTime;
        /**
         * Offset for the next inline query; pass an empty string if there are no more results.
         */
        public String nextOffset;
        /**
         * If non-empty, this text should be shown on the button that opens a private chat with the bot and sends a start message to the bot with the parameter switchPmParameter.
         */
        public String switchPmText;
        /**
         * The parameter for the bot start message.
         */
        public String switchPmParameter;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 485879477;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the result of a pre-checkout query; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AnswerPreCheckoutQuery extends Function {
        /**
         * Identifier of the pre-checkout query.
         */
        public long preCheckoutQueryId;
        /**
         * An error message, empty on success.
         */
        public String errorMessage;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1486789653;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the result of a shipping query; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AnswerShippingQuery extends Function {
        /**
         * Identifier of the shipping query.
         */
        public long shippingQueryId;
        /**
         * Available shipping options.
         */
        public ShippingOption[] shippingOptions;
        /**
         * An error message, empty on success.
         */
        public String errorMessage;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -434601324;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds a user to the blacklist.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BlockUser extends Function {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1239315139;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Stops the downloading of a file. If a file has already been downloaded, does nothing.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CancelDownloadFile extends Function {
        /**
         * Identifier of a file to stop downloading.
         */
        public int fileId;
        /**
         * Pass true to stop downloading only if it hasn't been started, i.e. request hasn't been sent to server.
         */
        public boolean onlyIfPending;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1954524450;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Stops the uploading of a file. Supported only for files uploaded by using uploadFile. For other files the behavior is undefined.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CancelUploadFile extends Function {
        /**
         * Identifier of the file to stop uploading.
         */
        public int fileId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1623539600;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Reports to the server whether a chat is a spam chat or not. Can be used only if ChatReportSpamState.canReportSpam is true. After this request, ChatReportSpamState.canReportSpam becomes false forever.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChangeChatReportSpamState extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * If true, the chat will be reported as spam; otherwise it will be marked as not spam.
         */
        public boolean isSpamChat;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1768597097;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes imported contacts using the list of current user contacts saved on the device. Imports newly added contacts and, if at least the file database is enabled, deletes recently deleted contacts. Query result depends on the result of the previous query, so only one query is possible at the same time.
     *
     * <p> Returns {@link ImportedContacts ImportedContacts} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChangeImportedContacts extends Function {
        /**
         * The new list of contacts, contact's vCard are ignored and are not imported.
         */
        public Contact[] contacts;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1968207955;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the phone number of the user and sends an authentication code to the user's new phone number. On success, returns information about the sent code.
     *
     * <p> Returns {@link AuthenticationCodeInfo AuthenticationCodeInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChangePhoneNumber extends Function {
        /**
         * The new phone number of the user in international format.
         */
        public String phoneNumber;
        /**
         * Settings for the authentication of the user's phone number.
         */
        public PhoneNumberAuthenticationSettings settings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -124666973;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Installs/uninstalls or activates/archives a sticker set.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ChangeStickerSet extends Function {
        /**
         * Identifier of the sticker set.
         */
        public long setId;
        /**
         * The new value of isInstalled.
         */
        public boolean isInstalled;
        /**
         * The new value of isArchived. A sticker set can't be installed and archived simultaneously.
         */
        public boolean isArchived;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 449357293;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the authentication token of a bot; to log in as a bot. Works only when the current authorization state is authorizationStateWaitPhoneNumber. Can be used instead of setAuthenticationPhoneNumber and checkAuthenticationCode to log in.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckAuthenticationBotToken extends Function {
        /**
         * The bot token.
         */
        public String token;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 639321206;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the authentication code. Works only when the current authorization state is authorizationStateWaitCode.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckAuthenticationCode extends Function {
        /**
         * The verification code received via SMS, Telegram message, phone call, or flash call.
         */
        public String code;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -302103382;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the authentication password for correctness. Works only when the current authorization state is authorizationStateWaitPassword.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckAuthenticationPassword extends Function {
        /**
         * The password to check.
         */
        public String password;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2025698400;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the authentication code sent to confirm a new phone number of the user.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckChangePhoneNumberCode extends Function {
        /**
         * Verification code received by SMS, phone call or flash call.
         */
        public String code;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1720278429;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the validity of an invite link for a chat and returns information about the corresponding chat.
     *
     * <p> Returns {@link ChatInviteLinkInfo ChatInviteLinkInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckChatInviteLink extends Function {
        /**
         * Invite link to be checked; should begin with &quot;https://t.me/joinchat/&quot;, &quot;https://telegram.me/joinchat/&quot;, or &quot;https://telegram.dog/joinchat/&quot;.
         */
        public String inviteLink;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -496940997;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks whether a username can be set for a chat.
     *
     * <p> Returns {@link CheckChatUsernameResult CheckChatUsernameResult} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckChatUsername extends Function {
        /**
         * Chat identifier; should be identifier of a supergroup chat, or a channel chat, or a private chat with self, or zero if chat is being created.
         */
        public long chatId;
        /**
         * Username to be checked.
         */
        public String username;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -119119344;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the database encryption key for correctness. Works only when the current authorization state is authorizationStateWaitEncryptionKey.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckDatabaseEncryptionKey extends Function {
        /**
         * Encryption key to check or set up.
         */
        public byte[] encryptionKey;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1018769307;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the email address verification code for Telegram Passport.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckEmailAddressVerificationCode extends Function {
        /**
         * Verification code.
         */
        public String code;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -426386685;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks phone number confirmation code.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckPhoneNumberConfirmationCode extends Function {
        /**
         * The phone number confirmation code.
         */
        public String code;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1348060966;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the phone number verification code for Telegram Passport.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckPhoneNumberVerificationCode extends Function {
        /**
         * Verification code.
         */
        public String code;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1497462718;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Checks the 2-step verification recovery email address verification code.
     *
     * <p> Returns {@link PasswordState PasswordState} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckRecoveryEmailAddressCode extends Function {
        /**
         * Verification code.
         */
        public String code;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1997039589;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes potentially dangerous characters from the name of a file. The encoding of the file name is supposed to be UTF-8. Returns an empty string on failure. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Text Text} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CleanFileName extends Function {
        /**
         * File name or path to the file.
         */
        public String fileName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 967964667;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Clears draft messages in all chats.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ClearAllDraftMessages extends Function {
        /**
         * If true, local draft messages in secret chats will not be cleared.
         */
        public boolean excludeSecretChats;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -46369573;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Clears all imported contacts, contact list remains unchanged.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class ClearImportedContacts extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 869503298;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Clears the list of recently used stickers.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ClearRecentStickers extends Function {
        /**
         * Pass true to clear the list of stickers recently attached to photo or video files; pass false to clear the list of recently sent stickers.
         */
        public boolean isAttached;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -321242684;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Clears the list of recently found chats.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class ClearRecentlyFoundChats extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -285582542;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Closes the TDLib instance. All databases will be flushed to disk and properly closed. After the close completes, updateAuthorizationState with authorizationStateClosed will be sent.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class Close extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1187782273;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Informs TDLib that the chat is closed by the user. Many useful activities depend on the chat being opened or closed.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CloseChat extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 39749353;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Closes a secret chat, effectively transfering its state to secretChatStateClosed.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CloseSecretChat extends Function {
        /**
         * Secret chat identifier.
         */
        public int secretChatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -471006133;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an existing chat corresponding to a known basic group.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateBasicGroupChat extends Function {
        /**
         * Basic group identifier.
         */
        public int basicGroupId;
        /**
         * If true, the chat will be created without network request. In this case all information about the chat except its type, title and photo can be incorrect.
         */
        public boolean force;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 642492777;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Creates a new call.
     *
     * <p> Returns {@link CallId CallId} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateCall extends Function {
        /**
         * Identifier of the user to be called.
         */
        public int userId;
        /**
         * Description of the call protocols supported by the client.
         */
        public CallProtocol protocol;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1742408159;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Creates a new basic group and sends a corresponding messageBasicGroupChatCreate. Returns the newly created chat.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateNewBasicGroupChat extends Function {
        /**
         * Identifiers of users to be added to the basic group.
         */
        public int[] userIds;
        /**
         * Title of the new basic group; 1-128 characters.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1874532069;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Creates a new secret chat. Returns the newly created chat.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateNewSecretChat extends Function {
        /**
         * Identifier of the target user.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1689344881;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Creates a new sticker set; for bots only. Returns the newly created sticker set.
     *
     * <p> Returns {@link StickerSet StickerSet} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateNewStickerSet extends Function {
        /**
         * Sticker set owner.
         */
        public int userId;
        /**
         * Sticker set title; 1-64 characters.
         */
        public String title;
        /**
         * Sticker set name. Can contain only English letters, digits and underscores. Must end with *&quot;_by_&lt;bot username&gt;&quot;* (*&lt;botUsername&gt;* is case insensitive); 1-64 characters.
         */
        public String name;
        /**
         * True, if stickers are masks.
         */
        public boolean isMasks;
        /**
         * List of stickers to be added to the set.
         */
        public InputSticker[] stickers;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 205093058;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Creates a new supergroup or channel and sends a corresponding messageSupergroupChatCreate. Returns the newly created chat.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateNewSupergroupChat extends Function {
        /**
         * Title of the new chat; 1-128 characters.
         */
        public String title;
        /**
         * True, if a channel chat should be created.
         */
        public boolean isChannel;
        /**
         * Chat description; 0-255 characters.
         */
        public String description;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1284982268;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an existing chat corresponding to a given user.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreatePrivateChat extends Function {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * If true, the chat will be created without network request. In this case all information about the chat except its type, title and photo can be incorrect.
         */
        public boolean force;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1807530364;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an existing chat corresponding to a known secret chat.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateSecretChat extends Function {
        /**
         * Secret chat identifier.
         */
        public int secretChatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1930285615;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an existing chat corresponding to a known supergroup or channel.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateSupergroupChat extends Function {
        /**
         * Supergroup or channel identifier.
         */
        public int supergroupId;
        /**
         * If true, the chat will be created without network request. In this case all information about the chat except its type, title and photo can be incorrect.
         */
        public boolean force;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 352742758;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Creates a new temporary password for processing payments.
     *
     * <p> Returns {@link TemporaryPasswordState TemporaryPasswordState} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CreateTemporaryPassword extends Function {
        /**
         * Persistent user password.
         */
        public String password;
        /**
         * Time during which the temporary password will be valid, in seconds; should be between 60 and 86400.
         */
        public int validFor;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1626509434;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes the account of the current user, deleting all information associated with the user from the server. The phone number of the account can be used to create a new account. Can be called before authorization when the current authorization state is authorizationStateWaitPassword.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteAccount extends Function {
        /**
         * The reason why the account was deleted; optional.
         */
        public String reason;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1203056508;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes all messages in the chat. Use Chat.canBeDeletedOnlyForSelf and Chat.canBeDeletedForAllUsers fields to find whether and how the method can be applied to the chat.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteChatHistory extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Pass true if the chat should be removed from the chat list.
         */
        public boolean removeFromChatList;
        /**
         * Pass true to try to delete chat history for all users.
         */
        public boolean revoke;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1472081761;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes all messages sent by the specified user to a chat. Supported only in supergroups; requires canDeleteMessages administrator privileges.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteChatMessagesFromUser extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1599689199;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes the default reply markup from a chat. Must be called after a one-time keyboard or a ForceReply reply markup has been used. UpdateChatReplyMarkup will be sent if the reply markup will be changed.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteChatReplyMarkup extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The message identifier of the used keyboard.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 100637531;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes a file from the TDLib file cache.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteFile extends Function {
        /**
         * Identifier of the file to delete.
         */
        public int fileId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1807653676;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes all information about a language pack in the current localization target. The language pack which is currently in use (including base language pack) or is being synchronized can't be deleted. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteLanguagePack extends Function {
        /**
         * Identifier of the language pack to delete.
         */
        public String languagePackId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2108761026;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes messages.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteMessages extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifiers of the messages to be deleted.
         */
        public long[] messageIds;
        /**
         * Pass true to try to delete messages for all chat members. Always true for supergroups, channels and secret chats.
         */
        public boolean revoke;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1130090173;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes a Telegram Passport element.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeletePassportElement extends Function {
        /**
         * Element type.
         */
        public PassportElementType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1719555468;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes a profile photo. If something changes, updateUser will be sent.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteProfilePhoto extends Function {
        /**
         * Identifier of the profile photo to delete.
         */
        public long profilePhotoId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1319794625;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes saved credentials for all payment provider bots.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class DeleteSavedCredentials extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 826300114;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes saved order info.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class DeleteSavedOrderInfo extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1629058164;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Deletes a supergroup or channel along with all messages in the corresponding chat. This will release the supergroup or channel username and remove all members; requires creator privileges in the supergroup or channel. Chats with more than 1000 members can't be deleted using this method.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DeleteSupergroup extends Function {
        /**
         * Identifier of the supergroup or channel.
         */
        public int supergroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1999855965;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Closes the TDLib instance, destroying all local data without a proper logout. The current user session will remain in the list of all active sessions. All local data will be destroyed. After the destruction completes updateAuthorizationState with authorizationStateClosed will be sent.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class Destroy extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 685331274;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Disables the currently enabled proxy. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class DisableProxy extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2100095102;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Discards a call.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DiscardCall extends Function {
        /**
         * Call identifier.
         */
        public int callId;
        /**
         * True, if the user was disconnected.
         */
        public boolean isDisconnected;
        /**
         * The call duration, in seconds.
         */
        public int duration;
        /**
         * Identifier of the connection used during the call.
         */
        public long connectionId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -923187372;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Disconnects all websites from the current user's Telegram account.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class DisconnectAllWebsites extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1082985981;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Disconnects website from the current user's Telegram account.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DisconnectWebsite extends Function {
        /**
         * Website identifier.
         */
        public long websiteId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -778767395;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Downloads a file from the cloud. Download progress and completion of the download will be notified through updateFile updates.
     *
     * <p> Returns {@link File File} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DownloadFile extends Function {
        /**
         * Identifier of the file to download.
         */
        public int fileId;
        /**
         * Priority of the download (1-32). The higher the priority, the earlier the file will be downloaded. If the priorities of two files are equal, then the last one for which downloadFile was called will be downloaded first.
         */
        public int priority;
        /**
         * The starting position from which the file should be downloaded.
         */
        public int offset;
        /**
         * Number of bytes which should be downloaded starting from the &quot;offset&quot; position before the download will be automatically cancelled; use 0 to download without a limit.
         */
        public int limit;
        /**
         * If false, this request returns file state just after the download has been started. If true, this request returns file state only after the download has succeeded, has failed, has been cancelled or a new downloadFile request with different offset/limit parameters was sent.
         */
        public boolean synchronous;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1102026662;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits information about a custom local language pack in the current localization target. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditCustomLanguagePackInfo extends Function {
        /**
         * New information about the custom local language pack.
         */
        public LanguagePackInfo info;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1320751257;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the caption of an inline message sent via a bot; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditInlineMessageCaption extends Function {
        /**
         * Inline message identifier.
         */
        public String inlineMessageId;
        /**
         * The new message reply markup.
         */
        public ReplyMarkup replyMarkup;
        /**
         * New message content caption; 0-GetOption(&quot;message_caption_length_max&quot;) characters.
         */
        public FormattedText caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -760985929;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the content of a live location in an inline message sent via a bot; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditInlineMessageLiveLocation extends Function {
        /**
         * Inline message identifier.
         */
        public String inlineMessageId;
        /**
         * The new message reply markup.
         */
        public ReplyMarkup replyMarkup;
        /**
         * New location content of the message; may be null. Pass null to stop sharing the live location.
         */
        public Location location;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 655046316;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the content of a message with an animation, an audio, a document, a photo or a video in an inline message sent via a bot; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditInlineMessageMedia extends Function {
        /**
         * Inline message identifier.
         */
        public String inlineMessageId;
        /**
         * The new message reply markup; for bots only.
         */
        public ReplyMarkup replyMarkup;
        /**
         * New content of the message. Must be one of the following types: InputMessageAnimation, InputMessageAudio, InputMessageDocument, InputMessagePhoto or InputMessageVideo.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 23553921;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the reply markup of an inline message sent via a bot; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditInlineMessageReplyMarkup extends Function {
        /**
         * Inline message identifier.
         */
        public String inlineMessageId;
        /**
         * The new message reply markup.
         */
        public ReplyMarkup replyMarkup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -67565858;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the text of an inline text or game message sent via a bot; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditInlineMessageText extends Function {
        /**
         * Inline message identifier.
         */
        public String inlineMessageId;
        /**
         * The new message reply markup.
         */
        public ReplyMarkup replyMarkup;
        /**
         * New text content of the message. Should be of type InputMessageText.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -855457307;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the message content caption. Returns the edited message after the edit is completed on the server side.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditMessageCaption extends Function {
        /**
         * The chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * The new message reply markup; for bots only.
         */
        public ReplyMarkup replyMarkup;
        /**
         * New message content caption; 0-GetOption(&quot;message_caption_length_max&quot;) characters.
         */
        public FormattedText caption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1154677038;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the message content of a live location. Messages can be edited for a limited period of time specified in the live location. Returns the edited message after the edit is completed on the server side.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditMessageLiveLocation extends Function {
        /**
         * The chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * The new message reply markup; for bots only.
         */
        public ReplyMarkup replyMarkup;
        /**
         * New location content of the message; may be null. Pass null to stop sharing the live location.
         */
        public Location location;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1146772745;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the content of a message with an animation, an audio, a document, a photo or a video. The media in the message can't be replaced if the message was set to self-destruct. Media can't be replaced by self-destructing media. Media in an album can be edited only to contain a photo or a video. Returns the edited message after the edit is completed on the server side.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditMessageMedia extends Function {
        /**
         * The chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * The new message reply markup; for bots only.
         */
        public ReplyMarkup replyMarkup;
        /**
         * New content of the message. Must be one of the following types: InputMessageAnimation, InputMessageAudio, InputMessageDocument, InputMessagePhoto or InputMessageVideo.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1152678125;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the message reply markup; for bots only. Returns the edited message after the edit is completed on the server side.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditMessageReplyMarkup extends Function {
        /**
         * The chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * The new message reply markup.
         */
        public ReplyMarkup replyMarkup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 332127881;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits the text of a message (or a text of a game message). Returns the edited message after the edit is completed on the server side.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditMessageText extends Function {
        /**
         * The chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * The new message reply markup; for bots only.
         */
        public ReplyMarkup replyMarkup;
        /**
         * New text content of the message. Should be of type InputMessageText.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 196272567;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Edits an existing proxy server for network requests. Can be called before authorization.
     *
     * <p> Returns {@link Proxy Proxy} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EditProxy extends Function {
        /**
         * Proxy identifier.
         */
        public int proxyId;
        /**
         * Proxy server IP address.
         */
        public String server;
        /**
         * Proxy server port.
         */
        public int port;
        /**
         * True, if the proxy should be enabled.
         */
        public boolean enable;
        /**
         * Proxy type.
         */
        public ProxyType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1605883821;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Enables a proxy. Only one proxy can be enabled at a time. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class EnableProxy extends Function {
        /**
         * Proxy identifier.
         */
        public int proxyId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1494450838;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Finishes the file generation.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FinishFileGeneration extends Function {
        /**
         * The identifier of the generation process.
         */
        public long generationId;
        /**
         * If set, means that file generation has failed and should be terminated.
         */
        public Error error;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1055060835;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Forwards previously sent messages. Returns the forwarded messages in the same order as the message identifiers passed in messageIds. If a message can't be forwarded, null will be returned instead of the message.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ForwardMessages extends Function {
        /**
         * Identifier of the chat to which to forward messages.
         */
        public long chatId;
        /**
         * Identifier of the chat from which to forward messages.
         */
        public long fromChatId;
        /**
         * Identifiers of the messages to forward.
         */
        public long[] messageIds;
        /**
         * Pass true to disable notification for the message, doesn't work if messages are forwarded to a secret chat.
         */
        public boolean disableNotification;
        /**
         * Pass true if the messages are sent from the background.
         */
        public boolean fromBackground;
        /**
         * True, if the messages should be grouped into an album after forwarding. For this to work, no more than 10 messages may be forwarded, and all of them must be photo or video messages.
         */
        public boolean asAlbum;
        /**
         * True, if content of the messages needs to be copied without links to the original messages. Always true if the messages are forwarded to a secret chat.
         */
        public boolean sendCopy;
        /**
         * True, if media captions of message copies needs to be removed. Ignored if sendCopy is false.
         */
        public boolean removeCaption;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1948637111;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Generates a new invite link for a chat; the previously generated link is revoked. Available for basic groups, supergroups, and channels. Requires administrator privileges and canInviteUsers right.
     *
     * <p> Returns {@link ChatInviteLink ChatInviteLink} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GenerateChatInviteLink extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1945532500;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the period of inactivity after which the account of the current user will automatically be deleted.
     *
     * <p> Returns {@link AccountTtl AccountTtl} </p>
     */
    public static class GetAccountTtl extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -443905161;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns all active live locations that should be updated by the client. The list is persistent across application restarts only if the message database is used.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    public static class GetActiveLiveLocationMessages extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1425459567;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns all active sessions of the current user.
     *
     * <p> Returns {@link Sessions Sessions} </p>
     */
    public static class GetActiveSessions extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1119710526;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns all available Telegram Passport elements.
     *
     * <p> Returns {@link PassportElements PassportElements} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetAllPassportElements extends Function {
        /**
         * Password of the current user.
         */
        public String password;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2038945045;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns application config, provided by the server. Can be called before authorization.
     *
     * <p> Returns {@link JsonValue JsonValue} </p>
     */
    public static class GetApplicationConfig extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1823144318;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of archived sticker sets.
     *
     * <p> Returns {@link StickerSets StickerSets} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetArchivedStickerSets extends Function {
        /**
         * Pass true to return mask stickers sets; pass false to return ordinary sticker sets.
         */
        public boolean isMasks;
        /**
         * Identifier of the sticker set from which to return the result.
         */
        public long offsetStickerSetId;
        /**
         * Maximum number of sticker sets to return.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1996943238;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of sticker sets attached to a file. Currently only photos and videos can have attached sticker sets.
     *
     * <p> Returns {@link StickerSets StickerSets} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetAttachedStickerSets extends Function {
        /**
         * File identifier.
         */
        public int fileId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1302172429;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the current authorization state; this is an offline request. For informational purposes only. Use updateAuthorizationState instead to maintain the current authorization state.
     *
     * <p> Returns {@link AuthorizationState AuthorizationState} </p>
     */
    public static class GetAuthorizationState extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1949154877;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns auto-download settings presets for the currently logged in user.
     *
     * <p> Returns {@link AutoDownloadSettingsPresets AutoDownloadSettingsPresets} </p>
     */
    public static class GetAutoDownloadSettingsPresets extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1721088201;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Constructs a persistent HTTP URL for a background.
     *
     * <p> Returns {@link HttpUrl HttpUrl} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetBackgroundUrl extends Function {
        /**
         * Background name.
         */
        public String name;
        /**
         * Background type.
         */
        public BackgroundType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 733769682;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns backgrounds installed by the user.
     *
     * <p> Returns {@link Backgrounds Backgrounds} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetBackgrounds extends Function {
        /**
         * True, if the backgrounds needs to be ordered for dark theme.
         */
        public boolean forDarkTheme;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 249072633;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a basic group by its identifier. This is an offline request if the current user is not a bot.
     *
     * <p> Returns {@link BasicGroup BasicGroup} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetBasicGroup extends Function {
        /**
         * Basic group identifier.
         */
        public int basicGroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 561775568;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns full information about a basic group by its identifier.
     *
     * <p> Returns {@link BasicGroupFullInfo BasicGroupFullInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetBasicGroupFullInfo extends Function {
        /**
         * Basic group identifier.
         */
        public int basicGroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1770517905;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns users that were blocked by the current user.
     *
     * <p> Returns {@link Users Users} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetBlockedUsers extends Function {
        /**
         * Number of users to skip in the result; must be non-negative.
         */
        public int offset;
        /**
         * Maximum number of users to return; up to 100.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -742912777;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a callback query to a bot and returns an answer. Returns an error with code 502 if the bot fails to answer the query before the query timeout expires.
     *
     * <p> Returns {@link CallbackQueryAnswer CallbackQueryAnswer} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetCallbackQueryAnswer extends Function {
        /**
         * Identifier of the chat with the message.
         */
        public long chatId;
        /**
         * Identifier of the message from which the query originated.
         */
        public long messageId;
        /**
         * Query payload.
         */
        public CallbackQueryPayload payload;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 116357727;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a chat by its identifier, this is an offline request if the current user is not a bot.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChat extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1866601536;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of users who are administrators of the chat.
     *
     * <p> Returns {@link Users Users} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatAdministrators extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 508231041;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of service actions taken by chat members and administrators in the last 48 hours. Available only in supergroups and channels. Requires administrator rights. Returns results in reverse chronological order (i. e., in order of decreasing eventId).
     *
     * <p> Returns {@link ChatEvents ChatEvents} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatEventLog extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Search query by which to filter events.
         */
        public String query;
        /**
         * Identifier of an event from which to return results. Use 0 to get results from the latest events.
         */
        public long fromEventId;
        /**
         * Maximum number of events to return; up to 100.
         */
        public int limit;
        /**
         * The types of events to return. By default, all types will be returned.
         */
        public ChatEventLogFilters filters;
        /**
         * User identifiers by which to filter events. By default, events relating to all users will be returned.
         */
        public int[] userIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 206900967;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns messages in a chat. The messages are returned in a reverse chronological order (i.e., in order of decreasing messageId). For optimal performance the number of returned messages is chosen by the library. This is an offline request if onlyLocal is true.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatHistory extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier of the message starting from which history must be fetched; use 0 to get results from the last message.
         */
        public long fromMessageId;
        /**
         * Specify 0 to get results from exactly the fromMessageId or a negative offset up to 99 to get additionally some newer messages.
         */
        public int offset;
        /**
         * The maximum number of messages to be returned; must be positive and can't be greater than 100. If the offset is negative, the limit must be greater or equal to -offset. Fewer messages may be returned than specified by the limit, even if the end of the message history has not been reached.
         */
        public int limit;
        /**
         * If true, returns only messages that are available locally without sending network requests.
         */
        public boolean onlyLocal;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -799960451;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a single member of a chat.
     *
     * <p> Returns {@link ChatMember ChatMember} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatMember extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 677085892;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the last message sent in a chat no later than the specified date.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatMessageByDate extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Point in time (Unix timestamp) relative to which to search for messages.
         */
        public int date;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1062564150;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns approximate number of messages of the specified type in the chat.
     *
     * <p> Returns {@link Count Count} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatMessageCount extends Function {
        /**
         * Identifier of the chat in which to count messages.
         */
        public long chatId;
        /**
         * Filter for message content; searchMessagesFilterEmpty is unsupported in this function.
         */
        public SearchMessagesFilter filter;
        /**
         * If true, returns count that is available locally without sending network requests, returning -1 if the number of messages is unknown.
         */
        public boolean returnLocal;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 205435308;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns list of chats with non-default notification settings.
     *
     * <p> Returns {@link Chats Chats} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatNotificationSettingsExceptions extends Function {
        /**
         * If specified, only chats from the specified scope will be returned.
         */
        public NotificationSettingsScope scope;
        /**
         * If true, also chats with non-default sound will be returned.
         */
        public boolean compareSound;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 201199121;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a pinned chat message.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatPinnedMessage extends Function {
        /**
         * Identifier of the chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 359865008;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information on whether the current chat can be reported as spam.
     *
     * <p> Returns {@link ChatReportSpamState ChatReportSpamState} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatReportSpamState extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -748866856;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an HTTP URL with the chat statistics. Currently this method can be used only for channels.
     *
     * <p> Returns {@link HttpUrl HttpUrl} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChatStatisticsUrl extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Parameters from &quot;tg://statsrefresh?params=******&quot; link.
         */
        public String parameters;
        /**
         * Pass true if a URL with the dark theme must be returned.
         */
        public boolean isDark;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1114621183;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an ordered list of chats. Chats are sorted by the pair (order, chatId) in decreasing order. (For example, to get a list of chats from the beginning, the offsetOrder should be equal to a biggest signed 64-bit number 9223372036854775807 == 2^63 - 1). For optimal performance the number of returned chats is chosen by the library.
     *
     * <p> Returns {@link Chats Chats} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetChats extends Function {
        /**
         * Chat order to return chats from.
         */
        public long offsetOrder;
        /**
         * Chat identifier to return chats from.
         */
        public long offsetChatId;
        /**
         * The maximum number of chats to be returned. It is possible that fewer chats than the limit are returned even if the end of the list is not reached.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2121381601;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns all website where the current user used Telegram to log in.
     *
     * <p> Returns {@link ConnectedWebsites ConnectedWebsites} </p>
     */
    public static class GetConnectedWebsites extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -170536110;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns all user contacts.
     *
     * <p> Returns {@link Users Users} </p>
     */
    public static class GetContacts extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1417722768;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Uses current user IP to found their country. Returns two-letter ISO 3166-1 alpha-2 country code. Can be called before authorization.
     *
     * <p> Returns {@link Text Text} </p>
     */
    public static class GetCountryCode extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1540593906;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of public chats with username created by the user.
     *
     * <p> Returns {@link Chats Chats} </p>
     */
    public static class GetCreatedPublicChats extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1609082914;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns all updates needed to restore current TDLib state, i.e. all actual UpdateAuthorizationState/UpdateUser/UpdateNewChat and others. This is especially usefull if TDLib is run in a separate process. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link Updates Updates} </p>
     */
    public static class GetCurrentState extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1191417719;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns database statistics.
     *
     * <p> Returns {@link DatabaseStatistics DatabaseStatistics} </p>
     */
    public static class GetDatabaseStatistics extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1942760263;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a tg:// deep link. Use &quot;tg://need_update_for_some_feature&quot; or &quot;tg:someUnsupportedFeature&quot; for testing. Returns a 404 error for unknown links. Can be called before authorization.
     *
     * <p> Returns {@link DeepLinkInfo DeepLinkInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetDeepLinkInfo extends Function {
        /**
         * The link.
         */
        public String link;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 680673150;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an HTTP URL which can be used to automatically log in to the translation platform and suggest new emoji replacements. The URL will be valid for 30 seconds after generation.
     *
     * <p> Returns {@link HttpUrl HttpUrl} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetEmojiSuggestionsUrl extends Function {
        /**
         * Language code for which the emoji replacements will be suggested.
         */
        public String languageCode;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1404101841;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns favorite stickers.
     *
     * <p> Returns {@link Stickers Stickers} </p>
     */
    public static class GetFavoriteStickers extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -338964672;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a file; this is an offline request.
     *
     * <p> Returns {@link File File} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetFile extends Function {
        /**
         * Identifier of the file to get.
         */
        public int fileId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1553923406;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns file downloaded prefix size from a given offset.
     *
     * <p> Returns {@link Count Count} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetFileDownloadedPrefixSize extends Function {
        /**
         * Identifier of the file.
         */
        public int fileId;
        /**
         * Offset from which downloaded prefix size should be calculated.
         */
        public int offset;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1668864864;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the extension of a file, guessed by its MIME type. Returns an empty string on failure. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Text Text} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetFileExtension extends Function {
        /**
         * The MIME type of the file.
         */
        public String mimeType;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -106055372;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the MIME type of a file, guessed by its extension. Returns an empty string on failure. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Text Text} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetFileMimeType extends Function {
        /**
         * The name of the file or path to the file.
         */
        public String fileName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2073879671;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the high scores for a game and some part of the high score table in the range of the specified user; for bots only.
     *
     * <p> Returns {@link GameHighScores GameHighScores} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetGameHighScores extends Function {
        /**
         * The chat that contains the message with the game.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1920923753;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of common group chats with a given user. Chats are sorted by their type and creation date.
     *
     * <p> Returns {@link Chats Chats} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetGroupsInCommon extends Function {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Chat identifier starting from which to return chats; use 0 for the first request.
         */
        public long offsetChatId;
        /**
         * Maximum number of chats to be returned; up to 100.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -23238689;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the total number of imported contacts.
     *
     * <p> Returns {@link Count Count} </p>
     */
    public static class GetImportedContactCount extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -656336346;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns game high scores and some part of the high score table in the range of the specified user; for bots only.
     *
     * <p> Returns {@link GameHighScores GameHighScores} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetInlineGameHighScores extends Function {
        /**
         * Inline message identifier.
         */
        public String inlineMessageId;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1833445800;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends an inline query to a bot and returns its results. Returns an error with code 502 if the bot fails to answer the query before the query timeout expires.
     *
     * <p> Returns {@link InlineQueryResults InlineQueryResults} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetInlineQueryResults extends Function {
        /**
         * The identifier of the target bot.
         */
        public int botUserId;
        /**
         * Identifier of the chat, where the query was sent.
         */
        public long chatId;
        /**
         * Location of the user, only if needed.
         */
        public Location userLocation;
        /**
         * Text of the query.
         */
        public String query;
        /**
         * Offset of the first entry to return.
         */
        public String offset;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1182511172;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of installed sticker sets.
     *
     * <p> Returns {@link StickerSets StickerSets} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetInstalledStickerSets extends Function {
        /**
         * Pass true to return mask sticker sets; pass false to return ordinary sticker sets.
         */
        public boolean isMasks;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1214523749;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the default text for invitation messages to be used as a placeholder when the current user invites friends to Telegram.
     *
     * <p> Returns {@link Text Text} </p>
     */
    public static class GetInviteText extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 794573512;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Converts a JsonValue object to corresponding JSON-serialized string. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Text Text} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetJsonString extends Function {
        /**
         * The JsonValue object.
         */
        public JsonValue jsonValue;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 663458849;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Converts a JSON-serialized string to corresponding JsonValue object. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link JsonValue JsonValue} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetJsonValue extends Function {
        /**
         * The JSON-serialized string.
         */
        public String json;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1829086715;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a language pack. Returned language pack identifier may be different from a provided one. Can be called before authorization.
     *
     * <p> Returns {@link LanguagePackInfo LanguagePackInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetLanguagePackInfo extends Function {
        /**
         * Language pack identifier.
         */
        public String languagePackId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2077809320;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a string stored in the local database from the specified localization target and language pack by its key. Returns a 404 error if the string is not found. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link LanguagePackStringValue LanguagePackStringValue} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetLanguagePackString extends Function {
        /**
         * Path to the language pack database in which strings are stored.
         */
        public String languagePackDatabasePath;
        /**
         * Localization target to which the language pack belongs.
         */
        public String localizationTarget;
        /**
         * Language pack identifier.
         */
        public String languagePackId;
        /**
         * Language pack key of the string to be returned.
         */
        public String key;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 150789747;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns strings from a language pack in the current localization target by their keys. Can be called before authorization.
     *
     * <p> Returns {@link LanguagePackStrings LanguagePackStrings} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetLanguagePackStrings extends Function {
        /**
         * Language pack identifier of the strings to be returned.
         */
        public String languagePackId;
        /**
         * Language pack keys of the strings to be returned; leave empty to request all available strings.
         */
        public String[] keys;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1246259088;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about the current localization target. This is an offline request if onlyLocal is true. Can be called before authorization.
     *
     * <p> Returns {@link LocalizationTargetInfo LocalizationTargetInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetLocalizationTargetInfo extends Function {
        /**
         * If true, returns only locally available information without sending network requests.
         */
        public boolean onlyLocal;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1849499526;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about currently used log stream for internal logging of TDLib. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link LogStream LogStream} </p>
     */
    public static class GetLogStream extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1167608667;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns current verbosity level for a specified TDLib internal log tag. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link LogVerbosityLevel LogVerbosityLevel} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetLogTagVerbosityLevel extends Function {
        /**
         * Logging tag to change verbosity level.
         */
        public String tag;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 951004547;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns list of available TDLib internal log tags, for example, [&quot;actor&quot;, &quot;binlog&quot;, &quot;connections&quot;, &quot;notifications&quot;, &quot;proxy&quot;]. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link LogTags LogTags} </p>
     */
    public static class GetLogTags extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -254449190;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns current verbosity level of the internal logging of TDLib. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link LogVerbosityLevel LogVerbosityLevel} </p>
     */
    public static class GetLogVerbosityLevel extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 594057956;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a file with a map thumbnail in PNG format. Only map thumbnail files with size less than 1MB can be downloaded.
     *
     * <p> Returns {@link File File} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetMapThumbnailFile extends Function {
        /**
         * Location of the map center.
         */
        public Location location;
        /**
         * Map zoom level; 13-20.
         */
        public int zoom;
        /**
         * Map width in pixels before applying scale; 16-1024.
         */
        public int width;
        /**
         * Map height in pixels before applying scale; 16-1024.
         */
        public int height;
        /**
         * Map scale; 1-3.
         */
        public int scale;
        /**
         * Identifier of a chat, in which the thumbnail will be shown. Use 0 if unknown.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -152660070;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the current user.
     *
     * <p> Returns {@link User User} </p>
     */
    public static class GetMe extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -191516033;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a message.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetMessage extends Function {
        /**
         * Identifier of the chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier of the message to get.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1821196160;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a private HTTPS link to a message in a chat. Available only for already sent messages in supergroups and channels. The link will work only for members of the chat.
     *
     * <p> Returns {@link HttpUrl HttpUrl} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetMessageLink extends Function {
        /**
         * Identifier of the chat to which the message belongs.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1362732326;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a public or private message link.
     *
     * <p> Returns {@link MessageLinkInfo MessageLinkInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetMessageLinkInfo extends Function {
        /**
         * The message link in the format &quot;https://t.me/c/...&quot;, or &quot;tg://privatepost?...&quot;, or &quot;https://t.me/username/...&quot;, or &quot;tg://resolve?...&quot;.
         */
        public String url;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -700533672;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a message, if it is available locally without sending network request. This is an offline request.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetMessageLocally extends Function {
        /**
         * Identifier of the chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier of the message to get.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -603575444;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about messages. If a message is not found, returns null on the corresponding position of the result.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetMessages extends Function {
        /**
         * Identifier of the chat the messages belong to.
         */
        public long chatId;
        /**
         * Identifiers of the messages to get.
         */
        public long[] messageIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 425299338;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns network data usage statistics. Can be called before authorization.
     *
     * <p> Returns {@link NetworkStatistics NetworkStatistics} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetNetworkStatistics extends Function {
        /**
         * If true, returns only data for the current library launch.
         */
        public boolean onlyCurrent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -986228706;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the value of an option by its name. (Check the list of available options on https://core.telegram.org/tdlib/options.) Can be called before authorization.
     *
     * <p> Returns {@link OptionValue OptionValue} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetOption extends Function {
        /**
         * The name of the option.
         */
        public String name;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1572495746;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a Telegram Passport authorization form for sharing data with a service.
     *
     * <p> Returns {@link PassportAuthorizationForm PassportAuthorizationForm} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetPassportAuthorizationForm extends Function {
        /**
         * User identifier of the service's bot.
         */
        public int botUserId;
        /**
         * Telegram Passport element types requested by the service.
         */
        public String scope;
        /**
         * Service's publicKey.
         */
        public String publicKey;
        /**
         * Authorization form nonce provided by the service.
         */
        public String nonce;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1468394095;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns already available Telegram Passport elements suitable for completing a Telegram Passport authorization form. Result can be received only once for each authorization form.
     *
     * <p> Returns {@link PassportElementsWithErrors PassportElementsWithErrors} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetPassportAuthorizationFormAvailableElements extends Function {
        /**
         * Authorization form identifier.
         */
        public int autorizationFormId;
        /**
         * Password of the current user.
         */
        public String password;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1738134754;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns one of the available Telegram Passport elements.
     *
     * <p> Returns {@link PassportElement PassportElement} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetPassportElement extends Function {
        /**
         * Telegram Passport element type.
         */
        public PassportElementType type;
        /**
         * Password of the current user.
         */
        public String password;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1882398342;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the current state of 2-step verification.
     *
     * <p> Returns {@link PasswordState PasswordState} </p>
     */
    public static class GetPasswordState extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -174752904;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an invoice payment form. This method should be called when the user presses inlineKeyboardButtonBuy.
     *
     * <p> Returns {@link PaymentForm PaymentForm} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetPaymentForm extends Function {
        /**
         * Chat identifier of the Invoice message.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2146950882;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a successful payment.
     *
     * <p> Returns {@link PaymentReceipt PaymentReceipt} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetPaymentReceipt extends Function {
        /**
         * Chat identifier of the PaymentSuccessful message.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1013758294;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an IETF language tag of the language preferred in the country, which should be used to fill native fields in Telegram Passport personal details. Returns a 404 error if unknown.
     *
     * <p> Returns {@link Text Text} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetPreferredCountryLanguage extends Function {
        /**
         * A two-letter ISO 3166-1 alpha-2 country code.
         */
        public String countryCode;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -933049386;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns list of proxies that are currently set up. Can be called before authorization.
     *
     * <p> Returns {@link Proxies Proxies} </p>
     */
    public static class GetProxies extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -95026381;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an HTTPS link, which can be used to add a proxy. Available only for SOCKS5 and MTProto proxies. Can be called before authorization.
     *
     * <p> Returns {@link Text Text} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetProxyLink extends Function {
        /**
         * Proxy identifier.
         */
        public int proxyId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1285597664;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a public HTTPS link to a message. Available only for messages in supergroups and channels with username.
     *
     * <p> Returns {@link PublicMessageLink PublicMessageLink} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetPublicMessageLink extends Function {
        /**
         * Identifier of the chat to which the message belongs.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * Pass true if a link for a whole media album should be returned.
         */
        public boolean forAlbum;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -374642839;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a globally unique push notification subscription identifier for identification of an account, which has received a push notification. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link PushReceiverId PushReceiverId} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetPushReceiverId extends Function {
        /**
         * JSON-encoded push notification payload.
         */
        public String payload;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -286505294;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns up to 20 recently used inline bots in the order of their last usage.
     *
     * <p> Returns {@link Users Users} </p>
     */
    public static class GetRecentInlineBots extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1437823548;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of recently used stickers.
     *
     * <p> Returns {@link Stickers Stickers} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetRecentStickers extends Function {
        /**
         * Pass true to return stickers and masks that were recently attached to photos or video files; pass false to return recently sent stickers.
         */
        public boolean isAttached;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -579622241;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns t.me URLs recently visited by a newly registered user.
     *
     * <p> Returns {@link TMeUrls TMeUrls} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetRecentlyVisitedTMeUrls extends Function {
        /**
         * Google Play referrer to identify the user.
         */
        public String referrer;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 806754961;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a 2-step verification recovery email address that was previously set up. This method can be used to verify a password provided by the user.
     *
     * <p> Returns {@link RecoveryEmailAddress RecoveryEmailAddress} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetRecoveryEmailAddress extends Function {
        /**
         * The password for the current user.
         */
        public String password;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1594770947;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a file by its remote ID; this is an offline request. Can be used to register a URL as a file for further uploading, or sending as a message. Even the request succeeds, the file can be used only if it is still accessible to the user. For example, if the file is from a message, then the message must be not deleted and accessible to the user. If a file database is disabled, then the corresponding object with the file must be preloaded by the client.
     *
     * <p> Returns {@link File File} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetRemoteFile extends Function {
        /**
         * Remote identifier of the file to get.
         */
        public String remoteFileId;
        /**
         * File type, if known.
         */
        public FileType fileType;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2137204530;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a message that is replied by given message.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetRepliedMessage extends Function {
        /**
         * Identifier of the chat the message belongs to.
         */
        public long chatId;
        /**
         * Identifier of the message reply to which get.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -641918531;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns saved animations.
     *
     * <p> Returns {@link Animations Animations} </p>
     */
    public static class GetSavedAnimations extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 7051032;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns saved order info, if any.
     *
     * <p> Returns {@link OrderInfo OrderInfo} </p>
     */
    public static class GetSavedOrderInfo extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1152016675;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the notification settings for chats of a given type.
     *
     * <p> Returns {@link ScopeNotificationSettings ScopeNotificationSettings} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetScopeNotificationSettings extends Function {
        /**
         * Types of chats for which to return the notification settings information.
         */
        public NotificationSettingsScope scope;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -995613361;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a secret chat by its identifier. This is an offline request.
     *
     * <p> Returns {@link SecretChat SecretChat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetSecretChat extends Function {
        /**
         * Secret chat identifier.
         */
        public int secretChatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 40599169;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns emoji corresponding to a sticker. The list is only for informational purposes, because a sticker is always sent with a fixed emoji from the corresponding Sticker object.
     *
     * <p> Returns {@link Emojis Emojis} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetStickerEmojis extends Function {
        /**
         * Sticker file identifier.
         */
        public InputFile sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1895508665;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a sticker set by its identifier.
     *
     * <p> Returns {@link StickerSet StickerSet} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetStickerSet extends Function {
        /**
         * Identifier of the sticker set.
         */
        public long setId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1052318659;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns stickers from the installed sticker sets that correspond to a given emoji. If the emoji is not empty, favorite and recently used stickers may also be returned.
     *
     * <p> Returns {@link Stickers Stickers} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetStickers extends Function {
        /**
         * String representation of emoji. If empty, returns all known installed stickers.
         */
        public String emoji;
        /**
         * Maximum number of stickers to be returned.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1594919556;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns storage usage statistics. Can be called before authorization.
     *
     * <p> Returns {@link StorageStatistics StorageStatistics} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetStorageStatistics extends Function {
        /**
         * Maximum number of chats with the largest storage usage for which separate statistics should be returned. All other chats will be grouped in entries with chatId == 0. If the chat info database is not used, the chatLimit is ignored and is always set to 0.
         */
        public int chatLimit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -853193929;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Quickly returns approximate storage usage statistics. Can be called before authorization.
     *
     * <p> Returns {@link StorageStatisticsFast StorageStatisticsFast} </p>
     */
    public static class GetStorageStatisticsFast extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 61368066;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a supergroup or channel by its identifier. This is an offline request if the current user is not a bot.
     *
     * <p> Returns {@link Supergroup Supergroup} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetSupergroup extends Function {
        /**
         * Supergroup or channel identifier.
         */
        public int supergroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2063063706;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns full information about a supergroup or channel by its identifier, cached for up to 1 minute.
     *
     * <p> Returns {@link SupergroupFullInfo SupergroupFullInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetSupergroupFullInfo extends Function {
        /**
         * Supergroup or channel identifier.
         */
        public int supergroupId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1150331262;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about members or banned users in a supergroup or channel. Can be used only if SupergroupFullInfo.canGetMembers == true; additionally, administrator privileges may be required for some filters.
     *
     * <p> Returns {@link ChatMembers ChatMembers} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetSupergroupMembers extends Function {
        /**
         * Identifier of the supergroup or channel.
         */
        public int supergroupId;
        /**
         * The type of users to return. By default, supergroupMembersRecent.
         */
        public SupergroupMembersFilter filter;
        /**
         * Number of users to skip.
         */
        public int offset;
        /**
         * The maximum number of users be returned; up to 200.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1427643098;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a user that can be contacted to get support.
     *
     * <p> Returns {@link User User} </p>
     */
    public static class GetSupportUser extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1733497700;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about the current temporary password.
     *
     * <p> Returns {@link TemporaryPasswordState TemporaryPasswordState} </p>
     */
    public static class GetTemporaryPasswordState extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -12670830;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns all entities (mentions, hashtags, cashtags, bot commands, URLs, and email addresses) contained in the text. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link TextEntities TextEntities} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetTextEntities extends Function {
        /**
         * The text in which to look for entites.
         */
        public String text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -341490693;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a salt to be used with locally stored password to access a local TON-based wallet.
     *
     * <p> Returns {@link TonWalletPasswordSalt TonWalletPasswordSalt} </p>
     */
    public static class GetTonWalletPasswordSalt extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -642507025;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of frequently used chats. Supported only if the chat info database is enabled.
     *
     * <p> Returns {@link Chats Chats} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetTopChats extends Function {
        /**
         * Category of chats to be returned.
         */
        public TopChatCategory category;
        /**
         * Maximum number of chats to be returned; up to 30.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -388410847;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a list of trending sticker sets.
     *
     * <p> Returns {@link StickerSets StickerSets} </p>
     */
    public static class GetTrendingStickerSets extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1729129957;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about a user by their identifier. This is an offline request if the current user is not a bot.
     *
     * <p> Returns {@link User User} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetUser extends Function {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -47586017;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns full information about a user by their identifier.
     *
     * <p> Returns {@link UserFullInfo UserFullInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetUserFullInfo extends Function {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -655443263;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the current privacy settings.
     *
     * <p> Returns {@link UserPrivacySettingRules UserPrivacySettingRules} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetUserPrivacySettingRules extends Function {
        /**
         * The privacy setting.
         */
        public UserPrivacySetting setting;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2077223311;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the profile photos of a user. The result of this query may be outdated: some photos might have been deleted already.
     *
     * <p> Returns {@link UserProfilePhotos UserProfilePhotos} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetUserProfilePhotos extends Function {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * The number of photos to skip; must be non-negative.
         */
        public int offset;
        /**
         * Maximum number of photos to be returned; up to 100.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2062927433;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns an instant view version of a web page if available. Returns a 404 error if the web page has no instant view page.
     *
     * <p> Returns {@link WebPageInstantView WebPageInstantView} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetWebPageInstantView extends Function {
        /**
         * The web page URL.
         */
        public String url;
        /**
         * If true, the full instant view for the web page will be returned.
         */
        public boolean forceFull;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1962649975;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns a web page preview by the text of the message. Do not call this function too often. Returns a 404 error if the web page has no preview.
     *
     * <p> Returns {@link WebPage WebPage} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GetWebPagePreview extends Function {
        /**
         * Message text with formatting.
         */
        public FormattedText text;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 573441580;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds new contacts or edits existing contacts; contacts' user identifiers are ignored.
     *
     * <p> Returns {@link ImportedContacts ImportedContacts} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ImportContacts extends Function {
        /**
         * The list of contacts to import or edit, contact's vCard are ignored and are not imported.
         */
        public Contact[] contacts;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -215132767;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds current user as a new member to a chat. Private and secret chats can't be joined using this method.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JoinChat extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 326769313;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Uses an invite link to add the current user to the chat if possible. The new member will not be added until the chat state has been synchronized with the server.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JoinChatByInviteLink extends Function {
        /**
         * Invite link to import; should begin with &quot;https://t.me/joinchat/&quot;, &quot;https://telegram.me/joinchat/&quot;, or &quot;https://telegram.dog/joinchat/&quot;.
         */
        public String inviteLink;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1049973882;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes current user from chat members. Private and secret chats can't be left using this method.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LeaveChat extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1825080735;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Closes the TDLib instance after a proper logout. Requires an available network connection. All local data will be destroyed. After the logout completes, updateAuthorizationState with authorizationStateClosed will be sent.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class LogOut extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1581923301;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Informs TDLib that the chat is opened by the user. Many useful activities depend on the chat being opened or closed (e.g., in supergroups and channels all updates are received only for opened chats).
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OpenChat extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -323371509;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Informs TDLib that the message content has been opened (e.g., the user has opened a photo, video, document, location or venue, or has listened to an audio file or voice note message). An updateMessageContentOpened update will be generated if something has changed.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OpenMessageContent extends Function {
        /**
         * Chat identifier of the message.
         */
        public long chatId;
        /**
         * Identifier of the message with the opened content.
         */
        public long messageId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -739088005;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Optimizes storage usage, i.e. deletes some files and returns new storage usage statistics. Secret thumbnails can't be deleted.
     *
     * <p> Returns {@link StorageStatistics StorageStatistics} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OptimizeStorage extends Function {
        /**
         * Limit on the total size of files after deletion. Pass -1 to use the default limit.
         */
        public long size;
        /**
         * Limit on the time that has passed since the last time a file was accessed (or creation time for some filesystems). Pass -1 to use the default limit.
         */
        public int ttl;
        /**
         * Limit on the total count of files after deletion. Pass -1 to use the default limit.
         */
        public int count;
        /**
         * The amount of time after the creation of a file during which it can't be deleted, in seconds. Pass -1 to use the default value.
         */
        public int immunityDelay;
        /**
         * If not empty, only files with the given type(s) are considered. By default, all types except thumbnails, profile photos, stickers and wallpapers are deleted.
         */
        public FileType[] fileTypes;
        /**
         * If not empty, only files from the given chats are considered. Use 0 as chat identifier to delete files not belonging to any chat (e.g., profile photos).
         */
        public long[] chatIds;
        /**
         * If not empty, files from the given chats are excluded. Use 0 as chat identifier to exclude all files not belonging to any chat (e.g., profile photos).
         */
        public long[] excludeChatIds;
        /**
         * Same as in getStorageStatistics. Affects only returned statistics.
         */
        public int chatLimit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 980397489;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Parses Bold, Italic, Code, Pre, PreCode and TextUrl entities contained in the text. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link FormattedText FormattedText} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ParseTextEntities extends Function {
        /**
         * The text which should be parsed.
         */
        public String text;
        /**
         * Text parse mode.
         */
        public TextParseMode parseMode;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1709194593;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Pins a message in a chat; requires canPinMessages rights.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PinChatMessage extends Function {
        /**
         * Identifier of the chat.
         */
        public long chatId;
        /**
         * Identifier of the new pinned message.
         */
        public long messageId;
        /**
         * True, if there should be no notification about the pinned message.
         */
        public boolean disableNotification;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -554712351;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Computes time needed to receive a response from a Telegram server through a proxy. Can be called before authorization.
     *
     * <p> Returns {@link Seconds Seconds} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PingProxy extends Function {
        /**
         * Proxy identifier. Use 0 to ping a Telegram server without a proxy.
         */
        public int proxyId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -979681103;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Handles a push notification. Returns error with code 406 if the push notification is not supported and connection to the server is required to fetch new data. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ProcessPushNotification extends Function {
        /**
         * JSON-encoded push notification payload with all fields sent by the server, and &quot;google.sentTime&quot; and &quot;google.notification.sound&quot; fields added.
         */
        public String payload;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 786679952;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Marks all mentions in a chat as read.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReadAllChatMentions extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1357558453;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Reads a part of a file from the TDLib file cache and returns read bytes. This method is intended to be used only if the client has no direct access to TDLib's file system, because it is usually slower than a direct read from the file.
     *
     * <p> Returns {@link FilePart FilePart} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReadFilePart extends Function {
        /**
         * Identifier of the file. The file must be located in the TDLib file cache.
         */
        public int fileId;
        /**
         * The offset from which to read the file.
         */
        public int offset;
        /**
         * Number of bytes to read. An error will be returned if there are not enough bytes available in the file from the specified position. Pass 0 to read all available data from the specified position.
         */
        public int count;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -407749314;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Recovers the password with a password recovery code sent to an email address that was previously set up. Works only when the current authorization state is authorizationStateWaitPassword.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RecoverAuthenticationPassword extends Function {
        /**
         * Recovery code to check.
         */
        public String recoveryCode;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 787436412;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Recovers the password using a recovery code sent to an email address that was previously set up.
     *
     * <p> Returns {@link PasswordState PasswordState} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RecoverPassword extends Function {
        /**
         * Recovery code to check.
         */
        public String recoveryCode;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1660185903;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Registers the currently used device for receiving push notifications. Returns a globally unique identifier of the push notification subscription.
     *
     * <p> Returns {@link PushReceiverId PushReceiverId} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RegisterDevice extends Function {
        /**
         * Device token.
         */
        public DeviceToken deviceToken;
        /**
         * List of user identifiers of other users currently using the client.
         */
        public int[] otherUserIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1734127493;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Finishes user registration. Works only when the current authorization state is authorizationStateWaitRegistration.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RegisterUser extends Function {
        /**
         * The first name of the user; 1-64 characters.
         */
        public String firstName;
        /**
         * The last name of the user; 0-64 characters.
         */
        public String lastName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -109994467;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes background from the list of installed backgrounds.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveBackground extends Function {
        /**
         * The background indentifier.
         */
        public long backgroundId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1484545642;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes users from the contact list.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveContacts extends Function {
        /**
         * Identifiers of users to be deleted.
         */
        public int[] userIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -615510759;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a sticker from the list of favorite stickers.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveFavoriteSticker extends Function {
        /**
         * Sticker file to delete from the list.
         */
        public InputFile sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1152945264;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes an active notification from notification list. Needs to be called only if the notification is removed by the current user.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveNotification extends Function {
        /**
         * Identifier of notification group to which the notification belongs.
         */
        public int notificationGroupId;
        /**
         * Identifier of removed notification.
         */
        public int notificationId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 862630734;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a group of active notifications. Needs to be called only if the notification group is removed by the current user.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveNotificationGroup extends Function {
        /**
         * Notification group identifier.
         */
        public int notificationGroupId;
        /**
         * Maximum identifier of removed notifications.
         */
        public int maxNotificationId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1713005454;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a proxy server. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveProxy extends Function {
        /**
         * Proxy identifier.
         */
        public int proxyId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1369219847;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a hashtag from the list of recently used hashtags.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveRecentHashtag extends Function {
        /**
         * Hashtag to delete.
         */
        public String hashtag;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1013735260;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a sticker from the list of recently used stickers.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveRecentSticker extends Function {
        /**
         * Pass true to remove the sticker from the list of stickers recently attached to photo or video files; pass false to remove the sticker from the list of recently sent stickers.
         */
        public boolean isAttached;
        /**
         * Sticker file to delete.
         */
        public InputFile sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1246577677;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a chat from the list of recently found chats.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveRecentlyFoundChat extends Function {
        /**
         * Identifier of the chat to be removed.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 717340444;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes an animation from the list of saved animations.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveSavedAnimation extends Function {
        /**
         * Animation file to be removed.
         */
        public InputFile animation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -495605479;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a sticker from the set to which it belongs; for bots only. The sticker set must have been created by the bot.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveStickerFromSet extends Function {
        /**
         * Sticker.
         */
        public InputFile sticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1642196644;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a chat from the list of frequently used chats. Supported only if the chat info database is enabled.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RemoveTopChat extends Function {
        /**
         * Category of frequently used chats.
         */
        public TopChatCategory category;
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1907876267;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the order of installed sticker sets.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReorderInstalledStickerSets extends Function {
        /**
         * Pass true to change the order of mask sticker sets; pass false to change the order of ordinary sticker sets.
         */
        public boolean isMasks;
        /**
         * Identifiers of installed sticker sets in the new correct order.
         */
        public long[] stickerSetIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1114537563;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Reports a chat to the Telegram moderators. Supported only for supergroups, channels, or private chats with bots, since other chats can't be checked by moderators.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReportChat extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The reason for reporting the chat.
         */
        public ChatReportReason reason;
        /**
         * Identifiers of reported messages, if any.
         */
        public long[] messageIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -312579772;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Reports some messages from a user in a supergroup as spam; requires administrator rights in the supergroup.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ReportSupergroupSpam extends Function {
        /**
         * Supergroup identifier.
         */
        public int supergroupId;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifiers of messages sent in the supergroup by the user. This list must be non-empty.
         */
        public long[] messageIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2125451498;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Requests to send a password recovery code to an email address that was previously set up. Works only when the current authorization state is authorizationStateWaitPassword.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class RequestAuthenticationPasswordRecovery extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1393896118;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Requests to send a password recovery code to an email address that was previously set up.
     *
     * <p> Returns {@link EmailAddressAuthenticationCodeInfo EmailAddressAuthenticationCodeInfo} </p>
     */
    public static class RequestPasswordRecovery extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -13777582;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Re-sends an authentication code to the user. Works only when the current authorization state is authorizationStateWaitCode and the nextCodeType of the result is not null.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class ResendAuthenticationCode extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -814377191;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Re-sends the authentication code sent to confirm a new phone number for the user. Works only if the previously received authenticationCodeInfo nextCodeType was not null.
     *
     * <p> Returns {@link AuthenticationCodeInfo AuthenticationCodeInfo} </p>
     */
    public static class ResendChangePhoneNumberCode extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -786772060;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Re-sends the code to verify an email address to be added to a user's Telegram Passport.
     *
     * <p> Returns {@link EmailAddressAuthenticationCodeInfo EmailAddressAuthenticationCodeInfo} </p>
     */
    public static class ResendEmailAddressVerificationCode extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1872416732;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Resends messages which failed to send. Can be called only for messages for which messageSendingStateFailed.canRetry is true and after specified in messageSendingStateFailed.retryAfter time passed. If a message is re-sent, the corresponding failed to send message is deleted. Returns the sent messages in the same order as the message identifiers passed in messageIds. If a message can't be re-sent, null will be returned instead of the message.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ResendMessages extends Function {
        /**
         * Identifier of the chat to send messages.
         */
        public long chatId;
        /**
         * Identifiers of the messages to resend. Message identifiers must be in a strictly increasing order.
         */
        public long[] messageIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -940655817;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Resends phone number confirmation code.
     *
     * <p> Returns {@link AuthenticationCodeInfo AuthenticationCodeInfo} </p>
     */
    public static class ResendPhoneNumberConfirmationCode extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2069068522;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Re-sends the code to verify a phone number to be added to a user's Telegram Passport.
     *
     * <p> Returns {@link AuthenticationCodeInfo AuthenticationCodeInfo} </p>
     */
    public static class ResendPhoneNumberVerificationCode extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1367629820;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Resends the 2-step verification recovery email address verification code.
     *
     * <p> Returns {@link PasswordState PasswordState} </p>
     */
    public static class ResendRecoveryEmailAddressCode extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 433483548;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Resets all notification settings to their default values. By default, all chats are unmuted, the sound is set to &quot;default&quot; and message previews are shown.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class ResetAllNotificationSettings extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -174020359;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Resets list of installed backgrounds to its default value.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class ResetBackgrounds extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 204852088;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Resets all network data usage statistics to zero. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class ResetNetworkStatistics extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1646452102;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Saves application log event on the server. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SaveApplicationLogEvent extends Function {
        /**
         * Event type.
         */
        public String type;
        /**
         * Optional chat identifier, associated with the event.
         */
        public long chatId;
        /**
         * The log event data.
         */
        public JsonValue data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -811154930;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for a background by its name.
     *
     * <p> Returns {@link Background Background} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchBackground extends Function {
        /**
         * The name of the background.
         */
        public String name;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2130996959;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for call messages. Returns the results in reverse chronological order (i. e., in order of decreasing messageId). For optimal performance the number of returned messages is chosen by the library.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchCallMessages extends Function {
        /**
         * Identifier of the message from which to search; use 0 to get results from the last message.
         */
        public long fromMessageId;
        /**
         * The maximum number of messages to be returned; up to 100. Fewer messages may be returned than specified by the limit, even if the end of the message history has not been reached.
         */
        public int limit;
        /**
         * If true, returns only messages with missed calls.
         */
        public boolean onlyMissed;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1077230820;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for a specified query in the first name, last name and username of the members of a specified chat. Requires administrator rights in channels.
     *
     * <p> Returns {@link ChatMembers ChatMembers} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchChatMembers extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Query to search for.
         */
        public String query;
        /**
         * The maximum number of users to be returned.
         */
        public int limit;
        /**
         * The type of users to return. By default, chatMembersFilterMembers.
         */
        public ChatMembersFilter filter;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -445823291;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for messages with given words in the chat. Returns the results in reverse chronological order, i.e. in order of decreasing messageId. Cannot be used in secret chats with a non-empty query (searchSecretMessages should be used instead), or without an enabled message database. For optimal performance the number of returned messages is chosen by the library.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchChatMessages extends Function {
        /**
         * Identifier of the chat in which to search messages.
         */
        public long chatId;
        /**
         * Query to search for.
         */
        public String query;
        /**
         * If not 0, only messages sent by the specified user will be returned. Not supported in secret chats.
         */
        public int senderUserId;
        /**
         * Identifier of the message starting from which history must be fetched; use 0 to get results from the last message.
         */
        public long fromMessageId;
        /**
         * Specify 0 to get results from exactly the fromMessageId or a negative offset to get the specified message and some newer messages.
         */
        public int offset;
        /**
         * The maximum number of messages to be returned; must be positive and can't be greater than 100. If the offset is negative, the limit must be greater than -offset. Fewer messages may be returned than specified by the limit, even if the end of the message history has not been reached.
         */
        public int limit;
        /**
         * Filter for message content in the search results.
         */
        public SearchMessagesFilter filter;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1528846671;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns information about the recent locations of chat members that were sent to the chat. Returns up to 1 location message per user.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchChatRecentLocationMessages extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Maximum number of messages to be returned.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 950238950;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for the specified query in the title and username of already known chats, this is an offline request. Returns chats in the order seen in the chat list.
     *
     * <p> Returns {@link Chats Chats} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchChats extends Function {
        /**
         * Query to search for. If the query is empty, returns up to 20 recently found chats.
         */
        public String query;
        /**
         * Maximum number of chats to be returned.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1879787060;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for the specified query in the title and username of already known chats via request to the server. Returns chats in the order seen in the chat list.
     *
     * <p> Returns {@link Chats Chats} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchChatsOnServer extends Function {
        /**
         * Query to search for.
         */
        public String query;
        /**
         * Maximum number of chats to be returned.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1158402188;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for the specified query in the first names, last names and usernames of the known user contacts.
     *
     * <p> Returns {@link Users Users} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchContacts extends Function {
        /**
         * Query to search for; may be empty to return all contacts.
         */
        public String query;
        /**
         * Maximum number of users to be returned.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1794690715;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for emojis by keywords. Supported only if the file database is enabled.
     *
     * <p> Returns {@link Emojis Emojis} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchEmojis extends Function {
        /**
         * Text to search for.
         */
        public String text;
        /**
         * True, if only emojis, which exactly match text needs to be returned.
         */
        public boolean exactMatch;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 454272250;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for recently used hashtags by their prefix.
     *
     * <p> Returns {@link Hashtags Hashtags} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchHashtags extends Function {
        /**
         * Hashtag prefix to search for.
         */
        public String prefix;
        /**
         * Maximum number of hashtags to be returned.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1043637617;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for installed sticker sets by looking for specified query in their title and name.
     *
     * <p> Returns {@link StickerSets StickerSets} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchInstalledStickerSets extends Function {
        /**
         * Pass true to return mask sticker sets; pass false to return ordinary sticker sets.
         */
        public boolean isMasks;
        /**
         * Query to search for.
         */
        public String query;
        /**
         * Maximum number of sticker sets to return.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 681171344;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for messages in all chats except secret chats. Returns the results in reverse chronological order (i.e., in order of decreasing (date, chatId, messageId)). For optimal performance the number of returned messages is chosen by the library.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchMessages extends Function {
        /**
         * Query to search for.
         */
        public String query;
        /**
         * The date of the message starting from which the results should be fetched. Use 0 or any date in the future to get results from the last message.
         */
        public int offsetDate;
        /**
         * The chat identifier of the last found message, or 0 for the first request.
         */
        public long offsetChatId;
        /**
         * The message identifier of the last found message, or 0 for the first request.
         */
        public long offsetMessageId;
        /**
         * The maximum number of messages to be returned, up to 100. Fewer messages may be returned than specified by the limit, even if the end of the message history has not been reached.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1579305146;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches a public chat by its username. Currently only private chats, supergroups and channels can be public. Returns the chat if found; otherwise an error is returned.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchPublicChat extends Function {
        /**
         * Username to be resolved.
         */
        public String username;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 857135533;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches public chats by looking for specified query in their username and title. Currently only private chats, supergroups and channels can be public. Returns a meaningful number of results. Returns nothing if the length of the searched username prefix is less than 5. Excludes private chats with contacts and chats from the chat list from the results.
     *
     * <p> Returns {@link Chats Chats} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchPublicChats extends Function {
        /**
         * Query to search for.
         */
        public String query;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 970385337;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for messages in secret chats. Returns the results in reverse chronological order. For optimal performance the number of returned messages is chosen by the library.
     *
     * <p> Returns {@link FoundMessages FoundMessages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchSecretMessages extends Function {
        /**
         * Identifier of the chat in which to search. Specify 0 to search in all secret chats.
         */
        public long chatId;
        /**
         * Query to search for. If empty, searchChatMessages should be used instead.
         */
        public String query;
        /**
         * The identifier from the result of a previous request, use 0 to get results from the last message.
         */
        public long fromSearchId;
        /**
         * Maximum number of messages to be returned; up to 100. Fewer messages may be returned than specified by the limit, even if the end of the message history has not been reached.
         */
        public int limit;
        /**
         * A filter for the content of messages in the search results.
         */
        public SearchMessagesFilter filter;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1670627915;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for a sticker set by its name.
     *
     * <p> Returns {@link StickerSet StickerSet} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchStickerSet extends Function {
        /**
         * Name of the sticker set.
         */
        public String name;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1157930222;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for ordinary sticker sets by looking for specified query in their title and name. Excludes installed sticker sets from the results.
     *
     * <p> Returns {@link StickerSets StickerSets} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchStickerSets extends Function {
        /**
         * Query to search for.
         */
        public String query;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1082314629;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Searches for stickers from public sticker sets that correspond to a given emoji.
     *
     * <p> Returns {@link Stickers Stickers} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchStickers extends Function {
        /**
         * String representation of emoji; must be non-empty.
         */
        public String emoji;
        /**
         * Maximum number of stickers to be returned.
         */
        public int limit;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1555771203;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Invites a bot to a chat (if it is not yet a member) and sends it the /start command. Bots can't be invited to a private chat other than the chat with the bot. Bots can't be invited to channels (although they can be added as admins) and secret chats. Returns the sent message.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendBotStartMessage extends Function {
        /**
         * Identifier of the bot.
         */
        public int botUserId;
        /**
         * Identifier of the target chat.
         */
        public long chatId;
        /**
         * A hidden parameter sent to the bot for deep linking purposes (https://core.telegram.org/bots#deep-linking).
         */
        public String parameter;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1112181339;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends debug information for a call.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendCallDebugInformation extends Function {
        /**
         * Call identifier.
         */
        public int callId;
        /**
         * Debug information in application-specific format.
         */
        public String debugInformation;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2019243839;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a call rating.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendCallRating extends Function {
        /**
         * Call identifier.
         */
        public int callId;
        /**
         * Call rating; 1-5.
         */
        public int rating;
        /**
         * An optional user comment if the rating is less than 5.
         */
        public String comment;
        /**
         * List of the exact types of problems with the call, specified by the user.
         */
        public CallProblem[] problems;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1402719502;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a notification about user activity in a chat.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendChatAction extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The action description.
         */
        public ChatAction action;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -841357536;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a notification about a screenshot taken in a chat. Supported only in private and secret chats.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendChatScreenshotTakenNotification extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 448399457;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the current TTL setting (sets a new self-destruct timer) in a secret chat and sends the corresponding message.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendChatSetTtlMessage extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New TTL value, in seconds.
         */
        public int ttl;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1432535564;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a custom request; for bots only.
     *
     * <p> Returns {@link CustomRequestResult CustomRequestResult} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendCustomRequest extends Function {
        /**
         * The method name.
         */
        public String method;
        /**
         * JSON-serialized method parameters.
         */
        public String parameters;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 285045153;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a code to verify an email address to be added to a user's Telegram Passport.
     *
     * <p> Returns {@link EmailAddressAuthenticationCodeInfo EmailAddressAuthenticationCodeInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendEmailAddressVerificationCode extends Function {
        /**
         * Email address.
         */
        public String emailAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -221621379;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends the result of an inline query as a message. Returns the sent message. Always clears a chat draft message.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendInlineQueryResultMessage extends Function {
        /**
         * Target chat.
         */
        public long chatId;
        /**
         * Identifier of a message to reply to or 0.
         */
        public long replyToMessageId;
        /**
         * Pass true to disable notification for the message. Not supported in secret chats.
         */
        public boolean disableNotification;
        /**
         * Pass true if the message is sent from background.
         */
        public boolean fromBackground;
        /**
         * Identifier of the inline query.
         */
        public long queryId;
        /**
         * Identifier of the inline result.
         */
        public String resultId;
        /**
         * If true, there will be no mention of a bot, via which the message is sent. Can be used only for bots GetOption(&quot;animation_search_bot_username&quot;), GetOption(&quot;photo_search_bot_username&quot;) and GetOption(&quot;venue_search_bot_username&quot;).
         */
        public boolean hideViaBot;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 893888200;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a message. Returns the sent message.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendMessage extends Function {
        /**
         * Target chat.
         */
        public long chatId;
        /**
         * Identifier of the message to reply to or 0.
         */
        public long replyToMessageId;
        /**
         * Pass true to disable notification for the message. Not supported in secret chats.
         */
        public boolean disableNotification;
        /**
         * Pass true if the message is sent from the background.
         */
        public boolean fromBackground;
        /**
         * Markup for replying to the message; for bots only.
         */
        public ReplyMarkup replyMarkup;
        /**
         * The content of the message to be sent.
         */
        public InputMessageContent inputMessageContent;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1694632114;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends messages grouped together into an album. Currently only photo and video messages can be grouped into an album. Returns sent messages.
     *
     * <p> Returns {@link Messages Messages} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendMessageAlbum extends Function {
        /**
         * Target chat.
         */
        public long chatId;
        /**
         * Identifier of a message to reply to or 0.
         */
        public long replyToMessageId;
        /**
         * Pass true to disable notification for the messages. Not supported in secret chats.
         */
        public boolean disableNotification;
        /**
         * Pass true if the messages are sent from the background.
         */
        public boolean fromBackground;
        /**
         * Contents of messages to be sent.
         */
        public InputMessageContent[] inputMessageContents;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -291823014;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a Telegram Passport authorization form, effectively sharing data with the service. This method must be called after getPassportAuthorizationFormAvailableElements if some previously available elements need to be used.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendPassportAuthorizationForm extends Function {
        /**
         * Authorization form identifier.
         */
        public int autorizationFormId;
        /**
         * Types of Telegram Passport elements chosen by user to complete the authorization form.
         */
        public PassportElementType[] types;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -602402218;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a filled-out payment form to the bot for final verification.
     *
     * <p> Returns {@link PaymentResult PaymentResult} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendPaymentForm extends Function {
        /**
         * Chat identifier of the Invoice message.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * Identifier returned by ValidateOrderInfo, or an empty string.
         */
        public String orderInfoId;
        /**
         * Identifier of a chosen shipping option, if applicable.
         */
        public String shippingOptionId;
        /**
         * The credentials chosen by user for payment.
         */
        public InputCredentials credentials;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 591581572;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends phone number confirmation code. Should be called when user presses &quot;https://t.me/confirmphone?phone=*******&amp;hash=**********&quot; or &quot;tg://confirmphone?phone=*******&amp;hash=**********&quot; link.
     *
     * <p> Returns {@link AuthenticationCodeInfo AuthenticationCodeInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendPhoneNumberConfirmationCode extends Function {
        /**
         * Value of the &quot;hash&quot; parameter from the link.
         */
        public String hash;
        /**
         * Value of the &quot;phone&quot; parameter from the link.
         */
        public String phoneNumber;
        /**
         * Settings for the authentication of the user's phone number.
         */
        public PhoneNumberAuthenticationSettings settings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1901171495;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a code to verify a phone number to be added to a user's Telegram Passport.
     *
     * <p> Returns {@link AuthenticationCodeInfo AuthenticationCodeInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendPhoneNumberVerificationCode extends Function {
        /**
         * The phone number of the user, in international format.
         */
        public String phoneNumber;
        /**
         * Settings for the authentication of the user's phone number.
         */
        public PhoneNumberAuthenticationSettings settings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2081689035;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a request to TON lite server through Telegram servers.
     *
     * <p> Returns {@link TonLiteServerResponse TonLiteServerResponse} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SendTonLiteServerRequest extends Function {
        /**
         * The request.
         */
        public byte[] request;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 964887713;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the period of inactivity after which the account of the current user will automatically be deleted.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetAccountTtl extends Function {
        /**
         * New account TTL.
         */
        public AccountTtl ttl;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 701389032;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Succeeds after a specified amount of time has passed. Can be called before authorization. Can be called before initialization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetAlarm extends Function {
        /**
         * Number of seconds before the function returns.
         */
        public double seconds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -873497067;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the phone number of the user and sends an authentication code to the user. Works only when the current authorization state is authorizationStateWaitPhoneNumber, or if there is no pending authentication query and the current authorization state is authorizationStateWaitCode or authorizationStateWaitPassword.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetAuthenticationPhoneNumber extends Function {
        /**
         * The phone number of the user, in international format.
         */
        public String phoneNumber;
        /**
         * Settings for the authentication of the user's phone number.
         */
        public PhoneNumberAuthenticationSettings settings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 868276259;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets auto-download settings.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetAutoDownloadSettings extends Function {
        /**
         * New user auto-download settings.
         */
        public AutoDownloadSettings settings;
        /**
         * Type of the network for which the new settings are applied.
         */
        public NetworkType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -353671948;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the background selected by the user; adds background to the list of installed backgrounds.
     *
     * <p> Returns {@link Background Background} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetBackground extends Function {
        /**
         * The input background to use, null for solid backgrounds.
         */
        public InputBackground background;
        /**
         * Background type; null for default background. The method will return error 404 if type is null.
         */
        public BackgroundType type;
        /**
         * True, if the background is chosen for dark theme.
         */
        public boolean forDarkTheme;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1035439225;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the bio of the current user.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetBio extends Function {
        /**
         * The new value of the user bio; 0-70 characters without line feeds.
         */
        public String bio;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1619582124;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Informs the server about the number of pending bot updates if they haven't been processed for a long time; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetBotUpdatesStatus extends Function {
        /**
         * The number of pending updates.
         */
        public int pendingUpdateCount;
        /**
         * The last error message.
         */
        public String errorMessage;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1154926191;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes client data associated with a chat.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetChatClientData extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New value of clientData.
         */
        public String clientData;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -827119811;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes information about a chat. Available for basic groups, supergroups, and channels. Requires canChangeInfo rights.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetChatDescription extends Function {
        /**
         * Identifier of the chat.
         */
        public long chatId;
        /**
         * New chat description; 0-255 characters.
         */
        public String description;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1957213277;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the draft message in a chat.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetChatDraftMessage extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New draft message; may be null.
         */
        public DraftMessage draftMessage;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -588175579;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the status of a chat member, needs appropriate privileges. This function is currently not suitable for adding new members to the chat; instead, use addChatMember. The chat member status will not be changed until it has been synchronized with the server.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetChatMemberStatus extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * The new status of the member in the chat.
         */
        public ChatMemberStatus status;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1754439241;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the notification settings of a chat.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetChatNotificationSettings extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New notification settings for the chat.
         */
        public ChatNotificationSettings notificationSettings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 777199614;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the chat members permissions. Supported only for basic groups and supergroups. Requires canRestrictMembers administrator right.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetChatPermissions extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New non-administrator members permissions in the chat.
         */
        public ChatPermissions permissions;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2138507006;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the photo of a chat. Supported only for basic groups, supergroups and channels. Requires canChangeInfo rights. The photo will not be changed before request to the server has been completed.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetChatPhoto extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New chat photo. You can use a zero InputFileId to delete the chat photo. Files that are accessible only by HTTP URL are not acceptable.
         */
        public InputFile photo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 132244217;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the chat title. Supported only for basic groups, supergroups and channels. Requires canChangeInfo rights. The title will not be changed until the request to the server has been completed.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetChatTitle extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New title of the chat; 1-128 characters.
         */
        public String title;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 164282047;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds or changes a custom local language pack to the current localization target.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetCustomLanguagePack extends Function {
        /**
         * Information about the language pack. Language pack ID must start with 'X', consist only of English letters, digits and hyphens, and must not exceed 64 characters. Can be called before authorization.
         */
        public LanguagePackInfo info;
        /**
         * Strings of the new language pack.
         */
        public LanguagePackString[] strings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -296742819;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds, edits or deletes a string in a custom local language pack. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetCustomLanguagePackString extends Function {
        /**
         * Identifier of a previously added custom local language pack in the current localization target.
         */
        public String languagePackId;
        /**
         * New language pack string.
         */
        public LanguagePackString newString;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1316365592;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the database encryption key. Usually the encryption key is never changed and is stored in some OS keychain.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetDatabaseEncryptionKey extends Function {
        /**
         * New encryption key.
         */
        public byte[] newEncryptionKey;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1204599371;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Informs TDLib on a file generation prograss.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetFileGenerationProgress extends Function {
        /**
         * The identifier of the generation process.
         */
        public long generationId;
        /**
         * Expected size of the generated file, in bytes; 0 if unknown.
         */
        public int expectedSize;
        /**
         * The number of bytes already generated.
         */
        public int localPrefixSize;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -540459953;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Updates the game score of the specified user in the game; for bots only.
     *
     * <p> Returns {@link Message Message} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetGameScore extends Function {
        /**
         * The chat to which the message with the game belongs.
         */
        public long chatId;
        /**
         * Identifier of the message.
         */
        public long messageId;
        /**
         * True, if the message should be edited.
         */
        public boolean editMessage;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * The new score.
         */
        public int score;
        /**
         * Pass true to update the score even if it decreases. If the score is 0, the user will be deleted from the high score table.
         */
        public boolean force;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1768307069;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Updates the game score of the specified user in a game; for bots only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetInlineGameScore extends Function {
        /**
         * Inline message identifier.
         */
        public String inlineMessageId;
        /**
         * True, if the message should be edited.
         */
        public boolean editMessage;
        /**
         * User identifier.
         */
        public int userId;
        /**
         * The new score.
         */
        public int score;
        /**
         * Pass true to update the score even if it decreases. If the score is 0, the user will be deleted from the high score table.
         */
        public boolean force;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 758435487;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets new log stream for internal logging of TDLib. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetLogStream extends Function {
        /**
         * New log stream.
         */
        public LogStream logStream;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1364199535;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the verbosity level for a specified TDLib internal log tag. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetLogTagVerbosityLevel extends Function {
        /**
         * Logging tag to change verbosity level.
         */
        public String tag;
        /**
         * New verbosity level; 1-1024.
         */
        public int newVerbosityLevel;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2095589738;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the verbosity level of the internal logging of TDLib. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetLogVerbosityLevel extends Function {
        /**
         * New value of the verbosity level for logging. Value 0 corresponds to fatal errors, value 1 corresponds to errors, value 2 corresponds to warnings and debug warnings, value 3 corresponds to informational, value 4 corresponds to debug, value 5 corresponds to verbose debug, value greater than 5 and up to 1023 can be used to enable even more logging.
         */
        public int newVerbosityLevel;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -303429678;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the first and last name of the current user. If something changes, updateUser will be sent.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetName extends Function {
        /**
         * The new value of the first name for the user; 1-64 characters.
         */
        public String firstName;
        /**
         * The new value of the optional last name for the user; 0-64 characters.
         */
        public String lastName;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1711693584;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the current network type. Can be called before authorization. Calling this method forces all network connections to reopen, mitigating the delay in switching between different networks, so it should be called whenever the network is changed, even if the network type remains the same. Network type is used to check whether the library can use the network at all and also for collecting detailed network data usage statistics.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetNetworkType extends Function {
        /**
         * The new network type. By default, networkTypeOther.
         */
        public NetworkType type;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -701635234;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the value of an option. (Check the list of available options on https://core.telegram.org/tdlib/options.) Only writable options can be set. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetOption extends Function {
        /**
         * The name of the option.
         */
        public String name;
        /**
         * The new value of the option.
         */
        public OptionValue value;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2114670322;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Adds an element to the user's Telegram Passport. May return an error with a message &quot;PHONE_VERIFICATION_NEEDED&quot; or &quot;EMAIL_VERIFICATION_NEEDED&quot; if the chosen phone number or the chosen email address must be verified first.
     *
     * <p> Returns {@link PassportElement PassportElement} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetPassportElement extends Function {
        /**
         * Input Telegram Passport element.
         */
        public InputPassportElement element;
        /**
         * Password of the current user.
         */
        public String password;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2068173212;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Informs the user that some of the elements in their Telegram Passport contain errors; for bots only. The user will not be able to resend the elements, until the errors are fixed.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetPassportElementErrors extends Function {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * The errors.
         */
        public InputPassportElementError[] errors;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1455869875;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the password for the user. If a new recovery email address is specified, then the change will not be applied until the new recovery email address is confirmed.
     *
     * <p> Returns {@link PasswordState PasswordState} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetPassword extends Function {
        /**
         * Previous password of the user.
         */
        public String oldPassword;
        /**
         * New password of the user; may be empty to remove the password.
         */
        public String newPassword;
        /**
         * New password hint; may be empty.
         */
        public String newHint;
        /**
         * Pass true if the recovery email address should be changed.
         */
        public boolean setRecoveryEmailAddress;
        /**
         * New recovery email address; may be empty.
         */
        public String newRecoveryEmailAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1193589027;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the order of pinned chats.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetPinnedChats extends Function {
        /**
         * The new list of pinned chats.
         */
        public long[] chatIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1369665719;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes user answer to a poll.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetPollAnswer extends Function {
        /**
         * Identifier of the chat to which the poll belongs.
         */
        public long chatId;
        /**
         * Identifier of the message containing the poll.
         */
        public long messageId;
        /**
         * 0-based identifiers of options, chosen by the user. Currently user can't choose more than 1 option.
         */
        public int[] optionIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1399388792;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Uploads a new profile photo for the current user. If something changes, updateUser will be sent.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetProfilePhoto extends Function {
        /**
         * Profile photo to set. inputFileId and inputFileRemote may still be unsupported.
         */
        public InputFile photo;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1594734550;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the 2-step verification recovery email address of the user. If a new recovery email address is specified, then the change will not be applied until the new recovery email address is confirmed. If newRecoveryEmailAddress is the same as the email address that is currently set up, this call succeeds immediately and aborts all other requests waiting for an email confirmation.
     *
     * <p> Returns {@link PasswordState PasswordState} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetRecoveryEmailAddress extends Function {
        /**
         * Password of the current user.
         */
        public String password;
        /**
         * New recovery email address.
         */
        public String newRecoveryEmailAddress;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1981836385;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes notification settings for chats of a given type.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetScopeNotificationSettings extends Function {
        /**
         * Types of chats for which to change the notification settings.
         */
        public NotificationSettingsScope scope;
        /**
         * The new notification settings for the given scope.
         */
        public ScopeNotificationSettings notificationSettings;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2049984966;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the position of a sticker in the set to which it belongs; for bots only. The sticker set must have been created by the bot.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetStickerPositionInSet extends Function {
        /**
         * Sticker.
         */
        public InputFile sticker;
        /**
         * New position of the sticker in the set, zero-based.
         */
        public int position;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 2075281185;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the sticker set of a supergroup; requires canChangeInfo rights.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetSupergroupStickerSet extends Function {
        /**
         * Identifier of the supergroup.
         */
        public int supergroupId;
        /**
         * New value of the supergroup sticker set identifier. Use 0 to remove the supergroup sticker set.
         */
        public long stickerSetId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -295782298;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the username of a supergroup or channel, requires creator privileges in the supergroup or channel.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetSupergroupUsername extends Function {
        /**
         * Identifier of the supergroup or channel.
         */
        public int supergroupId;
        /**
         * New value of the username. Use an empty string to remove the username.
         */
        public String username;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1428333122;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sets the parameters for TDLib initialization. Works only when the current authorization state is authorizationStateWaitTdlibParameters.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetTdlibParameters extends Function {
        /**
         * Parameters.
         */
        public TdlibParameters parameters;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1912557997;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes user privacy settings.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetUserPrivacySettingRules extends Function {
        /**
         * The privacy setting.
         */
        public UserPrivacySetting setting;
        /**
         * The new privacy rules.
         */
        public UserPrivacySettingRules rules;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -473812741;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the username of the current user. If something changes, updateUser will be sent.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SetUsername extends Function {
        /**
         * The new value of the username. Use an empty string to remove the username.
         */
        public String username;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 439901214;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Stops a poll. A poll in a message can be stopped when the message has canBeEdited flag set.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class StopPoll extends Function {
        /**
         * Identifier of the chat to which the poll belongs.
         */
        public long chatId;
        /**
         * Identifier of the message containing the poll.
         */
        public long messageId;
        /**
         * The new message reply markup; for bots only.
         */
        public ReplyMarkup replyMarkup;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1659374253;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Fetches the latest versions of all strings from a language pack in the current localization target from the server. This method doesn't need to be called explicitly for the current used/base language packs. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SynchronizeLanguagePack extends Function {
        /**
         * Language pack identifier.
         */
        public String languagePackId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2065307858;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Terminates all other sessions of the current user.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class TerminateAllOtherSessions extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1874485523;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Terminates a session of the current user.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TerminateSession extends Function {
        /**
         * Session identifier.
         */
        public long sessionId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -407385812;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the received bytes; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link TestBytes TestBytes} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestCallBytes extends Function {
        /**
         * Bytes to return.
         */
        public byte[] x;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -736011607;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Does nothing; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class TestCallEmpty extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -627291626;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the received string; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link TestString TestString} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestCallString extends Function {
        /**
         * String to return.
         */
        public String x;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1732818385;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the received vector of numbers; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link TestVectorInt TestVectorInt} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestCallVectorInt extends Function {
        /**
         * Vector of numbers to return.
         */
        public int[] x;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2137277793;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the received vector of objects containing a number; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link TestVectorIntObject TestVectorIntObject} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestCallVectorIntObject extends Function {
        /**
         * Vector of objects to return.
         */
        public TestInt[] x;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1825428218;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the received vector of strings; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link TestVectorString TestVectorString} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestCallVectorString extends Function {
        /**
         * Vector of strings to return.
         */
        public String[] x;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -408600900;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the received vector of objects containing a string; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link TestVectorStringObject TestVectorStringObject} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestCallVectorStringObject extends Function {
        /**
         * Vector of objects to return.
         */
        public TestString[] x;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1527666429;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Forces an updates.getDifference call to the Telegram servers; for testing only.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class TestGetDifference extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1747084069;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a simple network request to the Telegram servers; for testing only. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    public static class TestNetwork extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1343998901;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Sends a simple network request to the Telegram servers via proxy; for testing only. Can be called before authorization.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestProxy extends Function {
        /**
         * Proxy server IP address.
         */
        public String server;
        /**
         * Proxy server port.
         */
        public int port;
        /**
         * Proxy type.
         */
        public ProxyType type;
        /**
         * Identifier of a datacenter, with which to test connection.
         */
        public int dcId;
        /**
         * Maximum overall timeout for the request.
         */
        public double timeout;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1197366626;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the specified error and ensures that the Error object is used; for testing only. This is an offline method. Can be called before authorization. Can be called synchronously.
     *
     * <p> Returns {@link Error Error} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestReturnError extends Function {
        /**
         * The error to be returned.
         */
        public Error error;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 455179506;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Returns the squared received number; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link TestInt TestInt} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestSquareInt extends Function {
        /**
         * Number to square.
         */
        public int x;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -60135024;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Does nothing and ensures that the Update object is used; for testing only. This is an offline method. Can be called before authorization.
     *
     * <p> Returns {@link Update Update} </p>
     */
    public static class TestUseUpdate extends Function {
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 717094686;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the value of the default disableNotification parameter, used when a message is sent to a chat.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ToggleChatDefaultDisableNotification extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New value of defaultDisableNotification.
         */
        public boolean defaultDisableNotification;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 314794002;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the marked as unread state of a chat.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ToggleChatIsMarkedAsUnread extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New value of isMarkedAsUnread.
         */
        public boolean isMarkedAsUnread;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -986129697;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Changes the pinned state of a chat. You can pin up to GetOption(&quot;pinned_chat_count_max&quot;) non-secret chats and the same number of secret chats.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ToggleChatIsPinned extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * New value of isPinned.
         */
        public boolean isPinned;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1166802621;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Toggles whether the message history of a supergroup is available to new members; requires canChangeInfo rights.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ToggleSupergroupIsAllHistoryAvailable extends Function {
        /**
         * The identifier of the supergroup.
         */
        public int supergroupId;
        /**
         * The new value of isAllHistoryAvailable.
         */
        public boolean isAllHistoryAvailable;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1701526555;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Toggles sender signatures messages sent in a channel; requires canChangeInfo rights.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ToggleSupergroupSignMessages extends Function {
        /**
         * Identifier of the channel.
         */
        public int supergroupId;
        /**
         * New value of signMessages.
         */
        public boolean signMessages;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -558196581;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes a user from the blacklist.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UnblockUser extends Function {
        /**
         * User identifier.
         */
        public int userId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -307286367;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Removes the pinned message from a chat; requires canPinMessages rights in the group or channel.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UnpinChatMessage extends Function {
        /**
         * Identifier of the chat.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 277557690;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Creates a new supergroup from an existing basic group and sends a corresponding messageChatUpgradeTo and messageChatUpgradeFrom; requires creator privileges. Deactivates the original basic group.
     *
     * <p> Returns {@link Chat Chat} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UpgradeBasicGroupChatToSupergroupChat extends Function {
        /**
         * Identifier of the chat to upgrade.
         */
        public long chatId;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 300488122;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Asynchronously uploads a file to the cloud without sending it in a message. updateFile will be used to notify about upload progress and successful completion of the upload. The file will not have a persistent remote identifier until it will be sent in a message.
     *
     * <p> Returns {@link File File} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UploadFile extends Function {
        /**
         * File to upload.
         */
        public InputFile file;
        /**
         * File type.
         */
        public FileType fileType;
        /**
         * Priority of the upload (1-32). The higher the priority, the earlier the file will be uploaded. If the priorities of two files are equal, then the first one for which uploadFile was called will be uploaded first.
         */
        public int priority;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -745597786;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Uploads a PNG image with a sticker; for bots only; returns the uploaded file.
     *
     * <p> Returns {@link File File} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UploadStickerFile extends Function {
        /**
         * Sticker file owner.
         */
        public int userId;
        /**
         * PNG image with the sticker; must be up to 512 kB in size and fit in 512x512 square.
         */
        public InputFile pngSticker;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 1134087551;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Validates the order information provided by a user and returns the available shipping options for a flexible invoice.
     *
     * <p> Returns {@link ValidatedOrderInfo ValidatedOrderInfo} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ValidateOrderInfo extends Function {
        /**
         * Chat identifier of the Invoice message.
         */
        public long chatId;
        /**
         * Message identifier.
         */
        public long messageId;
        /**
         * The order information, provided by the user.
         */
        public OrderInfo orderInfo;
        /**
         * True, if the order information can be saved.
         */
        public boolean allowSave;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = 9480644;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Informs TDLib that messages are being viewed by the user. Many useful activities depend on whether the messages are currently being viewed or not (e.g., marking messages as read, incrementing a view counter, updating a view counter, removing deleted messages in supergroups and channels).
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ViewMessages extends Function {
        /**
         * Chat identifier.
         */
        public long chatId;
        /**
         * The identifiers of the messages being viewed.
         */
        public long[] messageIds;
        /**
         * True, if messages in closed chats should be marked as read.
         */
        public boolean forceRead;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -1925784915;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Informs the server that some trending sticker sets have been viewed by the user.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ViewTrendingStickerSets extends Function {
        /**
         * Identifiers of viewed trending sticker sets.
         */
        public long[] stickerSetIds;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -952416520;

        public int getConstructor() { return CONSTRUCTOR; }
    }

    /**
     * Writes a part of a generated file. This method is intended to be used only if the client has no direct access to TDLib's file system, because it is usually slower than a direct write to the destination file.
     *
     * <p> Returns {@link Ok Ok} </p>
     */
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class WriteGeneratedFilePart extends Function {
        /**
         * The identifier of the generation process.
         */
        public long generationId;
        /**
         * The offset from which to write the data to the file.
         */
        public int offset;
        /**
         * The data to write.
         */
        public byte[] data;
        /**
         * Identifier uniquely determining type of the object.
         */
        public static final int CONSTRUCTOR = -2062358189;

        public int getConstructor() { return CONSTRUCTOR; }
    }

}
