package org.drinkless.tdlib;

public final class Client {

    public static native long createNativeClient();

    public static native void nativeClientSend(long nativeClientId, long eventId, TdApi.Function function);

    public static native int nativeClientReceive(long nativeClientId, long[] eventIds, TdApi.Object[] events, double timeout);

    public static native TdApi.Object nativeClientExecute(TdApi.Function function);

    public static native void destroyNativeClient(long nativeClientId);

}
